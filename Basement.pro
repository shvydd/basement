QT -= gui

TEMPLATE = lib
DEFINES += BASEMENT_LIBRARY

CONFIG += c++17
CONFIG += object_parallel_to_source
CONFIG += static

unix:{
    LIBS += boost/COMPILED/libboost_serialization_GCC_9.a.a
} else {
    win32:contains(QMAKE_HOST.arch, x86_64) {
        LIBS += boost\COMPILED\libboost_serialization-mgw83-mt-x64-1_72.a
    } else {
        LIBS += boost\COMPILED\libboost_serialization-mgw83-mt-x32-1_72.a
    }
}

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    basement/Basement.cpp \
    basement/Constants.cpp \
    basement/algorithm/derivative/Taylor.cpp \
    basement/algorithm/expansion/Taylor.cpp \
    basement/algorithm/findRoot/Bisection.cpp \
    basement/algorithm/findRoot/QuadraticEquation.cpp \
    basement/algorithm/fourier/Brute.cpp \
    basement/algorithm/fourier/CooleyTukey.cpp \
    basement/algorithm/fourier/DFT.cpp \
    basement/algorithm/fourier/HighlyChirped.cpp \
    basement/algorithm/integral/RiemannSum.cpp \
    basement/algorithm/interpolation/Cubic.cpp \
    basement/algorithm/interpolation/D2/Bicubic.cpp \
    basement/algorithm/interpolation/D2/Bilinear.cpp \
    basement/algorithm/interpolation/Linear.cpp \
    basement/algorithm/width/LastRidge.cpp \
    basement/container/ColumnsDouble.cpp \
    basement/container/Distribution3D.cpp \
    basement/container/SurfaceDouble.cpp \
    basement/container/grid/D1.cpp \
    basement/extension/double.cpp \
    basement/extension/pointer.cpp \
    basement/extension/size_t.cpp \
    basement/extension/string.cpp \
    basement/extension/time_t.cpp \
    basement/geometry/General.cpp \
    basement/geometry/VectorAndBasis.cpp \
    basement/math/Extrema.cpp \
    basement/math/Sequence.cpp \
    basement/io/FileStream.cpp \
    basement/io/TextInput.cpp \
    basement/io/TextOutput.cpp \
    basement/math/DiracDelta.cpp \
    basement/math/Factorial.cpp \
    basement/math/InverseFunction.cpp \
    basement/math/ModifiedBesselFunctionSecondKindAsymptotic.cpp \
    basement/math/Modulo.cpp \
    basement/math/Sign.cpp \
    basement/math/SquareTooth.cpp \
    basement/math/SuperGauss.cpp \
    basement/math/statistics/ArithmeticMean.cpp \
    basement/math/statistics/RMS.cpp \
    basement/xml/PrimitiveParser.cpp

HEADERS += \
    basement/Basement_global.h \
    basement/Basement.h \
    basement/Constants.h \
    basement/algorithm/derivative/Taylor.h \
    basement/algorithm/expansion/Taylor.h \
    basement/algorithm/findRoot/Bisection.h \
    basement/algorithm/findRoot/QuadraticEquation.h \
    basement/algorithm/fourier/Brute.h \
    basement/algorithm/fourier/CooleyTukey.h \
    basement/algorithm/fourier/DFT.h \
    basement/algorithm/fourier/HighlyChirped.h \
    basement/algorithm/integral/RiemannSum.h \
    basement/algorithm/interpolation/Cubic.h \
    basement/algorithm/interpolation/D2/Bicubic.h \
    basement/algorithm/interpolation/D2/Bilinear.h \
    basement/algorithm/interpolation/Linear.h \
    basement/algorithm/width/LastRidge.h \
    basement/container/ColumnsDouble.h \
    basement/container/Distribution3D.h \
    basement/container/Histogram.h \
    basement/container/MatrixStorage.h \
    basement/container/SurfaceDouble.h \
    basement/container/UniqueValMap.h \
    basement/container/grid/D1.h \
    basement/extension/double.h \
    basement/extension/pointer.h \
    basement/extension/size_t.h \
    basement/extension/string.h \
    basement/extension/time_t.h \
    basement/extension/vector.h \
    basement/geometry/General.h \
    basement/geometry/VectorAndBasis.h \
    basement/math/Extrema.h \
    basement/math/Sequence.h \
    basement/io/FileStream.h \
    basement/io/TextInput.h \
    basement/io/TextOutput.h \
    basement/math/DiracDelta.h \
    basement/math/Factorial.h \
    basement/math/InverseFunction.h \
    basement/math/ModifiedBesselFunctionSecondKindAsymptotic.h \
    basement/math/Modulo.h \
    basement/math/Sign.h \
    basement/math/SquareTooth.h \
    basement/math/SuperGauss.h \
    basement/math/statistics/ArithmeticMean.h \
    basement/math/statistics/RMS.h \
    basement/xml/PrimitiveParser.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
