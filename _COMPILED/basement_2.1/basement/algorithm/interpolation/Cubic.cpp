#include "basement/algorithm/interpolation/Cubic.h"

#include <iterator>

namespace basement
{
    namespace algorithm
    {
        namespace interpolation
        {
            const std::string Cubic::CLASS_NAME = "basement::algorithm::interpolation::Cubic";

            Cubic::Cubic(std::vector<double> _x, std::vector<double> _y)
                : y(std::move(_y))
            {
                if ( _x.size() != y.size() )
                    throw std::invalid_argument(CLASS_NAME + ": x vector size does not match y vector size.");

                if ( _x.size() < 2 )
                    throw std::invalid_argument(CLASS_NAME + ": size of x and y < 2.");

                xBehavior = basement::math::sequence::monotonicity::find(_x);
                if (xBehavior != basement::math::sequence::Monotonicity::strictly_increasing)
                {
                    if (xBehavior == basement::math::sequence::Monotonicity::strictly_increasing)
                    {
                        std::reverse(_x.begin(), _x.end());
                        std::reverse(y.begin(), y.end());
                    }
                    else
                        throw std::invalid_argument(CLASS_NAME + ": x vector values must be strictly monotonic.");
                }
                x.setEndPoints(std::move(_x));
            }

            const std::vector<double>& Cubic::getX() const
            {
                return x.getEndPoints();
            }

            const std::vector<double>& Cubic::getY() const
            {
                return y;
            }

            double Cubic::operator() (const double& point) const
            {
                const auto statusAndIndex = x.findIntervalIndex(point);

                const auto& status = statusAndIndex.first;
                if (status != basement::container::grid::D1::ValuePosition::onGrid)
                    return std::numeric_limits<double>::quiet_NaN();

                const auto& index = statusAndIndex.second;

                const size_t shift = [&] ()
                {
                    if (index == 0)
                    {
                        return size_t(0);
                    }
                    else if (index == x.getEndPoints().size() - 2)
                    {
                        const size_t shift = x.getEndPoints().size() - 4;
                        return shift;
                    }
                    else if (index < x.getEndPoints().size() - 2)
                    {
                        const size_t shift = index - 1;
                        return shift;
                    }
                    else
                    {
                        throw std::invalid_argument(CLASS_NAME + "::operator(x): index > grid.size() - 2.");
                    }
                }();

                // There is a need to find a, b, c, d:

                // f(x)  = ax^3 + bx^2 + cx + d.
                // df(x) = 3ax^2 + 2bx + c.

                // f(x1) = y1.
                // f(x2) = y2.
                // df(x1) = (y2 - y0) / (x2 - x0).
                // df(x2) = (y3 - y1) / (x3 - x1).

                const double x1 = x.getEndPoints()[shift + 1];
                const double x2 = x.getEndPoints()[shift + 2];

                const double y1 = y[shift + 1];
                const double y2 = y[shift + 2];

                const double dy1 = (y[shift+2] - y[shift+0]) / (x.getEndPoints()[shift+2] - x.getEndPoints()[shift+0]);
                const double dy2 = (y[shift+3] - y[shift+1]) / (x.getEndPoints()[shift+3] - x.getEndPoints()[shift+1]);

                // b = alfa * a + beta.

                const double temp1 = 2.0 * (x1 - x2);
                const double beta = (dy1 - dy2) / temp1;
                const double alfa = 3.0 * (x2*x2 - x1*x1) / temp1;

                // c = theta * a + phi.

                const double temp2 = (x1 - x2);

                const double theta = (x2*x2*x2 - x1*x1*x1 + alfa * (x2*x2 - x1*x1) ) / temp2;
                const double phi   = (beta * (x2*x2 - x1*x1) + (y1 - y2) ) / temp2;


                double a;
                const double aDenominator1 = 3.0 * x1*x1 + 2.0 * alfa * x1 + theta;
                if (aDenominator1 != 0.0)
                {
                    a = (dy1 - phi - 2.0 * beta * x1) / aDenominator1;
                }
                else
                {
                    const double aDenominator2 = 3.0 * x2*x2 + 2.0 * alfa * x2 + theta;

                    if (aDenominator2 == 0.0)
                        throw std::invalid_argument(CLASS_NAME + "::operator(x): can't solve system of linear equations.");

                    a = (dy2 - phi - 2.0 * beta * x2) / aDenominator2;
                }

                const double b = alfa * a + beta;
                const double c = theta * a + phi;
                const double d = y1 - a * x1*x1*x1 - b * x1*x1 - c * x1;

                return a * point*point*point + b * point*point + c * point + d;
            }
        }
    }
}

