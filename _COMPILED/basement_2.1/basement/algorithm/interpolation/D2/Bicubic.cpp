#include "basement/algorithm/interpolation/D2/Bicubic.h"

#include "basement/container/grid/D1.h"

#include <array>

namespace basement
{
	namespace algorithm
	{
		namespace interpolation
		{
			namespace D2
			{
				const std::string Bicubic::CLASS_NAME = "basement::algorithm::interpolation::D2::Bicubic";

				double Bicubic::interpolateLine(const std::array<double, 4>& x, const std::array<double, 4>& y, const double& point) const
				{
                    // There is a need to find a, b, c, d:

                    // f(x)  = ax^3 + bx^2 + cx + d.
                    // df(x) = 3ax^2 + 2bx + c.

                    // f(x1) = y1.
                    // f(x2) = y2.
                    // df(x1) = (y2 - y0) / (x2 - x0).
                    // df(x2) = (y3 - y1) / (x3 - x1).

                    const double& x1 = x[1];
                    const double& x2 = x[2];

                    const double& y1 = y[1];
                    const double& y2 = y[2];

                    const double dy1 = (y[2] - y[0]) / (x[2] - x[0]);
                    const double dy2 = (y[3] - y[1]) / (x[3] - x[1]);

                    // b = alfa * a + beta.

                    const double temp1 = 2.0 * (x1 - x2);
                    const double beta = (dy1 - dy2) / temp1;
                    const double alfa = 3.0 * (x2*x2 - x1*x1) / temp1;

                    // c = theta * a + phi.

                    const double temp2 = (x1 - x2);

                    const double theta = (x2*x2*x2 - x1*x1*x1 + alfa * (x2*x2 - x1*x1) ) / temp2;
                    const double phi   = (beta * (x2*x2 - x1*x1) + (y1 - y2) ) / temp2;


                    double a;
                    const double aDenominator1 = 3.0 * x1*x1 + 2.0 * alfa * x1 + theta;
                    if (aDenominator1 != 0.0)
                    {
                        a = (dy1 - phi - 2.0 * beta * x1) / aDenominator1;
                    }
                    else
                    {
                        const double aDenominator2 = 3.0 * x2*x2 + 2.0 * alfa * x2 + theta;

                        if (aDenominator2 == 0.0)
                            throw std::invalid_argument(CLASS_NAME + "::operator(x,y)::interpolateLine: can't solve system of linear equations.");

                        a = (dy2 - phi - 2.0 * beta * x2) / aDenominator2;
                    }

                    const double b = alfa * a + beta;
                    const double c = theta * a + phi;
                    const double d = y1 - a * x1*x1*x1 - b * x1*x1 - c * x1;

                    return a * point*point*point + b * point*point + c * point + d;
				}

				Bicubic::Bicubic(basement::container::SurfaceDouble surface)
                    : vals(std::move(surface))
				{
                    if (vals.getMatrix().getColsNumber() < 4 || vals.getMatrix().getRowsNumber() < 4)
                        throw std::invalid_argument(CLASS_NAME + ": invalid surface dimensions - must be >= 4x4.");
				}

                double Bicubic::operator() (const double& x, const double& y) const
				{
                    const std::vector<double> xGrid = vals.getXGrid().getCenterPoints();
                    const std::vector<double> yGrid = vals.getYGrid().getCenterPoints();

                    const auto xStatusAndIndex = vals.getXGrid().findIntervalIndex(x);
                    const auto& xStatus = xStatusAndIndex.first;

                    if (xStatus != basement::container::grid::D1::ValuePosition::onGrid)
                        return std::numeric_limits<double>::quiet_NaN();

                    size_t xIndexMin = xStatusAndIndex.second;

                    const auto yStatusAndIndex = vals.getYGrid().findIntervalIndex(y);
                    const auto& yStatus = yStatusAndIndex.first;

                    if (yStatus != basement::container::grid::D1::ValuePosition::onGrid)
                        return std::numeric_limits<double>::quiet_NaN();

                    size_t yIndexMin = yStatusAndIndex.second;

                    const std::function<size_t(const std::vector<double>& grid, size_t indexMin)> matchSubGridStartIndex = [] (const std::vector<double>& grid, size_t indexMin)
                    {
                        if (indexMin == 0)
                        {
                            return size_t(0);
                        }
                        else if ( (indexMin == grid.size() - 1) || (indexMin == grid.size() - 2) )
                        {
                            return grid.size() - 4;
                        }
                        else if (indexMin < grid.size() - 2)
                        {
                            return indexMin - 1;
                        }
                        else
                        {
                            throw std::invalid_argument(CLASS_NAME + "::operator(x,y)::matchSubGridStartIndex: indexMin > grid.size() - 1.");
                        }
                    };

                    const size_t xShiftIndex = matchSubGridStartIndex(xGrid, xIndexMin);
                    const size_t yShiftIndex = matchSubGridStartIndex(yGrid, yIndexMin);

                    const std::function<std::array<std::array<double, 4>, 4> ()> makeSubVals = [this, &xShiftIndex, &yShiftIndex] ()
                    {
                        std::array<std::array<double, 4>, 4> subVals;

                        for (size_t yIndex = 0; yIndex < 4 ; yIndex++)
                        {
                            for (size_t xIndex = 0; xIndex < 4; xIndex++)
                            {
                                subVals[yIndex][xIndex] = vals.at(yShiftIndex + yIndex, xShiftIndex + xIndex);
                            }
                        }

                        return subVals;
                    };

                    const std::function<std::array<double, 4>(const size_t indexShift, const std::vector<double>& grid)> makeSubGrid = [](const size_t indexShift, const std::vector<double>& grid)
                    {
                        return std::array<double, 4>{grid[indexShift + 0], grid[indexShift + 1], grid[indexShift + 2], grid[indexShift + 3]};
                    };

                    const auto xSubGrid = makeSubGrid(xShiftIndex, xGrid);
                    const auto ySubGrid = makeSubGrid(yShiftIndex, yGrid);
                    const auto subVals  = makeSubVals();

                    std::array<double, 4> ySectionInterpolated;

					for (size_t index = 0; index < 4; index++)
					{
                        ySectionInterpolated[index] = interpolateLine(xSubGrid, subVals[index], x);
					}

                    return interpolateLine(ySubGrid, ySectionInterpolated, y);
				}

                const basement::container::SurfaceDouble& Bicubic::getSurface() const
                {
                    return vals;
                }
			}
		}
	}
}

