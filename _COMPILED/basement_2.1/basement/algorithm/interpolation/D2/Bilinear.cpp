#include "basement/algorithm/interpolation/D2/Bilinear.h"

#include "basement/math/Sequence.h"

namespace basement
{
	namespace algorithm
	{
		namespace interpolation
		{
			namespace D2
			{
				const std::string Bilinear::CLASS_NAME = "basement::algorithm::interpolation::D2::Bilinear";

				Bilinear::Bilinear(basement::container::SurfaceDouble surface)
                    : surface(std::move(surface))
				{

				}

				double Bilinear::operator() (const double& x, const double& y) const
				{
                    const auto xStatusAndIndex = surface.getXGrid().findIntervalIndex(x);
                    const auto& xStatus = xStatusAndIndex.first;

                    if (xStatus != basement::container::grid::D1::ValuePosition::onGrid)
						return std::numeric_limits<double>::quiet_NaN();

                    size_t xIndexMin = xStatusAndIndex.second;
                    size_t xIndexMax = xIndexMin + 1;


                    const auto yStatusAndIndex = surface.getYGrid().findIntervalIndex(y);
                    const auto& yStatus = yStatusAndIndex.first;

                    if (yStatus != basement::container::grid::D1::ValuePosition::onGrid)
                        return std::numeric_limits<double>::quiet_NaN();

                    size_t yIndexMin = yStatusAndIndex.second;
                    size_t yIndexMax = yIndexMin + 1;


                    const std::vector<double>& xGrid = surface.getXGrid().getCenterPoints();
                    const std::vector<double>& yGrid = surface.getYGrid().getCenterPoints();

					const double x1 = xGrid[xIndexMin];
					const double x2 = xGrid[xIndexMax];

					const double y1 = yGrid[yIndexMin];
					const double y2 = yGrid[yIndexMax];

                    const double Q11 = surface.at(yIndexMin, xIndexMin);
                    const double Q12 = surface.at(yIndexMax, xIndexMin);
                    const double Q21 = surface.at(yIndexMin, xIndexMax);
                    const double Q22 = surface.at(yIndexMax, xIndexMax);

					const double a0 = Q11 * x2 * y2 / (x1-x2) / (y1-y2)   +    Q12 * x2 * y1 / (x1-x2) / (y2-y1)   +    Q21 * x1 * y2 / (x1-x2) / (y2-y1) + Q22 * x1 * y1 / (x1-x2) / (y1-y2);

					const double a1 = Q11 * y2 / (x1-x2) / (y2-y1)   +   Q12 * y1 / (x1-x2) / (y1-y2)   +   Q21 * y2 / (x1-x2) / (y1-y2) + Q22 * y1 / (x1-x2) / (y2-y1);

					const double a2 = Q11 * x2 / (x1-x2) / (y2-y1)   +   Q12 * x2 / (x1-x2) / (y1-y2)   +   Q21 * x1 / (x1-x2) / (y1-y2) + Q22 * x1 / (x1-x2) / (y2-y1);

					const double a3 = Q11 / (x1-x2) / (y1-y2)   +   Q12 / (x1-x2) / (y2-y1)   +   Q21 / (x1-x2) / (y2-y1) + Q22 / (x1-x2) / (y1-y2);

					return a0 + a1 * x + a2 * y + a3 * x*y;
				}

                const basement::container::SurfaceDouble& Bilinear::getSurface() const
                {
                    return surface;
                }
			}
		}
	}
}
