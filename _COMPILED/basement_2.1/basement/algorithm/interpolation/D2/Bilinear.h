#ifndef BASEMENT_AlGORITHM_INTERPOLATION_D2_BILINEAR_H
#define BASEMENT_AlGORITHM_INTERPOLATION_D2_BILINEAR_H

#include "basement/Basement_global.h"

#include "basement/container/SurfaceDouble.h"

namespace basement
{
	namespace algorithm
	{
		namespace interpolation
		{
			namespace D2
			{
				class Bilinear
				{
                public:
                    static const std::string CLASS_NAME;

                    Bilinear(basement::container::SurfaceDouble surface);

                    double operator() (const double& x, const double& y) const;

                    const basement::container::SurfaceDouble& getSurface() const;

                private:
                    const basement::container::SurfaceDouble surface;
				};
			}
		}
	}
}


#endif // BASEMENT_AlGORITHM_INTERPOLATION_D2_BILINEAR_H
