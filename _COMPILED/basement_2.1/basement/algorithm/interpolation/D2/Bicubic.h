#ifndef BASEMENT_AlGORITHM_INTERPOLATION_D2_BICUBIC_H
#define BASEMENT_AlGORITHM_INTERPOLATION_D2_BICUBIC_H

#include "basement/Basement_global.h"

#include "basement/container/SurfaceDouble.h"

namespace basement
{
	namespace algorithm
	{
		namespace interpolation
		{
			namespace D2
			{
				class Bicubic
				{
                public:
                    static const std::string CLASS_NAME;

                    Bicubic(basement::container::SurfaceDouble surface);

                    // TODO The interpolation returns NaN if grid reversed.
                    double operator() (const double& x, const double& y) const;

                    const basement::container::SurfaceDouble& getSurface() const;

                private:
                    const basement::container::SurfaceDouble vals;

                    double interpolateLine(const std::array<double, 4>& x, const std::array<double, 4>& y, const double& point) const;
				};
			}
		}
	}
}

#endif // BASEMENT_AlGORITHM_INTERPOLATION_D2_BICUBIC_H
