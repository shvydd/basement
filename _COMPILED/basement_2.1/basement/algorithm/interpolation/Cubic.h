#ifndef BASEMENT_AlGORITHM_INTERPOLATION_CUBIC_H
#define BASEMENT_AlGORITHM_INTERPOLATION_CUBIC_H


#include "basement/Basement_global.h"

#include <vector>
#include "basement/math/Sequence.h"
#include "basement/container/grid/D1.h"

namespace basement
{
    namespace algorithm
    {
        namespace interpolation
        {
            class Cubic
            {
                public:
                    static const std::string CLASS_NAME;

                private:
                    basement::container::grid::D1 x;
                    basement::math::sequence::Monotonicity xBehavior;
                    std::vector<double> y;

                public:
                    Cubic(std::vector<double> x, std::vector<double> y);

                    const std::vector<double>& getX() const;
                    const std::vector<double>& getY() const;

                    double operator() (const double& x) const;
            };
        }
    }
}
#endif // BASEMENT_AlGORITHM_INTERPOLATION_CUBIC_H
