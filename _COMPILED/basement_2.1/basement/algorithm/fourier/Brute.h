#ifndef BASEMENT_ALGORITHM_FOURIER_BRUTE_H
#define BASEMENT_ALGORITHM_FOURIER_BRUTE_H


#include "basement/Basement_global.h"

#include <complex>
#include <vector>

namespace basement
{
	namespace algorithm
	{
		namespace fourier
		{
			class Brute
			{
					//f(x) = Summ(n): A[n] * exp(i * x * frq[n])
					// fft - exp(-)
					//ifft - exp(+)

					typedef std::complex<double> complex;


				private:
					Brute() = delete;

				public:
					static std::vector<complex> ifft(const std::vector<double>& spectralDomain, const std::vector<complex>& fInSpectralDomain, const std::vector<double>& regularDomain);
                    static std::vector<complex>  fft(const std::vector<double>& regularDomain, const std::vector<complex>& fInRegularDomain, const std::vector<double>& sectralDomain);
			};
		}
	}
}

#endif // BASEMENT_ALGORITHM_FOURIER_BRUTE_H
