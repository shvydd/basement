#include "basement/algorithm/fourier/CooleyTukey.h"

#include "basement/extension/vector.h"
#include "basement/extension/size_t.h"
#include "basement/Constants.h"

namespace basement
{
	namespace algorithm
	{
		namespace fourier
		{
			void CooleyTukey::straight(std::vector<std::complex<double>>& fInRegularDomain)
			{
				const size_t N = fInRegularDomain.size();
				if (N < 2) return;

				std::vector<std::complex<double>> even = extension::vector_::slice(fInRegularDomain, 0, N, 2);
				std::vector<std::complex<double>> odd  = extension::vector_::slice(fInRegularDomain, 1, N, 2);

				straight(even);
				straight(odd);

				for (size_t k = 0; k < N/2; ++k)
				{
					std::complex<double> t = std::polar(1.0, -2.0 * constants::pi * k / N) * odd[k];
					fInRegularDomain[k    ] = even[k] + t;
					fInRegularDomain[k+N/2] = even[k] - t;
				}
			}

			void CooleyTukey::inverse(std::vector<std::complex<double>>& fInSpectralDomain)
			{
				const size_t N = fInSpectralDomain.size();
				if (N < 2) return;

				std::vector<std::complex<double>> even = extension::vector_::slice(fInSpectralDomain, 0, N, 2);
				std::vector<std::complex<double>> odd  = extension::vector_::slice(fInSpectralDomain, 1, N, 2);

				inverse(even);
				inverse(odd);

				for (size_t k = 0; k < N/2; ++k)
				{
					std::complex<double> t = std::polar(1.0, 2.0 * constants::pi * k / N) * odd[k];
					fInSpectralDomain[k    ] = even[k] + t;
					fInSpectralDomain[k+N/2] = even[k] - t;
				}
			}

			void CooleyTukey::splitAndSwap(std::vector<std::complex<double>>& vector)
			{
				std::vector<std::complex<double>> buff(vector.begin(),vector.begin() + vector.size()/2);
				vector.erase(vector.begin(),vector.begin() + vector.size()/2);
				vector.insert(vector.end(), buff.begin(), buff.end());
			}

			std::vector<std::complex<double>>  CooleyTukey::fft
			(const std::vector<std::complex<double>>& fInRegularDomain)
			{
				if (fInRegularDomain.size() < 2)
					throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: vector size < 2");

				if ( !extension::size_t_::isPowerOfTwo(fInRegularDomain.size()) )
					throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: vector size is not power of 2");

				std::vector<std::complex<double>> fInSpectralDomain(fInRegularDomain);
				splitAndSwap(fInSpectralDomain);
				straight(fInSpectralDomain);
				splitAndSwap(fInSpectralDomain);
				return fInSpectralDomain;
			}

            double CooleyTukey::findStep(const std::vector<double>& grid)
            {
                return (grid.back() - grid.front()) / double(grid.size() - 1);
            }

			std::vector<std::complex<double>> CooleyTukey::fft(const double& regularDomainStep, const std::vector<std::complex<double>>& fInRegularDomain)
			{
				if (regularDomainStep <= 0.0)
					throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: regular domain grid step <= 0");
				//NOTE The algorithm is valid with negative step (may be normer should be corrected for it),
				//but I do not want to handle the case.

				auto fInSpectralDomain = fft(fInRegularDomain);

				const double N = double(fInSpectralDomain.size());
				//NOTE The mult (N-2) was find empirically, instead (N-1).
				//The (N-2) more precisely preserves norms: Integral:f(x)dx & Integral:f(w)dw.
				//But after backward transform Integral:f(x)dx slightly differs from integral over original Integral:f(x)dx.
				const double normer = std::sqrt(regularDomainStep * regularDomainStep * (N-1) / (2.0 * constants::pi * N) );

				for (auto& f_frq : fInSpectralDomain)
				{
					f_frq = f_frq * normer;
				}
				return fInSpectralDomain;
			}

			std::pair< std::vector<double>, std::vector<std::complex<double>> > CooleyTukey::fft
			(
			const std::vector<double>& regularDomain,
			const std::vector<std::complex<double>>& fInRegularDomain,
			const double& spectralShift
			)
			{
				if (regularDomain.size() != fInRegularDomain.size())
                    throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: x & f(x) vector sizes differ.");

				std::pair< std::vector<double>, std::vector<std::complex<double>> > res;
				res.first  = spectralDomain(regularDomain, spectralShift);
                const double reg_step = findStep(regularDomain);
				res.second = fft(reg_step, fInRegularDomain);

				return res;
			}

			std::vector<std::complex<double>> CooleyTukey::ifft
			(const std::vector<std::complex<double>>& fInSpectralDomain)
			{
				if (fInSpectralDomain.size() < 2)
                    throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: vector size < 2.");

				if ( !extension::size_t_::isPowerOfTwo(fInSpectralDomain.size()) )
                    throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: vector size is not power of 2.");

				std::vector<std::complex<double>> fInRegularDomain(fInSpectralDomain);
				splitAndSwap(fInRegularDomain);
				inverse(fInRegularDomain);
				splitAndSwap(fInRegularDomain);
				return fInRegularDomain;
			}

			std::vector<std::complex<double>> CooleyTukey::ifft(const double& spectralDomainStep, const std::vector<std::complex<double>>& fInSpectralDomain)
			{
				if (spectralDomainStep <= 0.0)
                    throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: spectral domain grid step <= 0.");
				//NOTE The algorithm is valid with negative step (may be normer should be corrected for it),
				//but I do not want to handle the case.

				auto fInRegularDomain = ifft(fInSpectralDomain);

				const double N = double(fInRegularDomain.size());
				//NOTE The mult (N-2) was find empirically, instead (N-1).
				//The (N-2) more precisely preserves norms: Integral:f(x)dx & Integral:f(w)dw.
				//But after backward transform Integral:f(x)dx slightly differs from integral over original Integral:f(x)dx.
				const double normer = std::sqrt(spectralDomainStep * spectralDomainStep * (N-1) / (2.0 * constants::pi * N) );

				for (auto& f_frq : fInRegularDomain)
				{
					f_frq = f_frq * normer;
				}
				return fInRegularDomain;
			}

			std::pair< std::vector<double>, std::vector<std::complex<double>> > CooleyTukey::ifft
			(
			const std::vector<double>& spectralDomain,
			const std::vector<std::complex<double>>& fInSpectralDomain,
			const double& regularShift
			)
			{
				if (spectralDomain.size() != fInSpectralDomain.size())
                    throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: frq & f(frq) vector sizes differ.");

				std::pair< std::vector<double>, std::vector<std::complex<double>> > res;
				res.first  = regularDomain(spectralDomain, regularShift);
                const double spec_step = findStep(spectralDomain);
				res.second = ifft(spec_step, fInSpectralDomain);

				return res;
			}

			std::vector<double> CooleyTukey::generateDomain(const double& step, const double& shift, const size_t size)
			{
				std::vector<double> domain(size);
				for (size_t i = 0; i < size; i++)
				{
					if (i < size/2)
						domain[i] = -double(size/2 - i)*step + shift;
					else
						domain[i] =  double(i - size/2)*step + shift;
				}
				return domain;
			}

			std::vector<double> CooleyTukey::spectralDomain(const std::vector<double>& regularDomain, const double& spectralShift)
			{
				const size_t N = regularDomain.size();

				if (N < 2)
                    throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: regular domain vector size < 2.");

				if ( !extension::size_t_::isPowerOfTwo(N) )
                    throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: regular domain vector size is not power of 2.");

                const double reg_step = findStep(regularDomain);
				if (reg_step <= 0.0)
                    throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: regular domain step <= 0.");
				//NOTE The algorithm is valid with negative step (may be normer should be corrected for it),
				//but I do not want to handle the case.

				const double spec_span = 2.0 * constants::pi / reg_step;
				const double spec_step = spec_span / double( N - 1 );
				return generateDomain(spec_step, spectralShift, N);
			}

			std::vector<double> CooleyTukey::regularDomain(const std::vector<double>& spectralDomain, const double& regularShift)
			{
				const size_t N = spectralDomain.size();

				if (N < 2)
					throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: spectral domain vector size < 2");

				if ( !extension::size_t_::isPowerOfTwo(N) )
					throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: spectral domain vector size is not power of 2");

                const double spec_step = findStep(spectralDomain);
				if (spec_step <= 0.0)
					throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey: spectral domain step <= 0");
				//NOTE The algorithm is valid with negative step (may be normer should be corrected for it),
				//but I do not want to handle the case.

				const double reg_span = 2.0 * constants::pi / spec_step;
				const double reg_step = reg_span / double( N - 1 );
				return generateDomain(reg_step, regularShift, N);
			}

			std::pair<std::vector<double>, std::vector<double>> CooleyTukey::generateOptimalDomains
			(
			const double& reg_step,
			const double& reg_span,
			const double& reg_shift,
			const double& spec_step,
			const double& spec_span,
			const double& spec_shift
			)
			{
				if (reg_step <= 0.0 || reg_span <= 0.0 || spec_step <= 0.0 || spec_span <= 0.0)
                    throw std::invalid_argument("basement::algorithm::fourier::CooleyTukey::generateOptimalDomains: argument(s) <= 0.");

				double regularStep = std::min(reg_step, 2.0 * constants::pi / spec_span);
				double regularSpan = std::max(reg_span, 2.0 * constants::pi / spec_step);

				size_t N = static_cast<size_t>(std::ceil(regularSpan/regularStep)) + 1;
				if (!extension::size_t_::isPowerOfTwo(N))
				{
					size_t desiredN = std::pow( 2, static_cast<size_t>( std::ceil( std::log(N)/std::log(2) ) ) );

					const double mult = std::sqrt( double(desiredN - 1) / double (N-1) );

					regularStep = regularStep / mult;
					regularSpan = regularSpan * mult;
					N = desiredN;
				}

				std::pair<std::vector<double>, std::vector<double>> res;
				res.first = generateDomain(regularStep, reg_shift, N);
				res.second = generateDomain(2.0 * constants::pi / regularSpan, spec_shift, N);
				return res;
			}
		}
	}
}
