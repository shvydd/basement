#ifndef BASEMENT_ALGORITHM_FOURIER_HIGHLYCHIRPED_H
#define BASEMENT_ALGORITHM_FOURIER_HIGHLYCHIRPED_H

#include "basement/Basement_global.h"

#include <complex>
#include <vector>
#include <utility>
#include <functional>

namespace basement
{
    namespace algorithm
    {
        namespace fourier
        {
            /**
             * @brief Fourier transform for spectrum with huge chirp - second derivative of phase by angular frequency at carrier.
             */
            class HighlyChirped
            {
            private:
                HighlyChirped() = delete;

                typedef std::complex<double> complex;

            public:
                static std::vector<complex> ifft(const std::vector<double> frqs, const double frqCenter, const std::vector<complex>& fieldInSpectrum, const std::vector<double>& times, const double chirp);
                static std::vector<double>  generateTimeGrid(const double frqCenter, const double frqMin, const double frqMax, const double chirp, size_t timeGridSize);
                static std::pair<std::vector<double>, std::vector<double>> generateOptimalGrids(const double frqCenter, const double frqMin, const double frqMax, const double chirp, const std::function<double(const double& x)>& phase);
            };
        }
    }
}

#endif // HIGHLYCHIRPED_H
