#include "basement/algorithm/fourier/HighlyChirped.h"

#include "basement/extension/vector.h"
#include "basement/extension/size_t.h"
#include "basement/Constants.h"
#include "basement/algorithm/interpolation/Linear.h"
#include "basement/algorithm/integral/RiemannSum.h"
#include "basement/container/grid/D1.h"

//TODO Do not preserve fine phase.

namespace basement
{
    namespace algorithm
    {
        namespace fourier
        {
            std::vector<HighlyChirped::complex> HighlyChirped::ifft(const std::vector<double> frqs, const double frqCenter, const std::vector<complex>& fieldInSpectrum, const std::vector<double>& times, const double chirp)
            {
                if (frqs.size() != fieldInSpectrum.size())
                    throw std::invalid_argument("basement::algorithm::fourier::HighlyChirped::ifft: frqs size does not match fieldInSpectrum size");

                if (frqs.size() == 0)
                    throw std::invalid_argument("basement::algorithm::fourier::HighlyChirped::ifft: spectrum void");

                std::vector<complex> fieldInTime(times.size());

                const std::vector<double> fieldInSpectrumWithoutPhase = [&]()
                {
                    std::vector<double> fieldInSpectrumWithoutPhase;

                    for (const auto& field : fieldInSpectrum)
                        fieldInSpectrumWithoutPhase.push_back(std::abs(field));

                    return fieldInSpectrumWithoutPhase;
                }();

                const basement::algorithm::interpolation::Linear linear(frqs, fieldInSpectrumWithoutPhase);

                const double frqMin = *std::min_element(frqs.begin(), frqs.end());
                const double frqMax = *std::max_element(frqs.begin(), frqs.end());

                for (size_t i = 0; i < times.size(); i++)
                {
                    const double curFrq = times[i] / chirp + frqCenter;

                    complex E_frq  = 0.0;
                    if (curFrq >= frqMin && curFrq <= frqMax)                    
                        E_frq = linear(curFrq);

                    fieldInTime[i] = std::sqrt(basement::constants::i / chirp) * std::exp(-basement::constants::i * times[i] * times[i] / chirp) * E_frq;
                }

                {
                    std::vector<double> intensityInSpectrum;
                    for (const auto& field : fieldInSpectrum)
                        intensityInSpectrum.push_back(std::real(field * std::conj(field)));

                    std::vector<double> intensityInTime;
                    for (const auto& field : fieldInTime)
                        intensityInTime.push_back(std::real(field * std::conj(field)));

                    const basement::algorithm::integral::RiemannSum integrate;

                    const double energyInSpectrum = integrate(frqs, intensityInSpectrum);
                    const double energyInTime = integrate(times, intensityInTime);

                    const double normer = std::sqrt(energyInSpectrum/energyInTime);

                    for (auto& field : fieldInTime)
                        field = field * normer;

                    intensityInTime.clear();
                    for (const auto& field : fieldInTime)
                        intensityInTime.push_back(std::real(field * std::conj(field)));
                }

                return fieldInTime;
            }

            std::vector<double>  HighlyChirped::generateTimeGrid(const double frqCenter, const double frqMin, const double frqMax, const double chirp, size_t timeGridSize)
            {
                 const double tMin = (frqMin - frqCenter) * chirp;
                 const double tMax = (frqMax - frqCenter) * chirp;

                 return basement::container::grid::D1::generateUniformSequence(tMin, tMax, timeGridSize);
            }

            std::pair<std::vector<double>, std::vector<double>> HighlyChirped::generateOptimalGrids(const double frqCenter, const double frqMin, const double frqMax, const double chirp, const std::function<double(const double& x)>& phase)
            {
                const std::function<double(const double& frq)> residualPhase = [&chirp, &phase, &frqCenter](const double& frq)
                {
                    return phase(frq) - chirp * (frq-frqCenter) * (frq-frqCenter) / 2.0;
                };

                const std::function<double(const std::vector<double>& frqs, const std::function<double(const double& frq)>& residualPhase)> findMaxResidualPhase = [] (const std::vector<double>& frqs, const std::function<double(const double& frq)>& residualPhase)
                {
                    if (frqs.size() < 2)
                        return std::numeric_limits<double>::infinity();

                    double maxResidualPhase = std::abs( residualPhase(frqs[1]) - residualPhase(frqs[0]) );
                    for (size_t i = 0; i < frqs.size()-1; i++)
                    {
                        double currentResidualPhase = std::abs( residualPhase(frqs[i+1]) - residualPhase(frqs[i]) );
                        if (currentResidualPhase > maxResidualPhase)
                            maxResidualPhase = currentResidualPhase;
                    }
                    return maxResidualPhase;
                };

                double maxResidualPhase = std::numeric_limits<double>::infinity();
                size_t gridSize = 32;

                while (maxResidualPhase >= basement::constants::pi)
                {
                    gridSize = gridSize * 2;

                    auto frqs = basement::container::grid::D1::generateUniformSequence(frqMin, frqMax, gridSize);

                    maxResidualPhase = findMaxResidualPhase(frqs, residualPhase);
                }

                const double tMin = (frqMin - frqCenter) * chirp;
                const double tMax = (frqMax - frqCenter) * chirp;

                auto times = basement::container::grid::D1::generateUniformSequence(tMin, tMax, gridSize);
                auto frqs =  basement::container::grid::D1::generateUniformSequence(frqMin, frqMax, gridSize);

                return std::pair(times, frqs);
            }
        }
    }
}
