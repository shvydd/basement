#ifndef BASEMENT_AlGORITHM_DERIVATIVE_TAYLOR_H
#define BASEMENT_AlGORITHM_DERIVATIVE_TAYLOR_H


#include "basement/Basement_global.h"

#include <functional>
#include <vector>
#include <limits>

namespace basement
{
	namespace algorithm
	{
		namespace derivative
		{
			class Taylor
			{
					//http://www-math.mit.edu/~djk/calculus_beginners/chapter09/section01.html
				public:
					static void   checkErrorOrder(const size_t err_order);

					static size_t requiredNumOfPoints(const size_t err_order, const size_t dif_order = 1);

					static double checkFunctionVectors
					(
					const std::vector<double>& x,
					const std::vector<double>& F,
					const size_t requiredNumOfPoints,
					const bool checkXGridUniformity
					);

                    /**
                     * @brief Returns first order derivative at point x.
                     */
					double operator()
					(
					const std::function<double(const double)>& F,
					const double x,
					const double d,
					const size_t err_order
					) const;

                    /**
                     * @brief Returns first order derivative at point x.
                     */
					double operator()
					(
					const std::function<double(const double)>& F,
					const double x,
					const double d,
					const size_t dif_order,
					const size_t err_order
					) const;
				private:
                    /**
                     * @brief Returns first order derivatives for each point where it is possible.
                     */
					std::vector<double> firstOrderDerivatives
					(
					const std::vector<double>& F,
					const double step,
					const size_t err_order,
					const size_t requiredNumberOfPoints
					) const;

				public:
					//Return first order derivative values at each x value. For marginal
					//points, where not enough neighboring points to meet requested error order,
					//returns nan.
					std::vector<double> operator()
					(
					const std::vector<double>& x,
					const std::vector<double>& F,
					const size_t err_order,
					const bool checkXGridUniformity = true
					) const;

					//Returns vector, filled with derivatives from order 0 to
					//diff_order at the point x[pointIndex].
					std::vector<double> operator()
					(
					const std::vector<double>& x,
					const std::vector<double>& F,
					const size_t pointIndex,
					const size_t dif_order,
					const size_t err_order,
					const bool checkXGridUniformity = true
					) const;
			};
		}
	}
}

#endif // BASEMENT_AlGORITHM_DERIVATIVE_TAYLOR_H
