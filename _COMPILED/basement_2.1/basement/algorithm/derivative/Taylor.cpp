#include "basement/algorithm/derivative/Taylor.h"

#include <cmath>
#include <stdexcept>
#include "basement/math/Sequence.h"

namespace basement
{
	namespace algorithm
	{
		namespace derivative
		{
			void Taylor::checkErrorOrder(const size_t err_order)
			{
				if (err_order % 2 == 1 || err_order < 2)
                    throw std::invalid_argument("basement::algorithm::derivative::Taylor: error_order must be even and must be >= 2");
			}

			std::size_t Taylor::requiredNumOfPoints(const std::size_t err_order, const size_t dif_order)
			{
				return std::pow(2, (err_order-2)/2 ) * 2 * dif_order + 1;
			}

			double Taylor::checkFunctionVectors
			(
			const std::vector<double>& x,
			const std::vector<double>& F,
			const size_t requiredNumOfPoints,
			const bool checkXGridUniformity
			)
			{
				if ( x.size() != F.size() )
                    throw std::invalid_argument("basement::algorithm::derivative::Taylor: x and F(x) vectors must be of equal size");

				if (x.size() < requiredNumOfPoints)
                    throw std::invalid_argument("basement::algorithm::derivative::Taylor: not enough points for specified derivative order and error order");

				double step;
				if (checkXGridUniformity)
				{
					//TODO Adjust tolerance
                    const auto isUniform = basement::math::sequence::isUniform(x);
                    if ( !isUniform.first )
                        throw std::invalid_argument("basement::algorithm::derivative::Taylor: x grid is not uniform.");

                    step = isUniform.second;
				}
				else
				{
                    step = (x.front() - x.back()) / double (x.size() - 1);
				}

				if (step == 0.0)
                    throw std::invalid_argument("basement::algorithm::derivative::Taylor: step = 0");

				return step;
			}

			double Taylor::operator()
			(
			const std::function<double(const double)>& F,
			const double x,
			const double _d,
			const size_t err_order
			) const
			{
				checkErrorOrder(err_order);

				if (_d == 0.0)
                    throw std::invalid_argument("basement::algorithm::derivative::Taylor: step = 0");

				std::function<double(const double)> df = [=](const double d)
				{
					return ( F(x+d)-F(x-d) ) / (2.0*d);
				};

				long int mult = 2;
				for (size_t i=4; i<=err_order; i+=2)
				{
					mult = mult * mult;
					df = [=](const double d)
					{
						return ( double(mult)*df(d) - df(2.0*d) ) / double(mult - 1);
					};
				}
				return df(_d);
			}

			double Taylor::operator()
			(
			const std::function<double(const double)>& F,
			const double x,
			const double d,
			const size_t dif_order,
			const size_t err_order
			) const
			{
				(void) F;
				(void) x;
				(void) d;
				(void) dif_order;
				(void) err_order;
				//TODO write this
				throw std::domain_error("basement::algorithm::derivative::Taylor: high order derivative of std::function is not defined");
			}

			std::vector<double> Taylor::firstOrderDerivatives
			(
			const std::vector<double>& F,
			const double step,
			const size_t err_order,
			const size_t requiredNumberOfPoints
			) const
			{
				//TODO Why doesn't work with reference capture
				std::function<double(const std::size_t _point, const std::size_t delta_i)> df =
				[=](const std::size_t _point, const std::size_t delta_i)
				{
					return ( F[_point+delta_i] - F[_point-delta_i] ) / (2.0*delta_i*step);
				};

				long int mult = 2;
				for (size_t i=4; i<=err_order; i+=2)
				{
					mult = mult * mult;
					df = [=](const std::size_t _point, const std::size_t delta_i)
					{
						//TODO Check correctness with err_orders >= 10
						return ( double(mult)*df(_point, delta_i) - df(_point, 2.0*delta_i) ) / double(mult - 1);
					};
				}

				std::vector<double> dF(F.size());
				for (std::size_t point = 0; point < F.size(); point++)
				{
					if (point < requiredNumberOfPoints/2 || point >= F.size()-requiredNumberOfPoints/2)
						dF[point] = std::numeric_limits<double>::quiet_NaN();
					else
						dF[point] = df(point, 1);
				}
				return dF;
			}

			std::vector<double> Taylor::operator()
			(
			const std::vector<double>& x,
			const std::vector<double>& F,
			const size_t err_order,
			const bool checkXGridUniformity
			) const
			{
				checkErrorOrder(err_order);
				size_t requiredNumberOfPoints = requiredNumOfPoints(err_order, 1);
				double step = checkFunctionVectors(x, F, requiredNumberOfPoints, checkXGridUniformity);
				return this->firstOrderDerivatives(F, step, err_order, requiredNumberOfPoints);
			}

			std::vector<double> Taylor::operator()
			(
			const std::vector<double>& x,
			const std::vector<double>& F,
			const size_t pointIndex,
			const size_t dif_order,
			const size_t err_order,
			const bool checkXGridUniformity
			) const
			{
				checkErrorOrder(err_order);
				size_t requiredNumberOfPoints = requiredNumOfPoints(err_order, dif_order);
				double step = checkFunctionVectors(x,F,requiredNumberOfPoints,checkXGridUniformity);

				if ( pointIndex < requiredNumberOfPoints/2 || pointIndex >= F.size() - requiredNumberOfPoints/2 )
                    throw std::invalid_argument("basement::algorithm::derivative::Taylor: not enough points for specified derivative order and error order");

				std::vector<double> result(dif_order+1);
				result[0] = F[pointIndex];

				size_t truncSize = requiredNumberOfPoints;
				std::vector<double> x_trunc(x.begin() + pointIndex - truncSize/2, x.begin() + pointIndex + truncSize/2 + 1);
				std::vector<double> F_trunc(F.begin() + pointIndex - truncSize/2, F.begin() + pointIndex + truncSize/2 + 1);

				for (size_t d_order = 1; d_order <= dif_order; d_order++)
				{
					F_trunc = this->firstOrderDerivatives(F_trunc, step, err_order, requiredNumOfPoints(err_order, 1) );
					result[d_order] = F_trunc[truncSize/2];

					truncSize = requiredNumOfPoints(err_order, dif_order - d_order);
					x_trunc = std::vector<double>(x_trunc.begin() + x_trunc.size()/2 - truncSize/2, x_trunc.begin() + x_trunc.size()/2 + truncSize/2 + 1);
					F_trunc = std::vector<double>(F_trunc.begin() + F_trunc.size()/2 - truncSize/2, F_trunc.begin() + F_trunc.size()/2 + truncSize/2 + 1);
				}
				return result;
			}
		}
	}
}
