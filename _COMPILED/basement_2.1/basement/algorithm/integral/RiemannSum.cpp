#include "basement/algorithm/integral/RiemannSum.h"

#include <stdexcept>
#include <basement/math/Sequence.h>

namespace basement
{
	namespace algorithm
	{
		namespace integral
		{
			template<class Tx, class Tf>
			std::complex<double> RiemannSum::integrate(const std::vector<Tx>& x, const std::vector<Tf>& f) const
			{
				if (x.size() < 2 || f.size() == 0)
					throw std::invalid_argument("basement::algorithm::integral::RiemannSum: x size must be >= 2, f size must be >= 1");

				std::complex<double> integral = 0.0;
				if (x.size()-1 == f.size())
				{
					//x type is endpoints
					for (size_t i = 0; i < f.size(); i++)
					{
						integral += f[i] * (x[i+1] - x[i]);
					}
				}
				else if (x.size() == f.size())
				{
					//x type is center
					for (size_t i = 0; i < f.size()-1; i++)
					{
						integral += (x[i+1] - x[i]) * (f[i+1] + f[i]) / 2.0;
					}
				}
				else
				{
					throw std::invalid_argument("basement::algorithm::integral::RiemannSum: x size is not valid for f size");
				}
				return integral;
			}

			double RiemannSum::operator() (const std::vector<double>& x, const std::vector<double>& f) const
			{
                auto xBehavior = basement::math::sequence::monotonicity::find(x);
                if ( xBehavior!= basement::math::sequence::Monotonicity::strictly_decreasing && xBehavior!= basement::math::sequence::Monotonicity::strictly_increasing)
					throw std::invalid_argument("basement::algorithm::integral::RiemannSum: x vector values must be strictly monotonic");

				return integrate(x,f).real();
			}

            double RiemannSum::operator() (const std::vector<double>& x, const std::function<double(const double& x)>& F) const
            {
                auto xBehavior = basement::math::sequence::monotonicity::find(x);
                if ( xBehavior!= basement::math::sequence::Monotonicity::strictly_decreasing && xBehavior!= basement::math::sequence::Monotonicity::strictly_increasing)
                    throw std::invalid_argument("basement::algorithm::integral::RiemannSum: x vector values must be strictly monotonic");

                std::vector<double> f;
                for (const auto& xVal : x)
                    f.push_back(F(xVal));

                return integrate(x,f).real();
            }

			std::complex<double> RiemannSum::operator() (const std::vector<double>& x, const std::vector<std::complex<double>>& f) const
			{
                auto xBehavior = basement::math::sequence::monotonicity::find(x);
                if ( xBehavior!= basement::math::sequence::Monotonicity::strictly_decreasing && xBehavior!= basement::math::sequence::Monotonicity::strictly_increasing)
                    throw std::invalid_argument("basement::algorithm::integral::RiemannSum: x vector values must be strictly monotonic");

				return integrate(x,f).real();
			}

			std::complex<double> RiemannSum::operator() (const std::vector<std::complex<double>>& x, const std::vector<double>& f) const
			{
				return integrate(x,f);
			}

			std::complex<double> RiemannSum::operator() (const std::vector<std::complex<double>>& x, const std::vector<std::complex<double>>& f) const
			{
				return integrate(x,f);
			}

			template<class Tx, class Ty>
			std::complex<double> RiemannSum::integrate(const std::vector<Tx>& x, const std::vector<Tx>& y, const std::function<std::complex<double>(const Tx& x, const Ty& y)>& F)
			{
				if (x.size() < 2 || y.size() < 2)
					throw std::invalid_argument("basement::algorithm::integral::RiemannSum: x and y sizes must be >= 2");

				std::complex<double> integral = 0.0;
				for (size_t i = 0; i < y.size()-1; i++)
				{
					const double& dy = y[i+1] - y[i];
					const double& Y = y[i] + dy / 2.0;

					for (size_t j = 0; j < x.size()-1; j++)
					{
						const double& dx = x[j+1] - x[j];
						const double& X = x[j] + dx / 2.0;

						integral += dx * dy * F(X, Y);
					}
				}

				return integral;
			}

			std::complex<double> RiemannSum::operator() (const std::vector<double>& x, const std::vector<double>& y, const std::function<std::complex<double>(const double& x, const double& y)>& F) const
			{
                auto xBehavior = basement::math::sequence::monotonicity::find(x);
                if ( xBehavior!= basement::math::sequence::Monotonicity::strictly_decreasing && xBehavior!= basement::math::sequence::Monotonicity::strictly_increasing)
                    throw std::invalid_argument("basement::algorithm::integral::RiemannSum: x vector values must be strictly monotonic");

                auto yBehavior = basement::math::sequence::monotonicity::find(y);
                if ( yBehavior!= basement::math::sequence::Monotonicity::strictly_decreasing && yBehavior!= basement::math::sequence::Monotonicity::strictly_increasing)
                    throw std::invalid_argument("basement::algorithm::integral::RiemannSum: y vector values must be strictly monotonic");

				return integrate(x, y, F);
			}

			double RiemannSum::operator() (const std::vector<double>& x, const std::vector<double>& y, const std::function<double(const double& x, const double& y)>& F) const
			{
                auto xBehavior = basement::math::sequence::monotonicity::find(x);
                if ( xBehavior!= basement::math::sequence::Monotonicity::strictly_decreasing && xBehavior!= basement::math::sequence::Monotonicity::strictly_increasing)
                    throw std::invalid_argument("basement::algorithm::integral::RiemannSum: x vector values must be strictly monotonic");

                auto yBehavior = basement::math::sequence::monotonicity::find(y);
                if ( yBehavior!= basement::math::sequence::Monotonicity::strictly_decreasing && yBehavior!= basement::math::sequence::Monotonicity::strictly_increasing)
                    throw std::invalid_argument("basement::algorithm::integral::RiemannSum: y vector values must be strictly monotonic");

				const std::function<std::complex<double>(const double& x, const double& y)> F_complex = [&](const double& x, const double& y)
				{
					return F(x,y);
				};

				return integrate(x, y, F_complex).real();
			}
		}
	}
}
