#ifndef BASEMENT_ALGORITHM_WIDTH_LASTRIDGE_H
#define BASEMENT_ALGORITHM_WIDTH_LASTRIDGE_H


#include "basement/Basement_global.h"

#include <vector>

namespace basement
{
	namespace algorithm
	{
		namespace width
		{
			class LastRidge
			{
				private:
					LastRidge();

				public:
					//Determines dx width as distance between positions where function y last time equals to max(y) * level, where max(y) defined on x
					static double find(const std::vector<double>& x, const std::vector<double>& y, const double level);
			};
		}
	}
}

#endif // BASEMENT_ALGORITHM_WIDTH_LASTRIDGE_H
