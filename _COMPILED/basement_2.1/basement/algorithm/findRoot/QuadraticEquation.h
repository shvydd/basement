#ifndef BASEMENT_AlGORITHM_FINDROOT_QUADRATICEQUATION_H
#define BASEMENT_AlGORITHM_FINDROOT_QUADRATICEQUATION_H


#include "basement/Basement_global.h"

#include <vector>

namespace basement
{
	namespace algorithm
	{
		namespace findRoot
		{
			class QuadraticEquation
			{
				private:
					QuadraticEquation();

				public:
					static std::vector<double> solve(const double a, const double b, const double c);

					//Returns nonempty vector only if two different real roots exist
					static std::vector<double> solveReturnOnlyTwoRoots(const double a, const double b, const double c);

			};
		}
	}
}

#endif // BASEMENT_AlGORITHM_FINDROOT_QUADRATICEQUATION_H
