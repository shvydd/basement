#include "basement/algorithm/findRoot/QuadraticEquation.h"

#include <cmath>
#include <stdexcept>

namespace basement
{
	namespace algorithm
	{
		namespace findRoot
		{
			QuadraticEquation::QuadraticEquation() {}

			std::vector<double> QuadraticEquation::solve(const double a, const double b, const double c)
			{
				std::vector<double> roots;

				if ( a == 0)
				{
					if (b == 0)
					{
						if (c != 0)
							throw std::invalid_argument("basement::algorithm::findRoot::QuadraticEquation: const = 0");

						return roots;
					}

					roots.push_back(-c/b);
					return roots;
				}

				double D = b*b - 4.0*a*c;

				if (D > 0)
				{
					double sqrtD = std::sqrt(D);

					roots.push_back( (-b-sqrtD)/(2.0*a) );
					roots.push_back( (-b+sqrtD)/(2.0*a) );
				}
				else
				{
					if (D == 0)
					{
						roots.push_back( -b /(2.0*a) );
					}
				}

				return roots;
			}

			std::vector<double> QuadraticEquation::solveReturnOnlyTwoRoots(const double a, const double b, const double c)
			{
				std::vector<double> roots;
				double D = b*b - 4.0*a*c;

				if (D > 0)
				{
					double sqrtD = std::sqrt(D);

					roots.push_back( (-b-sqrtD)/(2.0*a) );
					roots.push_back( (-b+sqrtD)/(2.0*a) );
				}
				return roots;
			}
		}
	}
}
