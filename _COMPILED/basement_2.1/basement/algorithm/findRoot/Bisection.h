#ifndef BASEMENT_AlGORITHM_FINDROOT_BISECTION_H
#define BASEMENT_AlGORITHM_FINDROOT_BISECTION_H


#include "basement/Basement_global.h"

#include <functional>
#include <limits>

namespace basement
{
	namespace algorithm
	{
		namespace findRoot
		{
			double bisection
			(
			const std::function<double(const double&)>& f,
			const double y,
			const double absoluteYtolerance,
			double xMin,
			double xMax,
			size_t maxIterations = 10000
			);
		}
	}
}

#endif /* BASEMENT_AlGORITHM_FINDROOT_BISECTION_H */

