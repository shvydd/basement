#ifndef BASEMENT_CONTAINER_COLUMNS_H
#define BASEMENT_CONTAINER_COLUMNS_H


#include "basement/Basement_global.h"

#include <vector>
#include <utility>
#include <string>
#include <limits>

namespace basement
{
	namespace container
	{
        /**
         * @brief The ColumnsDouble class keeps double data columns and its descriptions (captions).
         * Data columns are aligned to the first row. Captions can't contain new line symbol.
         * Captions must not contain newlines and tabs.
         */
		class ColumnsDouble
		{
        public:
            ColumnsDouble(std::string description = "");

            /**
             * @brief Adds column and returns its index.
             */
            size_t addColumn(std::string caption, std::vector<double> content);

            const std::pair<std::string, std::vector<double>>& getColumn(const size_t col) const;
            void setColumn(const size_t col, std::string caption, std::vector<double> content);
            void deleteColumn(const size_t col);

            const std::string& getDescription() const;
            void setDescription(const std::string& description);

            size_t getColNumber() const;
            size_t getRowNumber() const;

            std::string toXMLString
            (
            const int  precision = std::numeric_limits<double>::max_digits10,
            const bool scientific = true
            ) const;

            /**
             * @brief Returns false, is some tags are missing or aren't closed, or if specified number of rows is not actual.
             */
            static bool parseXMLString(std::string s, ColumnsDouble& entity);
            static ColumnsDouble parseXMLString(std::string s);

            std::string toRawTextColumns(const bool description, const bool captions, const std::string newLineMark = "\n", const std::string delimiter = "\t") const;
            static ColumnsDouble parseRawTextColumns(std::string text, const bool hasDescription, const bool hasCaptions, const std::string newLineMark = "\n", const std::string delimiter = "\t");

        private:
            std::string description;
            std::vector< std::pair<std::string, std::vector<double>> > columns;

            size_t rows;

            static const std::string CLASS_NAME;
            static const std::string XML_ROOT_TAG;
            static const std::string XML_DESCRIPTION_TAG;
            static const std::string XML_CAPTIONS_TAG;
            static const std::string XML_ROWSNUM_TAG;
            static const std::string XML_COLUMNS_TAG;
		};
	}
}

#endif // BASEMENT_CONTAINER_COLUMNS_H
