#include "basement/container/Distribution3D.h"
#include "basement/extension/string.h"
#include <complex>

namespace basement
{
	namespace container
	{
		template<class DataUnit>
		Distribution3D<DataUnit>::Distribution3D
		(
		const std::string& _description,
		const std::string& _rowsGridCaption,
		const std::vector<double>& _rowsGrid,
		const std::string& _colsGridCaption,
		const std::vector<double>& _colsGrid,
		const std::string& _laysGridCaption,
		const std::vector<double>& _laysGrid
		)
		: description(_description),
		rowsGridCaption(basement::extension::string_::removeNewlinesAndTabs(_rowsGridCaption)),
		rowsGrid(_rowsGrid),
		rows(_rowsGrid.size()),
		colsGridCaption(basement::extension::string_::removeNewlinesAndTabs(_colsGridCaption)),
		colsGrid(_colsGrid),
		cols(_colsGrid.size()),
		laysGridCaption(basement::extension::string_::removeNewlinesAndTabs(_laysGridCaption)),
		laysGrid(_laysGrid),
		lays(_laysGrid.size()),
		matrix(rows, cols, std::vector<DataUnit>(lays))
		{}

		template<class DataUnit>
		DataUnit& Distribution3D<DataUnit>::at(const size_t row, const size_t col, const size_t lay)
		{
			if (row >= rows || col >= cols || lay >= lays)
			{
				throw std::invalid_argument("basement::container::Distribution3D::at: number of columns, rows or layers is out of range");
			}
			return matrix.at(row, col)[lay];
		}

		template<class DataUnit>
		const DataUnit& Distribution3D<DataUnit>::at(const size_t row, const size_t col, const size_t lay) const
		{
			if (row >= rows || col >= cols || lay >= lays)
			{
				throw std::invalid_argument("basement::container::Distribution3D::at: number of columns, rows or layers is out of range");
			}
			return matrix.at(row, col)[lay];
		}

		template<class DataUnit>
		std::vector<DataUnit>& Distribution3D<DataUnit>::at(const size_t row, const size_t col)
		{
			return matrix.at(row,col);
		}

		template<class DataUnit>
		const std::vector<DataUnit>& Distribution3D<DataUnit>::at(const size_t row, const size_t col) const
		{
			return matrix.at(row, col);
		}

		template<class DataUnit>
		basement::container::MatrixStorage<DataUnit> Distribution3D<DataUnit>::getLay(const size_t lay, const DataUnit defaultDataUnit) const
		{
			if (lay >= lays)
				throw std::invalid_argument("basement::container::Distribution3D::getLay: lay index is out of range");

			basement::container::MatrixStorage<DataUnit> layMatrix(rows, cols, defaultDataUnit);

			for (size_t row = 0; row < rows; row++)
			{
				for (size_t col = 0; col < rows; col++)
				{
					layMatrix.at(row, col) = matrix.at(row, col)[lay];
				}
			}

			return layMatrix;
		}

		template<class DataUnit>
		std::string Distribution3D<DataUnit>::getRowsGridCaption() const
		{
			return rowsGridCaption;
		}

		template<class DataUnit>
		std::string Distribution3D<DataUnit>::getColsGridCaption() const
		{
			return colsGridCaption;
		}

		template<class DataUnit>
		std::string Distribution3D<DataUnit>::getLaysGridCaption() const
		{
			return laysGridCaption;
		}

		template<class DataUnit>
		std::vector<double> Distribution3D<DataUnit>::getRowsGrid() const
		{
			return rowsGrid;
		}

		template<class DataUnit>
		std::vector<double> Distribution3D<DataUnit>::getColsGrid() const
		{
			return colsGrid;
		}

		template<class DataUnit>
		std::vector<double> Distribution3D<DataUnit>::getLaysGrid() const
		{
			return laysGrid;
		}

		template<class DataUnit>
		void Distribution3D<DataUnit>::setLaysGrid(const std::vector<double>& grid)
		{
			if (laysGrid.size() != grid.size())
				throw std::invalid_argument("basement::container::Distribution3D::setLaysGrid: new grid size != old grid size");

			laysGrid = grid;
		}

		template<class DataUnit>
		size_t Distribution3D<DataUnit>::getRowsNumber() const
		{
			return rows;
		}

		template<class DataUnit>
		size_t Distribution3D<DataUnit>::getColsNumber() const
		{
			return cols;
		}

		template<class DataUnit>
		size_t Distribution3D<DataUnit>::getLaysNumber() const
		{
			return lays;
		}

		template<class DataUnit>
		std::string Distribution3D<DataUnit>::getDescription() const
		{
			return description;
		}

		template<class DataUnit>
		void Distribution3D<DataUnit>::setDescription(const std::string& string)
		{
			description = string;
		}
	}
}

template class basement::container::Distribution3D<double>;
template class basement::container::Distribution3D<std::complex<double>>;
