#include "D1.h"

#include "basement/math/Sequence.h"
#include "basement/extension/double.h"
#include "basement/extension/string.h"
#include <cmath>
#include "basement/xml/PrimitiveParser.h"
#include "basement/extension/size_t.h"

namespace  basement
{
    namespace container
    {
        namespace grid
        {
            std::string D1::pointTypeToString(const PointType& type)
            {
                if (type == PointType::center)
                    return POINT_TYPE_CENTER_STRING;
                else if (type == PointType::endpoint)
                    return POINT_TYPE_ENDPOINT_STRING;
                else
                    throw std::invalid_argument(CLASS_NAME + "::pointTypeToString: unknown type.");
            }

            D1::PointType D1::stringToPointType(const std::string& str)
            {
                if (str == POINT_TYPE_CENTER_STRING)
                    return PointType::center;
                else if (str == POINT_TYPE_ENDPOINT_STRING)
                    return PointType::endpoint;
                else
                    throw std::invalid_argument(CLASS_NAME + "::stringToPointType: unknown string \"" + str + "\".");
            }

            const std::string D1::POINT_TYPE_CENTER_STRING = "center";
            const std::string D1::POINT_TYPE_ENDPOINT_STRING = "endpoint";
            const std::string D1::EXCEPTION_UNKNOWN_TYPE = ": unknown type.";
            const std::string D1::EXCEPTION_INVALID_INDEX = ": invalid index.";
            const std::string D1::CLASS_NAME = "basement::container::grid::D1";

            std::pair<bool, std::string> D1::isSequenceValid(const std::vector<double>& sequence, const PointType& type)
            {
                if ( sequence.empty() )
                    return std::pair(false, "Sequence is empty.");

                if (type == PointType::endpoint && sequence.size() < 2)
                    return std::pair(false, "Grid type is " + POINT_TYPE_ENDPOINT_STRING + ", but sequence size < 2.");

                const auto monotonicity = basement::math::sequence::monotonicity::find(sequence);

                if (monotonicity != basement::math::sequence::Monotonicity::strictly_increasing)
                    return std::pair(false, "Sequence monotonicity is " + basement::math::sequence::monotonicity::toString(monotonicity) + ", but must be " + basement::math::sequence::monotonicity::toString(basement::math::sequence::Monotonicity::strictly_increasing) + ".");

                return std::pair(true, std::string());
            }

            D1::D1(std::string _description)
                : description(std::move(_description))
            {
                if (basement::extension::string_::containsNewlineOrTab(description))
                    throw std::invalid_argument(CLASS_NAME + ": description contains newline or tab.");

                endPoints = {-0.5, 0.5};
            }

            D1::D1(std::vector<double> centerPoints, const size_t intervalIndex, const double intervalLength, std::string _description)
                : description(std::move(_description))
            {
                if (basement::extension::string_::containsNewlineOrTab(description))
                    throw std::invalid_argument(CLASS_NAME + ": description contains newline or tab.");

                setCenterPoints(std::move(centerPoints), intervalIndex, intervalLength);
            }

            D1::D1(std::vector<double> endPoints, std::string _description)
                : description(std::move(_description))
            {
                if (basement::extension::string_::containsNewlineOrTab(description))
                    throw std::invalid_argument(CLASS_NAME + ": description contains newline or tab.");

                const auto valid = isSequenceValid(endPoints, PointType::endpoint);
                if ( !valid.first )
                    throw std::invalid_argument(CLASS_NAME + ": " + valid.second);

                this->endPoints = std::move(endPoints);
            }

            D1::D1(const double domainMin, const double domainMax, const size_t numOfIntervals, std::string _description)
                : description(std::move(_description))
            {
                if (basement::extension::string_::containsNewlineOrTab(description))
                    throw std::invalid_argument(CLASS_NAME + ": description contains newline or tab.");

                if (domainMin >= domainMax)
                    throw std::invalid_argument(CLASS_NAME + ": domain min >= domain max.");

                if (numOfIntervals == 0)
                    throw std::invalid_argument(CLASS_NAME + ": number of intervals = 0.");

                const double step = (domainMax - domainMin) / double(numOfIntervals);
                endPoints = std::vector<double>(numOfIntervals + 1);
                endPoints[0] = domainMin;
                for (size_t i = 1; i < numOfIntervals + 1; i++)
                {
                    endPoints[i] = domainMin + step * double(i);
                }
            }

            D1::D1(const size_t numOfIntervals, std::string _description)
                : description(std::move(_description))
            {
                if (basement::extension::string_::containsNewlineOrTab(description))
                    throw std::invalid_argument(CLASS_NAME + ": description contains newline or tab.");

                if (numOfIntervals == 0)
                    throw std::invalid_argument(CLASS_NAME + ": number of intervals = 0.");

                endPoints = std::vector<double>(numOfIntervals + 1);
                for (size_t i = 0; i < numOfIntervals + 1; i++)
                {
                    endPoints[i] = -0.5 + double(i);
                }
            }

            std::pair<bool, double> D1::isUniform() const
            {
                double stepMin = +std::numeric_limits<double>::infinity();
                double stepMax = -std::numeric_limits<double>::infinity();

                for (size_t i = 0; i < endPoints.size() - 1; i++)
                {
                    const double step = endPoints[i+1] - endPoints[i];

                    if (step > stepMax)
                        stepMax = step;

                    if (step < stepMin)
                        stepMin = step;
                }
                const double stepMean = getDomainLength() / double(getNumberOfIntervals());

                const double tolerance = std::numeric_limits<double>::epsilon();

                using namespace basement::extension::double_;

                if ( approximatelyEqual(stepMin, stepMean, tolerance) && approximatelyEqual(stepMax, stepMean, tolerance) )
                    return std::pair(true, stepMean);
                else
                    return std::pair(false, stepMean);
            }

            std::vector<double> D1::generateUniformSequence(const double domainMin, const double domainMax, const size_t numOfIntervals)
            {
                if (domainMin >= domainMax)
                    throw std::invalid_argument(CLASS_NAME + ": domain min >= domain max.");

                if (numOfIntervals == 0)
                    throw std::invalid_argument(CLASS_NAME + ": number of intervals = 0.");

                const double step = (domainMax - domainMin) / double(numOfIntervals);
                std::vector<double> endPoints = std::vector<double>(numOfIntervals + 1);
                endPoints[0] = domainMin;
                for (size_t i = 1; i < numOfIntervals + 1; i++)
                {
                    endPoints[i] = domainMin + step * double(i);
                }

                return endPoints;
            }

            const std::string& D1::getDescription() const
            {
                return description;
            }

            void D1::setDescription(const std::string &value)
            {
                if (basement::extension::string_::containsNewlineOrTab(value))
                    throw std::invalid_argument(CLASS_NAME + ": description contains newline or tab.");

                description = value;
            }

            const std::vector<double> D1::getCenterPoints() const
            {
                if (this->centerPoints.empty())
                {
                    centerPoints = std::vector<double>(endPoints.size() - 1);

                    for (size_t i = 0; i < endPoints.size() - 1; i++)
                        centerPoints[i] = (endPoints[i+1] + endPoints[i]) / 2.0;
                }
                return centerPoints;
            }

            void D1::clearCenterPoints() const
            {
                centerPoints.clear();
            }

            void D1::setCenterPoints(std::vector<double> centerPoints, const size_t intervalIndex, const double intervalLength)
            {
                if (intervalIndex >= centerPoints.size())
                    throw std::invalid_argument(CLASS_NAME + EXCEPTION_INVALID_INDEX);

                if (intervalLength <= std::numeric_limits<double>::epsilon())
                    throw std::invalid_argument(CLASS_NAME + ": intervalLength <= 0.");

                const auto valid = isSequenceValid(centerPoints, PointType::center);
                if ( !valid.first )
                    throw std::invalid_argument(CLASS_NAME + ": " + valid.second);

                std::vector<double> endPoints(centerPoints.size() + 1);
                endPoints[intervalIndex] = centerPoints[intervalIndex] - intervalLength/2.0;
                endPoints[intervalIndex + 1] = centerPoints[intervalIndex] + intervalLength/2.0;

                double lengthHalf = intervalLength/2.0;
                for (size_t index = intervalIndex; index < centerPoints.size() - 1; index++)
                {
                    lengthHalf = (centerPoints[index + 1] - centerPoints[index] - lengthHalf);
                    if (lengthHalf <= std::numeric_limits<double>::epsilon())
                        throw std::invalid_argument(CLASS_NAME + ": invalid interval length.");

                    endPoints[index + 2] = centerPoints[index + 1] + lengthHalf;
                }

                lengthHalf = intervalLength/2.0;
                for (size_t index = intervalIndex; index >= 1; index--)
                {
                    lengthHalf = (centerPoints[index] - centerPoints[index - 1] - lengthHalf);
                    if (lengthHalf <= std::numeric_limits<double>::epsilon())
                        throw std::invalid_argument(CLASS_NAME + ": invalid interval length.");

                    endPoints[index - 1] = centerPoints[index - 1] - lengthHalf;
                }

                this->centerPoints = std::move(centerPoints);
                this->endPoints = std::move(endPoints);
            }

            const std::vector<double>& D1::getEndPoints() const
            {
                return endPoints;
            }

            void D1::setEndPoints(std::vector<double> endPoints)
            {
                const auto valid = isSequenceValid(endPoints, PointType::endpoint);
                if ( !valid.first )
                    throw std::invalid_argument(CLASS_NAME + ": " + valid.second);

                this->endPoints = std::move(endPoints);
                this->centerPoints.clear();
            }

            std::pair<double, double> D1::getDomainEndpoints() const
            {
                return std::pair(endPoints.front(), endPoints.back());
            }

            double D1::getDomainLength() const
            {
                return endPoints.back() - endPoints.front();
            }

            double D1::getDomainCenter() const
            {
                return ( endPoints.front() + endPoints.back() ) / 2.0;
            }

            size_t D1::getNumberOfIntervals() const
            {
                return endPoints.size() - 1;
            }

            double D1::getIntervalLength(const size_t intervalIndex) const
            {
                if (intervalIndex >= endPoints.size() - 1)
                    throw std::invalid_argument(CLASS_NAME + EXCEPTION_INVALID_INDEX);

                return endPoints[intervalIndex + 1] - endPoints[intervalIndex];
            }

            double D1::getIntervalCenter(const size_t intervalIndex) const
            {
                if (intervalIndex >= endPoints.size() - 1)
                    throw std::invalid_argument(CLASS_NAME + EXCEPTION_INVALID_INDEX);

                return (endPoints[intervalIndex + 1] + endPoints[intervalIndex]) / 2.0;
            }

            std::pair<double, double> D1::getIntervalEndpoints(const size_t intervalIndex) const
            {
                if (intervalIndex >= endPoints.size() - 1)
                    throw std::invalid_argument(CLASS_NAME + EXCEPTION_INVALID_INDEX);

                return std::pair(endPoints[intervalIndex], endPoints[intervalIndex + 1]);
            }

            double D1::getMeanStep() const
            {
                return getDomainLength() / double(getNumberOfIntervals());
            }

            std::pair<D1::ValuePosition, size_t> D1::findIntervalIndex(const double& val) const
            {
                 if (std::isnan(val))
                     throw std::invalid_argument(CLASS_NAME + "::findIntervalIndex: val is nan.");

                 const double tolerance = std::numeric_limits<double>::epsilon();
                 using namespace basement::extension::double_;

                 if (approximatelyEqual(val, endPoints.front(), tolerance))
                     return std::pair(ValuePosition::onGrid, 0);

                 if (approximatelyEqual(val, endPoints.back(), tolerance))
                     return std::pair(ValuePosition::onGrid, endPoints.size() - 2);

                 const auto upperBoundIt = std::upper_bound(endPoints.begin(), endPoints.end(), val);
                 size_t upperBoundIndex = std::distance(endPoints.begin(), upperBoundIt); // index of first element that bigger than val.

                 if (upperBoundIndex == 0)
                     return std::pair(ValuePosition::tooSmall, std::numeric_limits<size_t>::quiet_NaN());

                 if (upperBoundIndex >= endPoints.size())
                     return std::pair(ValuePosition::tooBig, std::numeric_limits<size_t>::quiet_NaN());

                 return std::pair(ValuePosition::onGrid, upperBoundIndex - 1);
            }
        }
    }
}
