#ifndef BASEMENT_CONTAINER_GRID_D1_H
#define BASEMENT_CONTAINER_GRID_D1_H

#include "basement/Basement_global.h"

#include <vector>

namespace  basement
{
    namespace container
    {
        namespace grid
        {
            /**
             * @brief The grid::D1 class keeps strictly increasing sequence of double and grid description.
             * Description must not contain newlines and tabs.
             */
            class D1
            {
            public:
                /**
                 * @brief Type of 1-dimensional grid point sequence.
                 * Grid points of PointType::center correspond to centers of intervals. Number of intervals is number of center points.
                 * Grid points of PointType::endpoint correspond to endpoints of intervals. Number of intervals is number of end points - 1.
                 */
                enum class PointType {center, endpoint};
                static std::string pointTypeToString(const PointType& type);
                static PointType stringToPointType(const std::string& str);
                static const std::string POINT_TYPE_CENTER_STRING;
                static const std::string POINT_TYPE_ENDPOINT_STRING;

                /**
                 * @brief isPointsValid
                 * @param points
                 * @param type
                 * @return Returns false if points sequence is not valid for grid of type and desciption why it doesn't.
                 */
                static std::pair<bool, std::string> isSequenceValid(const std::vector<double>& pointSequence, const PointType& type);

                D1(std::string description = "");

                /**
                 * @brief D1 constructor using center points sequence.
                 * @param centerPoints Sequence of center points.
                 * @param intervalIndex The index defines the interval for which its length is known.
                 * @param intervalLength Length of interval at intervalIndex.
                 */
                D1(std::vector<double> centerPoints, const size_t intervalIndex, const double intervalLength, std::string description = "");

                D1(std::vector<double> endPoints, std::string description = "");

                /**
                 * @brief D1 constructor for uniform grid.
                 */
                D1(const double domainMin, const double domainMax, const size_t numOfIntervals, std::string description = "");

                /**
                 * @brief D1 constructor, making grid with center points [0; numOfIntervals - 1].
                 */
                D1(const size_t numOfIntervals, std::string description = "");

                /**
                 * @brief isUniform
                 * @return Returns true if grid is uniform and its mean step.
                 */
                std::pair<bool, double> isUniform() const;

                static std::vector<double> generateUniformSequence(const double domainMin, const double domainMax, const size_t numOfIntervals);

                const std::string& getDescription() const;
                void setDescription(const std::string &value);

                /**
                 * @brief Calculates centerPoints vector and returns it. Do not recalculate centerPoints, if it has been calculated previously.
                 */
                const std::vector<double> getCenterPoints() const;
                void setCenterPoints(std::vector<double> centerPoints, const size_t intervalIndex, const double intervalLength);

                /**
                 * @brief grid::D1 will keep only endPoints. centerPoints will be recalculated at first use of "getCenterPoints()".
                 */
                void clearCenterPoints() const;

                const std::vector<double>& getEndPoints() const;                
                void setEndPoints(std::vector<double> endPoints);

                std::pair<double, double> getDomainEndpoints() const;
                double getDomainLength() const;
                double getDomainCenter() const;

                /**
                 * @brief numberOfIntervals == endPoints.size - 1 == centerPoints.size.
                 */
                size_t getNumberOfIntervals() const;
                double getIntervalLength(const size_t intervalIndex) const;
                double getIntervalCenter(const size_t intervalIndex) const;
                std::pair<double, double> getIntervalEndpoints(const size_t intervalIndex) const;

                double getMeanStep() const;

                enum class ValuePosition {onGrid, tooSmall, tooBig};
                /**
                 * @brief Find index of the interval to which a value belongs.
                 */
                std::pair<ValuePosition, size_t> findIntervalIndex(const double& val) const;

                static const std::string EXCEPTION_UNKNOWN_TYPE;
                static const std::string EXCEPTION_INVALID_INDEX;
                static const std::string CLASS_NAME;

            private:
                std::string description;
                std::vector<double> endPoints;
                mutable std::vector<double> centerPoints;
            };
        }
    }
}

#endif // BASEMENT_CONTAINER_GRID_D1_H
