#ifndef BASEMENT_CONTAINER_SURFACEDOUBLE_H
#define BASEMENT_CONTAINER_SURFACEDOUBLE_H

#include "basement/Basement_global.h"

#include <string>
#include "basement/container/MatrixStorage.h"
#include "basement/container/grid/D1.h"
#include <limits>

namespace basement
{
	namespace container
	{
        /**
         * @brief The SurfaceDouble class keeps double matrix storage, grids and surface description.
         * Description must not contain newlines and tabs.
         */
		class SurfaceDouble
		{
        public:
            SurfaceDouble(grid::D1 xGrid, grid::D1 yGrid);

            SurfaceDouble();

            void setDescription(std::string description);
            std::string getDescription() const;

            const double& at(const size_t row, const size_t col) const;
            double& at(const size_t row, const size_t col);

            const grid::D1& getXGrid() const;
            const grid::D1& getYGrid() const;

            void setXGrid(grid::D1);
            void setYGrid(grid::D1);

            const MatrixStorage<double>& getMatrix() const;
            MatrixStorage<double>& getMatrix();
            void setMatrix(MatrixStorage<double> matrix);

            std::pair<double, std::pair<double, double>> findMaxAndItsCoordinates() const;
            std::pair<double, std::pair<double, double>> findMinAndItsCoordinates() const;

            std::string toXMLString
            (
            const int  precision = std::numeric_limits<double>::max_digits10,
            const bool scientific = true
            ) const;

            //Returns false, is some tags are missing or aren't closed, or if a grid does not correspond to matrix.
            static bool parseXMLString(std::string& xmlString, SurfaceDouble& entity);

        private:
            std::string description;

            grid::D1 xGrid; //Horizontal increasing axis. The smallest ones are located to the left.
            grid::D1 yGrid; //Vertical increasing axis. The smallest ones are located at the top.

            MatrixStorage<double> matrix;

            size_t cols;
            size_t rows;

            static const std::string CLASS_NAME;

            static const std::string XML_ROOT_TAG;
            static const std::string XML_DESCRIPTION_TAG;

            static const std::string XML_X_CAPTION_TAG;
            static const std::string XML_Y_CAPTION_TAG;

            static const std::string XML_ROWSNUM_TAG;
            static const std::string XML_COLSNUM_TAG;

            static const std::string XML_MATRIX_TAG;
		};
	}
}

#endif /* BASEMENT_CONTAINER_SURFACEDOUBLE_H */
