#ifndef BASEMENT_CONTAINER_DISTRIBUTION3D_H
#define BASEMENT_CONTAINER_DISTRIBUTION3D_H


#include "basement/Basement_global.h"

#include "basement/container/MatrixStorage.h"
#include <vector>
#include <string>

#include <boost/serialization/access.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/split_member.hpp>

namespace basement
{
	namespace container
	{
		template<class DataUnit>
        class Distribution3D
		{
        public:
            Distribution3D
            (
            const std::string& description,
            const std::string& rowsGridCaption,
            const std::vector<double>& rowsGrid,
            const std::string& colsGridCaption,
            const std::vector<double>& colsGrid,
            const std::string& laysGridCaption,
            const std::vector<double>& laysGrid
            );

            DataUnit& at(const size_t row, const size_t col, const size_t lay);
            const DataUnit& at(const size_t row, const size_t col, const size_t lay) const;

            std::vector<DataUnit>& at(const size_t row, const size_t col);
            const std::vector<DataUnit>& at(const size_t row, const size_t col) const;

            basement::container::MatrixStorage<DataUnit> getLay(const size_t lay, const DataUnit defaultDataUnit = DataUnit()) const;

            std::string getRowsGridCaption() const;
            std::string getColsGridCaption() const;
            std::string getLaysGridCaption() const;

            std::vector<double> getRowsGrid() const;
            std::vector<double> getColsGrid() const;
            std::vector<double> getLaysGrid() const;

            void setLaysGrid(const std::vector<double>& grid);

            size_t getRowsNumber() const;
            size_t getColsNumber() const;
            size_t getLaysNumber() const;

            std::string getDescription() const;
            void setDescription(const std::string& string);

    private:
        std::string description;

        std::string rowsGridCaption;
        std::vector<double> rowsGrid;
        size_t rows;

        std::string colsGridCaption;
        std::vector<double> colsGrid;
        size_t cols;

        std::string laysGridCaption;
        std::vector<double> laysGrid;
        size_t lays;

        basement::container::MatrixStorage<std::vector<DataUnit>> matrix;

        friend class boost::serialization::access;
        template<class Archive> void save(Archive & ar, const unsigned int version) const
        {
            (void) version;

            ar & description;

            ar & rowsGridCaption;
            ar & rowsGrid;
            ar & rows;

            ar & colsGridCaption;
            ar & colsGrid;
            ar & cols;

            ar & laysGridCaption;
            ar & laysGrid;
            ar & lays;

            ar & matrix;
        }

        template<class Archive> void load(Archive & ar, const unsigned int version)
        {
            (void) version;

            ar & description;

            ar & rowsGridCaption;
            ar & rowsGrid;
            ar & rows;
            if ( rows != rowsGrid.size())
                throw std::invalid_argument("basement::container::Distribution3D::load: Bad archive. rows != rowsGrid.size ");

            ar & colsGridCaption;
            ar & colsGrid;
            ar & cols;
            if ( cols != colsGrid.size())
                throw std::invalid_argument("basement::container::Distribution3D::load: Bad archive. cols != colsGrid.size ");

            ar & laysGridCaption;
            ar & laysGrid;
            ar & lays;
            if (lays != laysGrid.size())
                throw std::invalid_argument("basement::container::Distribution3D::load: Bad archive. lays != laysGrid.size() ");

            try
            {
                ar & matrix;
            }
            catch (std::invalid_argument& e)
            {
                throw std::invalid_argument("basement::container::Distribution3D::load: Bad archive. " + std::string(e.what()) );
            }

            if (rows != matrix.getRowsNumber())
                throw std::invalid_argument("basement::container::Distribution3D::load: Bad archive. Number of rows in grid no not match matrix rows");

            if (cols != matrix.getColsNumber())
                throw std::invalid_argument("basement::container::Distribution3D::load: Bad archive. Number of cols in grid no not match matrix cols");

            if (cols >= 1 && rows >= 1)
            {
                size_t matrix_lays = matrix.at(0,0).size();

                if (lays != matrix_lays)
                    throw std::invalid_argument("basement::container::Distribution3D::load: Bad archive. Number of lays in grid no not match matrix lays");
            }
        }

        BOOST_SERIALIZATION_SPLIT_MEMBER()

		};
	}
}

#endif // BASEMENT_CONTAINER_DISTRIBUTION3D_H
