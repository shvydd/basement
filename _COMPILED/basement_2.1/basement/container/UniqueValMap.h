#ifndef BASEMENT_CONTAINER_UNIQUEVALMAP_H
#define BASEMENT_CONTAINER_UNIQUEVALMAP_H


#include "basement/Basement_global.h"

#include <map>
#include <set>
#include <cassert>

namespace basement
{
	namespace container
	{
		template <
				class Key,
				class T,
				class KeyCompare = std::less<Key>,
				class KeyAllocator = std::allocator<std::pair<const Key, T> >,
				class TCompare = std::less<Key>,
				class TAllocator = std::allocator<T>
		>class UniqueMap
		{
				typedef std::pair<const Key, T> value_type  ;

			private:
				std::map<Key, T, KeyCompare, KeyAllocator> map;
				std::set<T, TCompare, TAllocator> valSet;

			public:
				//TODO Use TCompare and TAllocator

				UniqueMap()
				{}

				explicit UniqueMap( const KeyCompare& comp, TCompare val_comp, const KeyAllocator& alloc = KeyAllocator() )
					: map(comp, alloc), valSet(val_comp)
				{

				}

				explicit UniqueMap( const KeyCompare& comp, const KeyAllocator& alloc = KeyAllocator() )
					: map(comp, alloc)
				{

				}

				explicit UniqueMap( const KeyAllocator& alloc )
					: map(alloc)
				{

				}

				/*template< class InputIt >
				UniqueMap( InputIt first, InputIt last, const KeyCompare& comp = KeyCompare(), const KeyAllocator& alloc = KeyAllocator() )
					: map(first, last, comp, alloc)
				{

				}*/

				/*template< class InputIt >
				UniqueMap( InputIt first, InputIt last, const KeyAllocator& alloc )
					: map(first, last, alloc)
				{

				}*/

				UniqueMap( const UniqueMap& other )
					: map(other.map), valSet(other.valSet)
				{

				}

				UniqueMap( const UniqueMap& other, const KeyAllocator& alloc )
					: map(other.map, alloc), valSet(other.valSet)
				{

				}

				UniqueMap( UniqueMap&& other )
					: map(other.map), valSet(other.valSet)
				{

				}

				UniqueMap( UniqueMap&& other, const KeyAllocator& alloc )
					: map(other.map, alloc), valSet(other.valSet)
				{

				}

				/*UniqueMap( std::initializer_list<value_type> init, const KeyCompare& comp = KeyCompare(), const KeyAllocator& alloc = KeyAllocator() )
					: map(init, comp, alloc)
				{

				}*/

				/*UniqueMap( std::initializer_list<value_type> init, const KeyAllocator& alloc)
					: map(init, alloc)
				{

				}*/

				UniqueMap& operator=( const UniqueMap& other )
				{
					this->map = other.map;
					this->valSet = other.valSet;

					return *this;
				}

				UniqueMap& operator=( UniqueMap&& other )
				{
					this->map = std::move(other.map);
					this->valSet = std::move(other.valSet);

					return *this;
				}

				/*UniqueMap& operator=( std::initializer_list<value_type> ilist )
				{

				}*/

				KeyAllocator getKeyAllocator() const noexcept
				{
					return map.get_allocator();
				}

				const T& at( const Key& key ) const
				{
					return map.at(key);
				}

				auto begin() const noexcept
				{
					return map.begin();
				}

				auto cbegin() const noexcept
				{
					return map.cbegin();
				}

				auto end() const noexcept
				{
					return map.end();
				}

				auto cend() const noexcept
				{
					return map.cend();
				}

				auto rbegin() const noexcept
				{
					return map.rbegin();
				}

				auto crbegin() const noexcept
				{
					return map.crbegin();
				}

				auto rend() const noexcept
				{
					return map.rend();
				}

				auto crend() const noexcept
				{
					return map.crend();
				}

				bool empty() const noexcept
				{
					return map.empty();
				}

				auto size() const noexcept
				{
					return map.size();
				}

				auto max_size() const noexcept
				{
					return map.max_size();
				}

				void clear() noexcept
				{
					map.clear();
					valSet.clear();
				}

				enum class InsertStatus {KEY_EXIST, VAL_EXIST, INSERTED};

				// Returns iterator != end, if key is new or contained.
				auto insert( const value_type& value )
				{
					auto itSetVal = valSet.find(value.second);
					auto itMap = map.find(value.first);

					if (itSetVal != valSet.end())  // If the val already in map
					{
						return std::pair(itMap, InsertStatus::VAL_EXIST);
					}
					else // If the val is new
					{
						if (itMap != map.end()) //If map contains the key
						{
							return std::pair(itMap, InsertStatus::KEY_EXIST);
						}
						else // If the key is new
						{
							bool valInsert = valSet.insert(value.second).second;
							auto itMapInsert = map.insert(itMap, value);

							assert( valInsert && itMapInsert != map.end()); //Check if insertion should have been succesful, but it is not.

							return std::pair(itMapInsert, InsertStatus::INSERTED);
						}
					}
				}

				auto insert( value_type&& value )
				{
					auto itSetVal = valSet.find(value.second);
					auto itMap = map.find(value.first);

					if (itSetVal != valSet.end())  // If the val already in map
					{
						return std::pair(itMap, InsertStatus::VAL_EXIST);
					}
					else // If the val is new
					{
						if (itMap != map.end()) //If map contains the key
						{
							return std::pair(itMap, InsertStatus::KEY_EXIST);
						}
						else // If the key is new
						{
							auto itMapInsert = map.insert(itMap, std::move(value));
							bool valInsert = valSet.insert(itMapInsert->second).second;

							assert( valInsert && itMapInsert != map.end()); //Check if insertion should have been succesful, but it is not.

							return std::pair(itMapInsert, InsertStatus::INSERTED);
						}
					}
				}

				auto insert( typename std::map<Key, T, KeyCompare, KeyAllocator>::const_iterator hint, const value_type& value )
				{
					auto itSetVal = valSet.find(value.second);
					auto itMap = map.find(hint, value.first);

					if (itSetVal != valSet.end())  // If the val already in map
					{
						return std::pair(itMap, InsertStatus::VAL_EXIST);
					}
					else // If the val is new
					{
						if (itMap != map.end()) //If map contains the key
						{
							return std::pair(itMap, InsertStatus::KEY_EXIST);
						}
						else // If the key is new
						{
							bool valInsert = valSet.insert(value.second).second;
							auto itMapInsert = map.insert(itMap, value);

							assert( valInsert && itMapInsert != map.end()); //Check if insertion should have been succesful, but it is not.

							return std::pair(itMapInsert, InsertStatus::INSERTED);
						}
					}
				}

				/*auto erase( typename std::map<Key, T, KeyCompare, KeyAllocator>::const_iterator pos )
				{
					auto itMap = map.find(pos);

					if (itMap == map.end())
						return itMap;

					valSet.erase(itMap->second);
					return map.erase(itMap);
				}*/

				bool eraseUsingKey( const Key& key )
				{
					auto itMap = map.find(key);

					if (itMap == map.end())
						return false;

					valSet.erase(itMap->second);
					map.erase(itMap);
					return true;
				}

				bool eraseUsingVal(const T& val)
				{
					auto itValSet = valSet.find(val);

					if (itValSet == valSet.end())
						return false;

					auto itMap = map.begin();
					for (itMap = map.begin(); itMap < map.end(); itMap++)
					{
						if (itMap->second == val)
						{
							break;
						}
					}

					map.erase(itMap);
					valSet.erase(itValSet);
					return true;
				}

				auto findUsingKey( const Key& key ) const
				{
					return map.find(key);
				}

				// TODO why it does not compile?
				/*auto findUsingVal( const T& val ) const
				{
					auto itMap = map.begin();
					for (itMap = map.begin(); itMap < map.end(); itMap++)
					{
						if (itMap->second == val)
							return itMap;
					}
					return map.end();
				}*/

				const Key& findKeyUsingVal( const T& val ) const
				{
					for (const auto& pair : map)
					{
						if (pair.second == val)
							return pair.first;
					}

					throw std::invalid_argument("basement::container::UniqueValMap::findKeyUsingVal(& value): no value");
				}

				bool containsKey( const Key& key ) const
				{
					if (map.find(key) != map.end())
						return true;

					return false;
				}

				auto key_comp() const
				{
					return map.key_comp();
				}

				auto value_comp() const
				{
					return map.value_comp();
				}

				const std::map<Key, T, KeyCompare, KeyAllocator>& getMap() const
				{
					return map;
				}

				const std::set<T, TCompare, TAllocator>& getValSet() const
				{
					return valSet;
				}
		};


	}
}

#endif // BASEMENT_CONTAINER_UNIQUEVALMAP_H
