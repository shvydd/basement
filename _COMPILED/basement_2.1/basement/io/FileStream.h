#ifndef BASEMENT_IO_FILESTREAM_H
#define BASEMENT_IO_FILESTREAM_H

#include <stdexcept>
#include <fstream>
#include <filesystem>

namespace basement
{
	namespace io
	{
		class Application;

		class FileStreamException : public std::exception
		{
        public:
            explicit FileStreamException(const char* description) noexcept;
            explicit FileStreamException(const std::string& description) noexcept;
            ~FileStreamException() noexcept;

            const char* what() const noexcept;

        private:
            const std::string description;
		};

        class FileStream : public std::fstream
		{
        public:
            FileStream
            (
            const std::filesystem::path& path,
            std::ios_base::openmode mode,
            bool createFolderIfNotExist = true
            );

            ~FileStream();

            const std::filesystem::path& getPath() const;

        private:
            std::filesystem::path path;

            static const std::string EXCEPTION_OPENING;
            static const std::string EXCEPTION_CLOSING;
            static const std::string WARNING_FAIL;

            void close() = delete;
		};
	}
}

#endif /* BASEMENT_IO_FILESTREAM_H */
