#include "basement/io/TextOutput.h"

#include "basement/extension/double.h"
#include "basement/extension/string.h"
#include <iostream>

namespace basement
{
	namespace io
	{
        TextOutput::TextOutput
		(
        const std::list<std::filesystem::path>& filePaths,
		const bool _consoleOutput,
		const bool _truncFiles,
        const bool _createFoldersIfNotExist,
		const int  _indent
		)
        : consoleOutput(_consoleOutput), indent(_indent)
		{
            if (indent < 0)
                throw std::invalid_argument(EXCEPTION_INDENT_NEGATIVE);

			auto fileMode = std::ios::out;
            if (_truncFiles)
			{
				fileMode = std::ios_base::out | std::ios::trunc;
			}
			else
			{
				fileMode = std::ios_base::out | std::ios::app;
			}

            for (auto& filePath : filePaths)
			{
                files.push_back( new FileStream(filePath, fileMode, _createFoldersIfNotExist) );
			}            
		}

		TextOutput::TextOutput
		(
        const std::filesystem::path& filePath,
		const bool consoleOutput,
		const bool truncFiles,
        const bool createFolderIfNotExist,
		const int  indent
        ) :
        TextOutput
		(
        std::list<std::filesystem::path>(1, filePath),
		consoleOutput,
		truncFiles,
        createFolderIfNotExist,
		indent
		)
		{

		}

        TextOutput::TextOutput(const int  indent)
        :
        TextOutput
        (
        std::list<std::filesystem::path>(),
        true,
        false,
        false,
        indent
        )
        {

        }

		TextOutput::~TextOutput()
		{
			for (auto file : files)
			{
				delete file;
			}
		}

		void TextOutput::indentAdd() const
		{
			indent++;
		}

		void TextOutput::indentReduce() const
		{
			if (indent == 0)
                throw std::invalid_argument(EXCEPTION_INDENT_NEGATIVE);

			indent--;
		}

		int TextOutput::indentGet() const
		{
			return indent;
		}

		void TextOutput::indentSet(const int numberOfTabs) const
		{
			indent = numberOfTabs;
		}

		void TextOutput::print(const std::string& text) const
		{
			std::string formattedText = extension::string_::addIndent(text, indent);

            for (auto file : files)
            {
                *file << formattedText << std::flush;;
            }
			if (consoleOutput)
			{
				std::cout << formattedText << std::flush;
			}
		}

		void TextOutput::printLine(const std::string& text) const
		{
			std::string formattedText = extension::string_::addIndent(text, indent);

            for (auto file : files)
            {
                *file << formattedText << std::endl;
            }
            if (consoleOutput)
            {
                std::cout << formattedText << std::endl;
            }
		}

        const std::string TextOutput::EXCEPTION_INDENT_NEGATIVE =
        "basement::TextOutput: Indent is negative.";
	}
}
