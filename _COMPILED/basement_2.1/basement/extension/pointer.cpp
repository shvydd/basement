#include "basement/extension/pointer.h"

#include <sstream>

namespace basement
{
	namespace extension
	{
		namespace pointer_
		{
			std::string toString(void const *pointer)
			{
				std::ostringstream out;
				out << pointer;
				return out.str();
			}
		}
	}
}
