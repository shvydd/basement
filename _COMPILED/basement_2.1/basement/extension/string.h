#ifndef BASEMENT_EXTENSION_STRING_H
#define BASEMENT_EXTENSION_STRING_H

#include "basement/Basement_global.h"

#include <string>
#include <initializer_list>

namespace basement
{
	namespace extension
	{
		namespace string_
		{
            /**
             * @brief Adds indent (in number of tabs) to the whole text.
             */
			std::string addIndent(const std::string& str, const size_t tabNumber = 1);

            /**
             * @brief Adds indent (in number of tabs) to the whole text.
             */
			std::string& addIndent(std::string& str, const size_t tabNumber = 1);

            /**
             * @brief Removes indent of defined length (number of tabs). Throws exception, if a line in the string does not contain the indent.
             */
			std::string& removeIndent(std::string& str, const size_t tabNumber = 1);

            /**
             * @brief Finds first substring - token (determined as substring,
               sandwiched between beginning of the line and a delimeter, or string end) - in a string, cuts off
               the token and the bracing delimeter from the string and returns token.
             */
			std::string extractToken(std::string& str, const std::string& delimiter);

            /**
             * @brief Finds first substring - token (determined as substring,
               sandwiched between beginning of the line and a delimeter, or string end) - in a string, cuts off
               the token and the bracing delimeter from the string and returns token.
             */
			std::string extractToken(std::string& str, const std::vector<std::string>& delimiters);

            bool containsNewlineOrTab(const std::string& str);

			std::string removeNewlinesAndTabs(const std::string& str);
            std::string removeNewlinesAndTabs(std::string&& str);

			std::string removeCharacters(const std::string& str, const std::initializer_list<char>& chars);

			size_t toSize_t(const std::string& str);

			std::string toLowerCase(const std::string& str);
			std::string toLowerCase(std::string&& str);
			std::string toUpperCase(const std::string& str);
			std::string toUpperCase(std::string&& str);

			std::string removeExtension(const std::string& filename);
		}
	}
}

#endif /* BASEMENT_EXTENSION_STRING_H */
