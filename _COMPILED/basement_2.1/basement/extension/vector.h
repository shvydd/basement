#ifndef BASEMENT_EXTENSION_VECTOR_H
#define BASEMENT_EXTENSION_VECTOR_H

#include "basement/Basement_global.h"

#include <vector>
#include <stdexcept>

namespace basement
{
	namespace extension
	{
		namespace vector_
		{
			template <class T>
			std::vector<T> slice
			(
			const std::vector<T>& vector,
			const size_t startPos,
			const size_t endPos,
			const size_t stride
			)
			{
				size_t vectorSize = vector.size();
                if (startPos >= vectorSize)
					throw std::invalid_argument("basement::extension::vector::slice: start position is out of range.");

                if (endPos >= vectorSize)
					throw std::invalid_argument("basement::extension::vector::slice: end position is out of range.");

				if (startPos > endPos)
					throw std::invalid_argument("basement::extension::vector::slice: start position > end position.");

				std::vector<T> res;
                for (size_t i = startPos; i <= endPos; i += stride)
				{
					res.push_back(vector[i]);
				}
				return res;
            }
		}
	}
}

#endif // BASEMENT_EXTENSION_VECTOR_H
