#include "basement/extension/double.h"

#include <cmath>
#include <sstream>

namespace basement
{
	namespace extension
	{
		namespace double_
		{
			std::string toString(const double value, const int precision)
			{
				std::ostringstream out;
				out.precision(precision);
				out << std::fixed << value;
				return out.str();
			}

			std::string toString(const double value, const int precision, const bool discardZeros)
			{
				std::ostringstream out;
				out.precision(precision);
				out << std::fixed << value;

				if (discardZeros)
				{
					std::string result = out.str();

					if (std::stod(result) != 0.0)
					{
						return result;
					}
					else
					{
						return "0";
					}
				}
				else
				{
					return out.str();
				}
			}

            std::string toStringScientific
			(
            const double value,
			const int precision,
			const bool discardZeros
			)
			{
				std::ostringstream out;
				out.precision(precision);
                out << value;

				if (discardZeros)
				{
					std::string result = out.str();
					if (std::stod(result) != 0.0)
					{
						return result;
					}
					else
					{
						return "0";
					}
				}
				else
				{
					return out.str();
				}
			}

            bool isNull(const double& value, double epsilon)
			{
                if ( std::abs(value) < epsilon) return true;
				else return false;
			}

            bool approximatelyEqual(double a, double b, double epsilon)
            {
                return std::abs(a - b) <= ( (std::abs(a) < std::abs(b) ? std::abs(b) : std::abs(a)) * epsilon);
            }

            bool essentiallyEqual(double a, double b, double epsilon)
            {
                return std::abs(a - b) <= ( (std::abs(a) > std::abs(b) ? std::abs(b) : std::abs(a)) * epsilon);
            }

            bool definitelyGreaterThan(double a, double b, double epsilon)
            {
                return (a - b) > ( (std::abs(a) < std::abs(b) ? std::abs(b) : std::abs(a)) * epsilon);
            }

            bool definitelyLessThan(double a, double b, double epsilon)
            {
                return (b - a) > ( (std::abs(a) < std::abs(b) ? std::abs(b) : std::abs(a)) * epsilon);
            }
		}
	}
}
