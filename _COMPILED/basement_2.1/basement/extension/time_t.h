#ifndef BASEMENT_EXTENSION_TIME_T_H
#define BASEMENT_EXTENSION_TIME_T_H

#include "basement/Basement_global.h"

#include <ctime>

namespace basement
{
	namespace extension
	{
		namespace time_t_
		{
			extern const std::string format_ISO8601_DTZ_Interchange;
			extern const std::string format_ISO8601_DTZ;
			extern const std::string format_ISO8601_D;
			extern const std::string format_ISO8601_T;
			extern const std::string format_ISO8601_T_NoSec;

			std::string toGmtTimeString(const std::time_t& time, const std::string& format = format_ISO8601_DTZ);
			std::string toLocalTimeString(const std::time_t& time, const std::string& format = format_ISO8601_DTZ);

			std::time_t timeNow();

			std::string toTimeString(const int seconds);
		}
	}
}

#endif /* BASEMENT_EXTENSION_TIME_T_H */
