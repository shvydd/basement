#ifndef BASEMENT_EXTENSION_POINTER_H
#define BASEMENT_EXTENSION_POINTER_H

#include "basement/Basement_global.h"

namespace basement
{
	namespace extension
	{
		namespace pointer_
		{
			std::string toString(void const *);
		}
	}
}

#endif // BASEMENT_EXTENSION_POINTER_H
