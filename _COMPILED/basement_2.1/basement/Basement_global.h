#ifndef BASEMENT_GLOBAL_H
#define BASEMENT_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(BASEMENT_LIBRARY)
#  define BASEMENT_EXPORT Q_DECL_EXPORT
#else
#  define BASEMENT_EXPORT Q_DECL_IMPORT
#endif

#endif // BASEMENT_GLOBAL_H
