#include "basement/geometry/VectorAndBasis.h"

#include "basement/Constants.h"
#include "basement/extension/double.h"
#include <cmath>

namespace basement
{
	namespace geometry
	{
		const std::string Vector::CLASS_NAME = "basement::geometry::Vector";

		//TODO replace [] with iterators ?

		Vector::Vector()
		{
			setUndefined();
		}

		Vector::Vector(const double& x, const double& y, const double& z)
		{
			coordinates[0] = x;
			coordinates[1] = y;
			coordinates[2] = z;
		}

		Vector::Vector(const double& x, const double& y, const double& z, const Basis& basis)
		{
			*this = Vector(x,y,z).multiplyMatrixByThis(basis.getRotation()).sumToThis(basis.getTranslation());
		}

		Vector::Vector(const Vector& instance, const Basis& basis)
		{
			*this = instance.multiplyMatrixByThis(basis.getRotation()).sumToThis(basis.getTranslation());
		}

		Vector Vector::rotateNotTranslate(const Basis& basis) const
		{
			return this->multiplyMatrixByThis(basis.getRotation());
		}

		Vector& Vector::rotateToThis(const double& eulerX, const double& eulerY, const double& eulerZ)
		{
			auto rotationMatrix = generateRotationMatrix(eulerX, eulerY, eulerZ);
			*this = this->multiplyMatrixByThis(rotationMatrix);

			return *this;
		}

		Vector Vector::rotate(const double& eulerX, const double& eulerY, const double& eulerZ) const
		{
			auto rotationMatrix = generateRotationMatrix(eulerX, eulerY, eulerZ);
			return this->multiplyMatrixByThis(rotationMatrix);
		}

		Vector Vector::rotate(Vector axis, const double& angle) const
		{
			if (axis.isNull())
				throw std::invalid_argument("basement::geometry::Vector::rotate(const Vector& axis, const double& angle): axis is null");

			axis = axis.makeUnitVector();

			//Rodrigues' rotation formula
			return (*this)*std::cos(angle) + crossProduct(axis, *this)*std::sin(angle) + axis*(axis * (*this))*(1.0 - std::cos(angle));
		}

		Vector& Vector::rotateToThis(const Vector& axis, const double& angle)
		{
			*this = rotate(axis, angle);
			return *this;
		}

		Vector Vector::sum(const Vector& operand) const
		{
			return Vector
			(
			this->coordinates[0] + operand.coordinates[0],
			this->coordinates[1] + operand.coordinates[1],
			this->coordinates[2] + operand.coordinates[2]
			);
		}

		Vector Vector::difference(const Vector& operand) const
		{
			return Vector
			(
			this->coordinates[0] - operand.coordinates[0],
			this->coordinates[1] - operand.coordinates[1],
			this->coordinates[2] - operand.coordinates[2]
			);
		}

		Vector Vector::scaling(const double& operand) const
		{
			return Vector
			(
			this->coordinates[0] * operand,
			this->coordinates[1] * operand,
			this->coordinates[2] * operand
			);
		}

		Vector Vector::crossProduct(const Vector& operand) const
		{
			return Vector
			(
			this->coordinates[1]*operand.coordinates[2] - coordinates[2]*operand.coordinates[1],
			this->coordinates[2]*operand.coordinates[0] - coordinates[0]*operand.coordinates[2],
			this->coordinates[0]*operand.coordinates[1] - coordinates[1]*operand.coordinates[0]
			);
		}

		double Vector::dotProduct(const Vector& operand) const
		{
			return coordinates[0] * operand.coordinates[0] + coordinates[1] * operand.coordinates[1] + coordinates[2] * operand.coordinates[2];
		}

		Vector Vector::multiplyMatrixByThis(const Matrix& matrix) const
		{
			return Vector
			(
			coordinates[0]*matrix(0,0) + coordinates[1]*matrix(0,1) + coordinates[2]*matrix(0,2),
			coordinates[0]*matrix(1,0) + coordinates[1]*matrix(1,1) + coordinates[2]*matrix(1,2),
			coordinates[0]*matrix(2,0) + coordinates[1]*matrix(2,1) + coordinates[2]*matrix(2,2)
			);
		}

		double Vector::norm() const
		{
			return std::sqrt(coordinates[0]*coordinates[0] + coordinates[1]*coordinates[1] + coordinates[2]*coordinates[2]);
		}

		Vector Vector::projectionOnVector(const Vector& direction) const
		{
			//a.projectOn(b) = b.normalize() * dotProduct(a,b)/b.norm() = b * dotProduct(a,b)/b.norm()*b.norm() =
			//               = b * dotProduct(a,b)/dotProduct(b,b);
			double dotProductThisAndDirection = this->dotProduct(direction);
			double dotProductDirectionAndDirection = direction.dotProduct(direction);

			if (dotProductDirectionAndDirection == 0.0)
				return direction;

			return direction.scaling(dotProductThisAndDirection/dotProductDirectionAndDirection);
		}

		Vector Vector::projectionOnPlane(const Vector& normal) const
		{
			return *this - projectionOnVector(normal);
		}

		Vector Vector::makeUnitVector() const
		{
			return this->scaling(1.0 / this->norm());
		}

		Vector& Vector::orientThisToTheSameSemisphere(const Vector& vector)
		{
			const Vector projection = this->projectionOnVector(vector);
			if (projection.isCoDirected(vector))
				return *this;
			else
			{
				*this = -(*this);
				return *this;
			}
		}

		Vector& Vector::sumToThis(const Vector& operand)
		{
			coordinates[0] += operand.coordinates[0];
			coordinates[1] += operand.coordinates[1];
			coordinates[2] += operand.coordinates[2];
			return *this;
		}

		Vector& Vector::differenceToThis(const Vector& operand)
		{
			coordinates[0] -= operand.coordinates[0];
			coordinates[1] -= operand.coordinates[1];
			coordinates[2] -= operand.coordinates[2];
			return *this;
		}

		Vector& Vector::scalingToThis(const double& operand)
		{
			coordinates[0] = coordinates[0] * operand;
			coordinates[1] = coordinates[1] * operand;
			coordinates[2] = coordinates[2] * operand;
			return *this;
		}

		double Vector::getX() const
		{
			return coordinates[0];
		}

		double Vector::getY() const
		{
			return coordinates[1];
		}

		double Vector::getZ() const
		{
			return coordinates[2];
		}

		void Vector::setX(const double& operand)
		{
			coordinates[0] = operand;
		}

		void Vector::setY(const double& operand)
		{
			coordinates[1] = operand;
		}

		void Vector::setZ(const double& operand)
		{
			coordinates[2] = operand;
		}

		std::string Vector::toString() const
		{
			return "{" + std::to_string(coordinates[0]) + "; " + std::to_string(coordinates[1]) + "; " + std::to_string(coordinates[2]) + "}";
		}

		std::string Vector::toStringScientific(const int precision) const
		{
			return "{" + basement::extension::double_::toString(coordinates[0], true, precision) + "; " +
						 basement::extension::double_::toString(coordinates[1], true, precision) + "; " +
						 basement::extension::double_::toString(coordinates[2], true, precision) + "}";
		}

		Vector Vector::operator-() const
		{
			return Vector(-this->getX(), -this->getY(), -this->getZ());
		}

		bool Vector::operator==(const Vector& operand) const
		{
			if (this->getX() == operand.getX() && this->getY() == operand.getY() && this->getZ() == operand.getZ())
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		bool Vector::operator!=(const Vector& operand) const
		{
			return !this->operator==(operand);
		}

		bool Vector::isCollinear(const Vector& operand) const
		{
			double thisNorm = this->norm();
			double operandNorm = operand.norm();

			if (thisNorm == 0.0 || operandNorm == 0.0)
				return true;

			Vector nThis = this->scaling(1.0 / thisNorm);
			Vector nOper = operand.scaling(1.0 / operandNorm);


			//TODO Take into account real meaning epsilon
			if ( std::abs(std::abs(nThis.dotProduct(nOper)) - 1.0) < std::numeric_limits<double>::epsilon() * 10.0 )
				return true;

			return false;
		}

		bool Vector::isCoDirected(const Vector& operand) const
		{
			double thisNorm = this->norm();
			double operandNorm = operand.norm();

			if (thisNorm == 0.0 || operandNorm == 0.0)
				return true;

			Vector nThis = this->scaling(1.0 / thisNorm);
			Vector nOper = operand.scaling(1.0 / operandNorm);


			//TODO Take into account real meaning epsilon
			if ( std::abs(nThis * nOper - 1.0) < std::numeric_limits<double>::epsilon() * 10.0 )
				return true;

			return false;
		}

		bool Vector::isPerpendicular(const Vector& operand) const
		{
			double thisNorm = this->norm();
			double operandNorm = operand.norm();

			if (thisNorm == 0.0 || operandNorm == 0.0)
				return true;

			Vector nThis = this->scaling(1.0 / thisNorm);
			Vector nOper = operand.scaling(1.0 / operandNorm);

			//TODO Take into account real meaning epsilon
			if ( std::abs(std::abs(nThis.dotProduct(nOper))) < std::numeric_limits<double>::epsilon() * 2.0 )
				return true;

			return false;
		}

		bool Vector::isNull() const
		{
			if (norm() < std::numeric_limits<double>::epsilon()) return true;
			else return false;
		}

		Vector Vector::operator+(const Vector& operand) const
		{
			return this->sum(operand);
		}

		Vector Vector::operator-(const Vector& operand) const
		{
			return this->difference(operand);
		}

		Vector Vector::operator*(const double& mult) const
		{
			return this->scaling(mult);
		}

		double Vector::operator*(const Vector& operand) const
		{
			return this->dotProduct(operand);
		}

		double Vector::dotProduct(const Vector& v1, const Vector& v2)
		{
			return v1.dotProduct(v2);
		}

		Vector Vector::crossProduct(const Vector& v1, const Vector& v2)
		{
			return v1.crossProduct(v2);
		}

		void Vector::setUndefined()
		{
			coordinates[0] = std::numeric_limits<double>::quiet_NaN();
			coordinates[1] = std::numeric_limits<double>::quiet_NaN();
			coordinates[2] = std::numeric_limits<double>::quiet_NaN();
		}

		bool Vector::isUndefined() const
		{
			if
			(
			coordinates[0] == std::numeric_limits<double>::quiet_NaN() &&
			coordinates[1] == std::numeric_limits<double>::quiet_NaN() &&
			coordinates[2] == std::numeric_limits<double>::quiet_NaN()
			)
			{
				return true;
			}
			return false;
		}

		bool Vector::isNan() const
		{
			if
			(
			std::isnan(coordinates[0]) ||
			std::isnan(coordinates[1]) ||
			std::isnan(coordinates[2])
			)
			{
				return true;
			}
			return false;
		}

		// Returns angle, defined in interval [-pi; pi]
		double Vector::findAngleUsingSinAndCos(const double& cosA, const double& sinA)
		{
			const double modulus = cosA*cosA + sinA*sinA;

            if (basement::extension::double_::definitelyGreaterThan(modulus, 1.0, 1.0e-14)) //TODO Magic epsilon
				throw std::invalid_argument(CLASS_NAME + "::findAngleUsingSinAndCos: cos^2 + sin^2 > 1.0");

			const double sinNormed = sinA / std::sqrt(modulus);
			const double cosNormed = cosA / std::sqrt(modulus);

			/*if (cosA - 1 <= 1.0e-15)
				return 0.0;
			else if (-cosA - 1 <= 1.0e-15)
				return basement::constants::pi;*/

			if (sinNormed >= 0)
			{
				return acos(cosNormed);
			}
			else
			{
				return -acos(cosNormed);
			}
		}

		double Vector::distance(const Vector& a, const Vector& b)
		{
			return std::sqrt( a.getX()*b.getX() + a.getY()*b.getY() + a.getZ()*b.getZ() );
		}

		// Returns angle, defined in interval [0; pi]
		double Vector::angleBetween(Vector v1, Vector v2)
		{
			double v1Norm = v1.norm();
			double v2Norm = v2.norm();

			if(v1Norm == 0.0 || v2Norm == 0.0)
				throw std::invalid_argument(CLASS_NAME + "::angleBetween(Vector v1, Vector v2): vector modulus is null");

			v1.scalingToThis(1.0/v1Norm);
			v2.scalingToThis(1.0/v2Norm);

			Vector rotationAxis = v1.crossProduct(v2);

			double absSinRotationAngle = rotationAxis.norm();
			double cosRotationAngle = v1.dotProduct(v2);

			return findAngleUsingSinAndCos(cosRotationAngle, absSinRotationAngle);
		}

		// Returns angle, defined in interval [-pi; pi]
		double Vector::angleBetween(const Vector& v1, const Vector& v2, Vector axis)
		{
			auto v1Pr = v1.projectionOnPlane(axis);
			auto v2Pr = v2.projectionOnPlane(axis);

			double v1PrNorm = v1Pr.norm();
			double v2PrNorm = v2Pr.norm();
			double axisNorm = axis.norm();

			if(v1PrNorm == 0.0 || v2PrNorm == 0.0 || axisNorm == 0.0)
				throw std::invalid_argument(CLASS_NAME + "::angleBetween(const Vector& v1, const Vector& v2, Vector axis): projection of v1 or v2 on the plane with normal=axis is null either axis modulus is null");

			v1Pr.scalingToThis(1.0/v1PrNorm);
			v2Pr.scalingToThis(1.0/v2PrNorm);
			axis.scalingToThis(1.0/axisNorm);

			Vector rotationAxis = v1Pr.crossProduct(v2Pr);

			double absSinRotationAngle = rotationAxis.norm();
			double cosRotationAngle = v1Pr.dotProduct(v2Pr);

			double sinRotationAngle;
			if ( rotationAxis.isCoDirected(axis) )
				sinRotationAngle = absSinRotationAngle;
			else
				sinRotationAngle = -absSinRotationAngle;

			return findAngleUsingSinAndCos(cosRotationAngle, sinRotationAngle);
		}

		const Vector Vector::x = Vector(1, 0, 0);
		const Vector Vector::y = Vector(0, 1, 0);
		const Vector Vector::z = Vector(0, 0, 1);

		Matrix generateRotationMatrix(const double& alpha, const double& beta, const double& gamma)
		{
			Matrix Rx;
			Rx(0,0) = 1;
			Rx(0,1) = 0;
			Rx(0,2) = 0;

			Rx(1,0) =  0;
			Rx(1,1) =  std::cos(alpha);
			Rx(1,2) = -std::sin(alpha);

			Rx(2,0) = 0;
			Rx(2,1) = std::sin(alpha);
			Rx(2,2) = std::cos(alpha);

			Matrix Ry;
			Ry(0,0) = std::cos(beta);
			Ry(0,1) = 0;
			Ry(0,2) = std::sin(beta);

			Ry(1,0) = 0;
			Ry(1,1) = 1;
			Ry(1,2) = 0;

			Ry(2,0) = -std::sin(beta);
			Ry(2,1) =  0;
			Ry(2,2) =  std::cos(beta);

			Matrix Rz;
			Rz(0,0) =  std::cos(gamma);
			Rz(0,1) = -std::sin(gamma);
			Rz(0,2) =  0;

			Rz(1,0) = std::sin(gamma);
			Rz(1,1) = std::cos(gamma);
			Rz(1,2) = 0;

			Rz(2,0) = 0;
			Rz(2,1) = 0;
			Rz(2,2) = 1;

			return boost::numeric::ublas::prec_prod(Rz, Matrix(boost::numeric::ublas::prec_prod(Ry, Rx)));
		}

		Matrix generateInverseRotationMatrix(const double& _alpha, const double& _beta, const double& _gamma)
		{
			const double alpha = -_alpha;
			const double beta = -_beta;
			const double gamma = -_gamma;

			Matrix Rx;
			Rx(0,0) = 1;
			Rx(0,1) = 0;
			Rx(0,2) = 0;

			Rx(1,0) =  0;
			Rx(1,1) =  std::cos(alpha);
			Rx(1,2) = -std::sin(alpha);

			Rx(2,0) = 0;
			Rx(2,1) = std::sin(alpha);
			Rx(2,2) = std::cos(alpha);

			Matrix Ry;
			Ry(0,0) = std::cos(beta);
			Ry(0,1) = 0;
			Ry(0,2) = std::sin(beta);

			Ry(1,0) = 0;
			Ry(1,1) = 1;
			Ry(1,2) = 0;

			Ry(2,0) = -std::sin(beta);
			Ry(2,1) =  0;
			Ry(2,2) =  std::cos(beta);

			Matrix Rz;
			Rz(0,0) =  std::cos(gamma);
			Rz(0,1) = -std::sin(gamma);
			Rz(0,2) =  0;

			Rz(1,0) = std::sin(gamma);
			Rz(1,1) = std::cos(gamma);
			Rz(1,2) = 0;

			Rz(2,0) = 0;
			Rz(2,1) = 0;
			Rz(2,2) = 1;

			return boost::numeric::ublas::prec_prod(Rx, Matrix(boost::numeric::ublas::prec_prod(Ry, Rz)));
		}

		Basis::Basis(const Vector& translation, const double& _alpha, const double& _beta, const double& _gamma)
		: translation(translation), alpha(getAngleFromMinusPiToPi(_alpha)), beta(getAngleFromMinusPiToPi(_beta)), gamma(getAngleFromMinusPiToPi(_gamma))
		{
			// Vector in prime basis = translation + rotation * vector in this basis.
			rotation = generateRotationMatrix(alpha, beta, gamma);
			inverseRotation = generateInverseRotationMatrix(alpha, beta, gamma);
		}

		Basis::Basis(const double& alpha, const double& beta, const double& gamma)
		: Basis(Vector(0,0,0), alpha, beta, gamma)
		{

		}

		Vector Basis::getTranslation() const
		{
			return translation;
		}

		Matrix Basis::getRotation() const
		{
			return rotation;
		}

		Matrix Basis::getInverseRotation() const
		{
			return inverseRotation;
		}

		std::string Basis::toString() const
		{
			return "{translation " + translation.toString() + "; Euler angles alpha(" +
			basement::extension::double_::toString(alpha*180.0/basement::constants::pi) + basement::constants::degree +
			"), beta(" +
			basement::extension::double_::toString(beta*180.0/basement::constants::pi) + basement::constants::degree +
			"), gamma(" +
			basement::extension::double_::toString(gamma*180.0/basement::constants::pi) + basement::constants::degree + ")}";
		}

		double Basis::getAngleFromMinusPiToPi(const double& angle)
		{
			double _2pi = 2.0 * basement::constants::pi;
			double n = std::floor(angle / _2pi );

			double result = angle - n * _2pi;

			if (result > basement::constants::pi)
				result -= _2pi;

			return result;
		}

		const Basis Basis::DEFAULT_BASIS = Basis(Vector(0.0, 0.0, 0.0), 0.0, 0.0, 0.0);
	}
}


basement::geometry::Vector operator*(const basement::geometry::Matrix& matrix, const basement::geometry::Vector& vector)
{
	return vector.multiplyMatrixByThis(matrix);
}

basement::geometry::Vector operator*(const double& factor, const basement::geometry::Vector& vector)
{
	return vector.scaling(factor);
}
