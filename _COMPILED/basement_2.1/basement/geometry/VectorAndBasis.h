#ifndef BASEMENT_GEOMETRY_VECTORANDBASIS_H
#define BASEMENT_GEOMETRY_VECTORANDBASIS_H


#include "basement/Basement_global.h"

#include <boost/numeric/ublas/matrix.hpp>
#include <limits>

namespace basement
{
	namespace geometry
	{
		typedef boost::numeric::ublas::bounded_matrix<double,3,3> Matrix;

		class Basis;

		class Vector
		{
			private:
				static const std::string CLASS_NAME;

				double coordinates[3];

			public:
				Vector();
				Vector(const double& x, const double& y, const double& z);

				//Creates a vector with coordinates, defined in the prime basis, if
				//coordinates in "basis" is {"x", "y", "z"}
				Vector(const double& x, const double& y, const double& z, const Basis& basis);

				Vector(const Vector&) = default;
				Vector(Vector&&) = default;
				Vector& operator= (const Vector&) = default;
				Vector& operator= (Vector&&) = default;

				//Creates a vector with coordinates, defined in the prime basis, if
				//the vector in "basis" is "instance"
				Vector(const Vector& instance, const Basis& basis);

				Vector rotateNotTranslate(const Basis& basis) const;
				Vector& rotateToThis(const double& eulerX, const double& eulerY, const double& eulerZ);
				Vector rotate(const double& eulerX, const double& eulerY, const double& eulerZ) const;
				Vector& rotateToThis(const Vector& axis, const double& angle);
				Vector rotate(Vector axis, const double& angle) const;

				Vector sum(const Vector&) const;
				Vector difference(const Vector&) const;
				Vector scaling(const double&) const;
				Vector crossProduct(const Vector&) const;
				double dotProduct(const Vector&) const;
				Vector multiplyMatrixByThis(const Matrix& matrix) const;
				double norm() const;
				Vector projectionOnVector(const Vector& vector) const;
				Vector projectionOnPlane(const Vector& normal) const;
				Vector makeUnitVector() const;

				Vector& orientThisToTheSameSemisphere(const Vector& vector);

				Vector& sumToThis(const Vector&);
				Vector& differenceToThis(const Vector&);
				Vector& scalingToThis(const double&);

				double getX() const;
				double getY() const;
				double getZ() const;

				void setX(const double&);
				void setY(const double&);
				void setZ(const double&);

				Vector operator-() const;
				bool operator==(const Vector&) const;
				bool operator!=(const Vector&) const;
				bool isCollinear(const Vector&) const;
				bool isCoDirected(const Vector&) const;
				bool isPerpendicular(const Vector&) const;

				bool isNull() const;

				Vector operator+(const Vector&) const;
				Vector operator-(const Vector&) const;
				Vector operator*(const double&) const;
				double operator*(const Vector&) const;

				static double dotProduct(const Vector&, const Vector&);
				static Vector crossProduct(const Vector&, const Vector&);

				// Returns angle, defined in interval [-pi; pi]
				static double findAngleUsingSinAndCos(const double& cosA, const double& sinA);

				static double distance(const Vector&, const Vector&);

				// Returns angle, defined in interval [0; pi]
				static double angleBetween(Vector v1, Vector v2);

				// Returns angle, defined in interval [-pi; pi]
				static double angleBetween(const Vector& v1, const Vector& v2, Vector axis);

				void setUndefined();
				bool isUndefined() const;
				bool isNan() const;

				std::string toString() const;
				std::string toStringScientific(const int precision = std::numeric_limits<double>::max_digits10) const;

				static const Vector x;
				static const Vector y;
				static const Vector z;
		};

		Matrix generateRotationMatrix(const double& alpha, const double& beta, const double& gamma);
		Matrix generateInverseRotationMatrix(const double& alpha, const double& beta, const double& gamma);

		class Basis
		{
			public:
				static const Basis DEFAULT_BASIS;

			private:
				//Translation and rotation are defined relative to the prime (default) basis.
				Vector translation;
				Matrix rotation;
				Matrix inverseRotation;

				// (alpha, beta, gamma) - Euler angles. General case rotation order - x, y, z.
				// (value, 0, 0) - turn around X (euler X)
				// (0, value, 0) - turn around Y (euler Y)
				// (0, 0, value) - turn around Z (euler Z)
				//https://en.wikipedia.org/wiki/Euler_angles
				//https://en.wikipedia.org/wiki/Rotation_matrix
				double alpha;
				double beta;
				double gamma;

			public:
				Basis(const Vector& translation, const double& alpha, const double& beta, const double& gamma);
				Basis(const double& alpha, const double& beta, const double& gamma);
				Basis(const Basis&) = default;
				Basis(Basis&&) = default;

				Vector getTranslation() const;
				Matrix getRotation() const;
				Matrix getInverseRotation() const;

				std::string toString() const;

				static double getAngleFromMinusPiToPi(const double& angle);
		};
	}
}

basement::geometry::Vector operator*(const double& factor, const basement::geometry::Vector& vector);
basement::geometry::Vector operator*(const basement::geometry::Matrix& matrix, const basement::geometry::Vector& vector);

#endif // BASEMENT_GEOMETRY_VECTORANDBASIS_H
