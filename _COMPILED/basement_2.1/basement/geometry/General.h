#ifndef BASEMENT_GEOMETRY_GENERAL_H
#define BASEMENT_GEOMETRY_GENERAL_H

#include "basement/geometry/VectorAndBasis.h"
#include <stdexcept>

namespace basement
{
	namespace geometry
	{
		typedef boost::numeric::ublas::bounded_matrix<double,4,4> Matrix_4_4;

		extern const std::string EXCEPTION_VECTOR_NORM_NULL;

		//Returns angle, defined in interval [0; 2*pi]
		double findAngleUsingSinAndCos(const double& cosA, const double& sinA);

		//Returns angle, defined in interval [-pi; pi]
		double findAngleFromMinusPiToPiUsingSinAndCos(const double& cosA, const double& sinA);

		Vector findOrthogonalUnitaryVector(const Vector& vector);

		//Returns vector which is directed as axis between input vectors and has norm value as angle between them.
		//Turn direction is clockwise if looking along returned vector direction.
		Vector findBetweenVectorsAngleAndAxis(Vector vector1, Vector vector2);

		//
		double findBetweenVectorsAngle(Vector vector1, Vector vector2);

		//Generates matrix that rotates vector (not a basis) on angle around axis
		Matrix generateRotationMatrix(const Vector& axis, const double& angle);
		Matrix generateRotationMatrix(const Vector& angleAxis);

		//Generates transformation matrix using rotation matrix (R) and shift vector (S).
		//transformationMatrix(R,S) = M(S)*M(R)
		Matrix_4_4 generateTransformationMatrix(const Matrix& rotation, const Vector& shift);
		//transformationMatrix(R,S) = M(R)*M(S)
		Matrix_4_4 generateTransformationMatrix(const Vector& shift, const Matrix& rotation);

		//Transform a matrix (A) of an extended quadratic form Q=trans(X)*A*X [ X is vector(x,y,z,1) ]
		//to matrix (newA) of form Q = trans(Y)*newA*Y, defined in the basis, where X=SY (S - vectorTransformMatrix).
		Matrix_4_4 transformExtendedQuadraticFormMatrix(const Matrix_4_4& formMatrixInOldBasis, const Matrix_4_4& vectorTransformMatrix);

		Vector projectVectorOnPlane(const Vector& vector, Vector planeNormal);
	}
}

#endif // BASEMENT_GEOMETRY_GENERAL_H
