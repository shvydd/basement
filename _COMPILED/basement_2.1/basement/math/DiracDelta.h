#ifndef BASEMENT_MATH_DIRACDELTA_H
#define BASEMENT_MATH_DIRACDELTA_H


#include "basement/Basement_global.h"

namespace basement
{
	namespace math
	{
		//Returns 1, if x==0, 0 otherwise.
		double diracDelta(const double& x);
	}
}

#endif // BASEMENT_MATH_DIRACDELTA_H
