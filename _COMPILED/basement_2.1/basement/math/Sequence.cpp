#include "Sequence.h"

#include <stdexcept>
#include "basement/extension/double.h"

namespace basement
{
    namespace math
    {
        namespace sequence
        {
            namespace monotonicity
            {
                extern const std::string STRICTLY_INCREASING_STRING = "strictly increasing";
                extern const std::string INCREASING_STRING = "increasing";
                extern const std::string STRICTLY_DECREASING_STRING = "strictly decreasing";
                extern const std::string DECREASING_STRING = "decreasing";
                extern const std::string NON_MONOTONIC_STRING = "non monotonic";
                extern const std::string CONSTANT_STRING = "constant";
                extern const std::string CLASS_NAME = "basement::math::sequence::monotonicity";

                std::string toString(const Monotonicity& monotonicity)
                {
                    if (monotonicity == Monotonicity::strictly_increasing)
                        return STRICTLY_INCREASING_STRING;
                    else if (monotonicity == Monotonicity::increasing)
                        return INCREASING_STRING;
                    else if (monotonicity == Monotonicity::strictly_decreasing)
                        return STRICTLY_DECREASING_STRING;
                    else if (monotonicity == Monotonicity::decreasing)
                        return DECREASING_STRING;
                    else if (monotonicity == Monotonicity::non_monotonic)
                        return NON_MONOTONIC_STRING;
                    else if (monotonicity == Monotonicity::constant)
                        return CONSTANT_STRING;
                    else
                        throw std::invalid_argument(CLASS_NAME + ": unknown type of monotonicity.");
                }

                Monotonicity find(const std::vector<double>& sequence, const double& tolerance)
                {
                    const size_t N = sequence.size();

                    if (N == 0)
                        throw std::invalid_argument(CLASS_NAME + "::findMonotonicity: sequence length = 0");

                    if (N == 1)
                        return Monotonicity::constant;

                    bool increasing = false;
                    bool decreasing = false;
                    bool strictly = true;

                    using namespace basement::extension::double_;
                    for (size_t i = 0; i < N-1; i++)
                    {
                        if ( definitelyGreaterThan(sequence[i+1], sequence[0], tolerance) )
                            increasing = true;
                        else if ( definitelyLessThan(sequence[i+1], sequence[0], tolerance) )
                            decreasing = true;
                        else // if ( approximatelyEqual(sequence[i+1], sequence[0], tolerance) )
                            strictly = false;
                    }

                    if (increasing && !decreasing)
                    {
                        if (strictly)
                            return Monotonicity::strictly_increasing;
                        else
                            return Monotonicity::increasing;
                    }

                    if (!increasing && decreasing)
                    {
                        if (strictly)
                            return Monotonicity::strictly_decreasing;
                        else
                            return Monotonicity::decreasing;
                    }

                    if (!increasing && !decreasing)
                        return Monotonicity::constant;

                    return Monotonicity::non_monotonic;
                }
            } // namespace monotonicity

            std::pair<bool, double> isUniform(const std::vector<double>& sequence, const double tolerance)
            {
                if (sequence.size() < 2)
                    throw std::invalid_argument("basement::math::sequence::isUniform: sequence size < 2.");

                const auto monotonicity = monotonicity::find(sequence);
                if (monotonicity == Monotonicity::non_monotonic || monotonicity == Monotonicity::increasing || monotonicity == Monotonicity::decreasing)
                {
                    double stepMean = 0.0;
                    for (size_t i = 0; i < sequence.size() - 1; i++)
                        stepMean += sequence[i+1] - sequence[i];

                    stepMean = stepMean / double(sequence.size() - 1);
                    return std::pair(false, stepMean);
                }

                if (monotonicity == Monotonicity::constant)
                    return std::pair(true, 0.0);

                double stepMin = +std::numeric_limits<double>::infinity();
                double stepMax = -std::numeric_limits<double>::infinity();

                for (size_t i = 0; i < sequence.size() - 1; i++)
                {
                    const double step = sequence[i+1] - sequence[i];

                    if (step > stepMax)
                        stepMax = step;

                    if (step < stepMin)
                        stepMin = step;
                }
                const double stepMean = (sequence.front() - sequence.back()) / double(sequence.size() - 1);

                using namespace basement::extension::double_;

                if ( approximatelyEqual(stepMin, stepMean, tolerance) && approximatelyEqual(stepMax, stepMean, tolerance) )
                    return std::pair(true, stepMean);
                else
                    return std::pair(false, stepMean);
            }
        } // namespace sequence
    } // namespace math
} // namespace basement
