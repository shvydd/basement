#include "basement/math/Extrema.h"

#include <stdexcept>

namespace basement
{
	namespace math
	{
        const std::string Extrema::CLASS_NAME = "basement::math::Extrema";

        std::vector<std::pair<size_t, Extrema::ExtremumType>> Extrema::find(const std::vector<double>& y)
		{
            if (y.size() < 3)
                throw std::invalid_argument(CLASS_NAME + ": sample size < 3");

            std::vector<std::pair<size_t, ExtremumType>> extrema;

            for (size_t i = 1; i < y.size() - 1; i++)
			{
                if (y[i-1] < y[i])
                {
                    //Possibly local maximum at i.
                    if (y[i] > y[i+1])
                    {
                        extrema.emplace_back(i, ExtremumType::max);
                    }
                    else if (y[i] == y[i+1])
                    {
                        //Possibly shelf on local maximum has begun.
                        size_t iShelf;
                        for (iShelf = i+1; iShelf < y.size() - 1; iShelf++)
                        {
                            if (y[iShelf] > y[iShelf + 1])
                            {
                                //The shelf is over and on local maximum. Last index of the shelf is iShelf;
                                extrema.emplace_back(i, ExtremumType::max);
                                extrema.emplace_back(iShelf, ExtremumType::max);
                                i = iShelf;
                                break;
                            }
                            else if (y[iShelf] < y[iShelf + 1])
                            {
                                //The shelf is over. The shelf is not on local maximum. y[i] is not local maximum.
                                i = iShelf;
                                break;
                            }
                            else // if (y[iShelf] == y[iShelf + 1])
                            {
                                //The shelf continues.
                                if (iShelf + 1 == y.size() - 1)
                                {
                                    // The shelf is on local maximum and is limited by domain. Last index of the shelf is iShelf + 1;
                                    extrema.emplace_back(i, ExtremumType::max);
                                    extrema.emplace_back(iShelf + 1, ExtremumType::max);
                                    i = iShelf;
                                    break;
                                }
                            }
                        } // On shelf iteration.
                    } // Possibly shelf on local maximum.
                    // else // if (y[i] < y[i+1]) // y[i] is not extremum.
                } //Possibly local maximum at i.
                else if (y[i-1] > y[i])
                {
                    //Possibly local minimum at i.
                    if (y[i] < y[i+1])
                    {
                        extrema.emplace_back(i, ExtremumType::min);
                    }
                    else if (y[i] == y[i+1])
                    {
                        //Possibly shelf on local minimum has begun.
                        size_t iShelf;
                        for (iShelf = i+1; iShelf < y.size() - 1; iShelf++)
                        {
                            if (y[iShelf] < y[iShelf + 1])
                            {
                                //The shelf is over and on local minimum. Last index of the shelf is iShelf;
                                extrema.emplace_back(i, ExtremumType::min);
                                extrema.emplace_back(iShelf, ExtremumType::min);
                                i = iShelf;
                                break;
                            }
                            else if (y[iShelf] > y[iShelf + 1])
                            {
                                //The shelf is over. The shelf is not on local minimum. y[i] is not local minimum.
                                i = iShelf;
                                break;
                            }
                            else // if (y[iShelf] == y[iShelf + 1])
                            {
                                //The shelf continues.
                                if (iShelf + 1 == y.size() - 1)
                                {
                                    // The shelf is on local minimum and is limited by domain. Last index of the shelf is iShelf + 1;
                                    extrema.emplace_back(i, ExtremumType::min);
                                    extrema.emplace_back(iShelf + 1, ExtremumType::min);
                                    i = iShelf;
                                    break;
                                }
                            }
                        } // On shelf iteration.
                    } // Possibly shelf on local minimum.
                    // else // if (y[i] > y[i+1]) // y[i] is not extremum.
                } //Possibly local minimum at i.
                else // if (y[i-1] == y[i])
                {
                    // Shelf has been discovered in the beginning.
                    size_t iShelf;
                    for (iShelf = i; iShelf < y.size() - 1; iShelf++)
                    {
                        if (y[iShelf] > y[iShelf + 1])
                        {
                            //The shelf is over and on local maximum. Last index of the shelf is iShelf;
                            extrema.emplace_back(i-1, ExtremumType::max);
                            extrema.emplace_back(iShelf, ExtremumType::max);
                            i = iShelf;
                            break;
                        }
                        else if (y[iShelf] < y[iShelf + 1])
                        {
                            //The shelf is over and on local minimum. Last index of the shelf is iShelf;
                            extrema.emplace_back(i-1, ExtremumType::min);
                            extrema.emplace_back(iShelf, ExtremumType::min);
                            i = iShelf;
                            break;
                        }
                        else // if (y[iShelf] == y[iShelf + 1])
                        {
                            //The shelf continues.
                            if (iShelf + 1 == y.size() - 1)
                            {
                                // The shelf is spanned through the whole domain. No extrema.
                                i = iShelf;
                                break;
                            }
                        }
                    } // On shelf iteration.
                }// Shelf has been discovered in the beginning.
			}

            if ( !extrema.empty() )
            {
                // There are no extrema. Thy y sequence is constant.
                return extrema;
            }

            // Now front and back points are included in extrema vector, only if they are on shelves.
            if (extrema.front().first != 0)
            {
                // Front point is not on a shelf.
                if (extrema.front().second == ExtremumType::max)
                    extrema.insert(extrema.begin(), std::pair<size_t, ExtremumType>(0, ExtremumType::min));
                else
                    extrema.insert(extrema.begin(), std::pair<size_t, ExtremumType>(0, ExtremumType::max));
            }
            if (extrema.back().first != y.size() - 1)
            {
                // Back point is not on a shelf.
                if (extrema.back().second == ExtremumType::max)
                    extrema.push_back(std::pair<size_t, ExtremumType>(y.size() - 1, ExtremumType::min));
                else
                    extrema.push_back(std::pair<size_t, ExtremumType>(y.size() - 1, ExtremumType::max));
            }

            return extrema;
		}
	}
}
