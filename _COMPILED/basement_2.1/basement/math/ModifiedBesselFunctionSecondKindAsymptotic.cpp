#include "basement/math/ModifiedBesselFunctionSecondKindAsymptotic.h"

#include <stdexcept>
#include <cmath>
#include "basement/Constants.h"

namespace basement
{
	namespace math
	{
		double modifiedBesselFunctionSecondKindAsymptotic(const double& order, const double& x, const size_t expansion_length)
		{
			(void) expansion_length;

			try
			{
				return std::cyl_bessel_k(order, x);
			}
			catch (const std::runtime_error& e)
			{
				//TODO make asymptotic expansion
				throw std::runtime_error("basement::math::modifiedBesselFunctionSecondKindAsymptotic: is not defined for large x");
			}
		}
	}
}
