#ifndef BASEMENT_MATH_MODIFIEDBESSELFUNCTIONSECONDKINDASYMPTOTIC_H
#define BASEMENT_MATH_MODIFIEDBESSELFUNCTIONSECONDKINDASYMPTOTIC_H


#include "basement/Basement_global.h"

#include <cstddef>

namespace basement
{
	namespace math
	{
		double modifiedBesselFunctionSecondKindAsymptotic(const double& order, const double& x, const size_t expansion_length);
	}
}

#endif // BASEMENT_MATH_MODIFIEDBESSELFUNCTIONSECONDKINDASYMPTOTIC_H
