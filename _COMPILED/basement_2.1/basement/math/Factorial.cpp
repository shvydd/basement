#include "basement/math/Factorial.h"

#include <cmath>
#include <stdexcept>

namespace basement
{
	namespace math
	{
		size_t factorial(const size_t n)
		{
			return std::tgamma(n+1);
		}

		double factorial(const double n)
		{
			if (n < 0.0)
				throw std::invalid_argument("basement::math::factorial: n < 0");

			return std::tgamma(n + 1.0);
		}
	}
}
