#ifndef BASEMENT_MATH_SIGN_H
#define BASEMENT_MATH_SIGN_H


#include "basement/Basement_global.h"

namespace basement
{
	namespace math
	{
		//Return -1 if argument < 0, 1 otherwise.
		double sign(const double);
	}
}

#endif /* BASEMENT_MATH_SIGN_H */

