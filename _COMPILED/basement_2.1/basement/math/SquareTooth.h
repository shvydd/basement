#ifndef BASEMENT_MATH_SQUARETOOTH_H
#define BASEMENT_MATH_SQUARETOOTH_H


#include "basement/Basement_global.h"

#include <stdexcept>

namespace basement
{
	namespace math
	{
		//Returns 1, if x belongs to [min; max]
		double squareTooth(const double& min, const double& max, const double& x);
	}
}

#endif // BASEMENT_MATH_SQUARETOOTH_H
