#ifndef BASEMENT_CONTAINER_MATRIXSTORAGE_H
#define BASEMENT_CONTAINER_MATRIXSTORAGE_H

#include "basement/Basement_global.h"

#include <vector>
#include <string>
#include <stdexcept>

#include <boost/serialization/access.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/complex.hpp>
#include <boost/serialization/split_member.hpp>

namespace basement
{
	namespace container
	{
        template <class DataUnit>
		class MatrixStorage
		{
			private:
				size_t rows;
				size_t cols;
				std::vector<std::vector<DataUnit>> matrix;

				friend class boost::serialization::access;
				template<class Archive> void save(Archive & ar, const unsigned int version) const
				{
					(void) version;

					ar & rows;
					ar & cols;
					ar & matrix;
				}

				template<class Archive> void load(Archive & ar, const unsigned int version)
				{
					(void) version;

					ar & rows;
					ar & cols;
					ar & matrix;

					if (matrix.size() != rows)
						throw std::invalid_argument("basement::container::MatrixStorage::load: Bad archive. rows number do not correspond to matrix.rows");

					if (matrix.size())
					{
						if( matrix.front().size() != cols )
							throw std::invalid_argument("basement::container::MatrixStorage::load: Bad archive. cols number do not correspond to matrix.cols");
					}
				}

				BOOST_SERIALIZATION_SPLIT_MEMBER()

			public:
				MatrixStorage(const size_t rows, const size_t cols, const DataUnit defaultDataUnitValue)
				: rows(rows), cols(cols), matrix( rows, std::vector<DataUnit>(cols, defaultDataUnitValue) )
				{

				}

				~MatrixStorage()
				{

				}

                bool isEmpty() const
                {
                    if (rows == 0 || cols == 0)
                        return true;

                    return false;
                }

				DataUnit& at(const size_t row, const size_t col)
				{
					if (row >= rows || col >= cols)
					{
                        throw std::invalid_argument("basement::container::MatrixStorage: number of column or row is out of range");
					}
					return matrix.at(row).at(col);
				}

				const DataUnit& at(const size_t row, const size_t col) const
				{
					if (row >= rows || col >= cols)
					{
                        throw std::invalid_argument("basement::container::MatrixStorage: number of column or row is out of range");
					}
					return matrix.at(row).at(col);
				}

                const std::vector<DataUnit>& getRow(const size_t row) const
                {
                    if (row >= rows)
                    {
                        throw std::invalid_argument("basement::container::MatrixStorage: number of row is out of range");
                    }
                    return matrix[row];
                }

                std::vector<DataUnit> getCol(const size_t col) const
                {
                    if (col >= cols)
                    {
                        throw std::invalid_argument("basement::container::MatrixStorage: number of col is out of range");
                    }

                    std::vector<DataUnit> column;
                    column.reserve(rows);
                    for (size_t row = 0; row < rows; row++)
                    {
                        column.push_back(matrix[row][col]);
                    }

                    return column;
                }

				size_t getRowsNumber() const
				{
					return rows;
				}

				size_t getColsNumber() const
				{
					return cols;
				}

                MatrixStorage& flipVertically()
				{
					for (size_t row = 0; row < rows/2; row++)
					{
						const std::vector<DataUnit> temp = matrix.at(row);
						const size_t conjugateRow = rows - 1 - row;

						matrix.at(row) = matrix.at(conjugateRow);
						matrix.at(conjugateRow) = temp;
					}                    
                    return *this;
				}

                MatrixStorage& flipHorizontally()
                {
                    for (size_t row = 0; row < rows; row++)
                    {
                        std::reverse(matrix.at(row).begin(), matrix.at(row).end());
                    }
                    return *this;
                }

                DataUnit findSumOfElements() const
                {
                    DataUnit sum = 0;
                    for (const auto& row : matrix)
                    {
                        for (const auto& val : row)
                        {
                            sum = sum + val;
                        }
                    }
                    return sum;
                }

                DataUnit findSumOfElementsIgnoreNaN() const
                {
                    DataUnit sum = 0;
                    for (const auto& row : matrix)
                    {
                        for (const auto& val : row)
                        {
                            if ( !std::isnan(val) )
                                sum = sum + val;
                        }
                    }
                    return sum;
                }

                MatrixStorage& multiplyElementsBy(DataUnit factor)
                {
                    for (auto& row : matrix)
                    {
                        for (auto& val : row)
                        {
                            val = val * factor;
                        }
                    }
                    return *this;
                }

                MatrixStorage& addToElements(DataUnit add)
                {
                    for (auto& row : matrix)
                    {
                        for (auto& val : row)
                        {
                            val = val + add;
                        }
                    }
                    return *this;
                }

                double findMax() const
                {
                    if (isEmpty())
                        throw std::invalid_argument("basement::container::MatrixStorage::findMax: matrix is empty.");

                    DataUnit max = matrix[0][0];
                    for (const auto& row : matrix)
                    {
                        for (const auto& val : row)
                        {
                            if ( val > max )
                               max = val;
                        }
                    }
                    return max;
                }

                double findMin() const
                {
                    if (isEmpty())
                        throw std::invalid_argument("basement::container::MatrixStorage::findMin: matrix is empty.");

                    DataUnit min = matrix[0][0];
                    for (const auto& row : matrix)
                    {
                        for (const auto& val : row)
                        {
                            if ( val < min )
                               min = val;
                        }
                    }
                    return min;
                }

                double findMaxIgnoreNaN() const
                {
                    if (isEmpty())
                        throw std::invalid_argument("basement::container::MatrixStorage::findMaxIgnoreNaN: matrix is empty.");

                    DataUnit max = matrix[0][0];
                    for (const auto& row : matrix)
                    {
                        for (const auto& val : row)
                        {
                            if (!std::isnan(val))
                            {
                                if (std::isnan(max) || val > max)
                                    max = val;
                            }
                        }
                    }
                    return max;
                }

                std::pair<double, std::pair<size_t, size_t>> findMaxAndItsPositionIgnoreNaN() const
                {
                    if (isEmpty())
                        throw std::invalid_argument("basement::container::MatrixStorage::findMaxAndItsPositionIgnoreNaN: matrix is empty.");

                    std::pair<size_t, size_t> position(0, 0);
                    DataUnit max = matrix[0][0];

                    for (size_t indexRow = 0; indexRow < rows; indexRow++)
                    {
                        for (size_t indexCol = 0; indexCol < cols; indexCol++)
                        {
                            const double& val = matrix[indexRow][indexCol];
                            if (!std::isnan(val))
                            {
                                if (std::isnan(max) || val > max)
                                {
                                    max = val;
                                    position = std::pair<size_t, size_t>(indexRow, indexCol);
                                }
                            }
                        }
                    }

                    if (std::isnan(max))
                    {
                        const auto& nan = std::numeric_limits<size_t>::quiet_NaN();
                        position = std::pair<size_t, size_t>(nan, nan);
                    }

                    return std::pair(max, position);
                }

                double findMinIgnoreNaN() const
                {
                    if (isEmpty())
                        throw std::invalid_argument("basement::container::MatrixStorage::findMaxIgnoreNaN: matrix is empty.");

                    DataUnit min = matrix[0][0];
                    for (const auto& row : matrix)
                    {
                        for (const auto& val : row)
                        {
                            if (!std::isnan(val))
                            {
                                if (std::isnan(min) || val < min)
                                    min = val;
                            }
                        }
                    }
                    return min;
                }

                std::pair<double, std::pair<size_t, size_t>> findMinAndItsPositionIgnoreNaN() const
                {
                    if (isEmpty())
                        throw std::invalid_argument("basement::container::MatrixStorage::findMinAndItsPositionIgnoreNaN: matrix is empty.");

                    std::pair<size_t, size_t> position(0, 0);
                    DataUnit min = matrix[0][0];

                    for (size_t indexRow = 0; indexRow < rows; indexRow++)
                    {
                        for (size_t indexCol = 0; indexCol < cols; indexCol++)
                        {
                            const double& val = matrix[indexRow][indexCol];
                            if (!std::isnan(val))
                            {
                                if (std::isnan(min) || val < min)
                                {
                                    min = val;
                                    position = std::pair<size_t, size_t>(indexRow, indexCol);
                                }
                            }
                        }
                    }

                    if (std::isnan(min))
                    {
                        const auto& nan = std::numeric_limits<size_t>::quiet_NaN();
                        position = std::pair<size_t, size_t>(nan, nan);
                    }

                    return std::pair(min, position);
                }
		};
	}
}

#endif /* BASEMENT_CONTAINER_MATRIXSTORAGE_H */
