#include "basement/container/ColumnsDouble.h"

#include "basement/extension/string.h"
#include "basement/extension/double.h"
#include <stdexcept>
#include "basement/xml/PrimitiveParser.h"
#include <deque>

namespace basement
{
	namespace container
	{
        ColumnsDouble::ColumnsDouble(std::string _description)
            : description(std::move(_description)), rows(0)
		{
            if (basement::extension::string_::containsNewlineOrTab(description))
                throw std::invalid_argument(CLASS_NAME + ": description contains newline or tab.");
		}

        size_t ColumnsDouble::addColumn(std::string caption, std::vector<double> content)
		{
            if (basement::extension::string_::containsNewlineOrTab(caption))
                throw std::invalid_argument(CLASS_NAME + ": caption contains newline or tab.");

            if (rows < content.size())
                rows = content.size();

            columns.push_back( std::pair<std::string, std::vector<double>>(std::move(caption), std::move(content)) );

            return columns.size() - 1;
        }

		const std::pair<std::string, std::vector<double>>& ColumnsDouble::getColumn(const size_t col) const
		{
			return columns.at(col);
		}

        void ColumnsDouble::setColumn(const size_t col, std::string caption, std::vector<double> content)
		{
            if (col >= columns.size())
                throw std::out_of_range(CLASS_NAME + ": column does not exist.");

            if (basement::extension::string_::containsNewlineOrTab(caption))
                throw std::invalid_argument(CLASS_NAME + ": caption contains newline or tab.");

            columns.at(col) = std::pair<std::string, std::vector<double>>(std::move(caption), std::move(content));

			size_t tempRows = columns.at(col).second.size();
			if ( tempRows >= rows)
			{
				rows = tempRows;
			}
			else
			{
				for (auto& column : columns)
				{
					tempRows = column.second.size();
					if (tempRows > rows)
					{
						rows = tempRows;
					}
				}
			}
		}

		void ColumnsDouble::deleteColumn(const size_t col)
		{
            if (col >= columns.size())
                throw std::out_of_range(CLASS_NAME + ": column does not exist.");

            columns.erase(columns.begin() + col);
		}

		const std::string& ColumnsDouble::getDescription() const
		{
			return description;
		}
		void ColumnsDouble::setDescription(const std::string& _description)
		{
            if (basement::extension::string_::containsNewlineOrTab(_description))
                throw std::invalid_argument(CLASS_NAME + ": description contains newline or tab.");

            description = _description;
		}

		size_t ColumnsDouble::getColNumber() const
		{
			return columns.size();
		}
		size_t ColumnsDouble::getRowNumber() const
		{
			return rows;
		}

		std::string ColumnsDouble::toXMLString
		(
		const int  _precision,
		const bool _scientific
		) const
		{
			std::string header = xml::PrimitiveParser::wrapContentWithTag(description, XML_DESCRIPTION_TAG) + "\n";

			std::string captionsString;
			if (columns.size() != 0)
			{
				for( size_t i = 0; i<columns.size()-1; i++ )
				{
					captionsString += columns[i].first + "\t";
				}
				captionsString += columns.back().first;
			}
			header += xml::PrimitiveParser::wrapContentWithTag(captionsString, XML_CAPTIONS_TAG) + "\n";
            header += xml::PrimitiveParser::wrapContentWithTag(extension::double_::toString(rows, 0), XML_ROWSNUM_TAG) + "\n";

			std::string res;
			if (columns.size() != 0)
			{
				for (size_t row = 0; row < rows; row++)
				{
					for (size_t col = 0; col < columns.size()-1; col++)
					{
						if ( row < columns[col].second.size() )
						{
                            if (_scientific)
                                res += extension::double_::toStringScientific(columns[col].second[row], _precision);
                            else
                                res += extension::double_::toString(columns[col].second[row], _precision);
						}
						res += "\t";
					}
					if ( row < columns.back().second.size() )
					{
                        if (_scientific)
                            res += extension::double_::toStringScientific(columns.back().second[row], _precision);
                        else
                            res += extension::double_::toString(columns.back().second[row], _precision);
					}
					if (row != rows-1)
						res += "\n";
				}
			}
			xml::PrimitiveParser::wrapContentWithTag(res, XML_COLUMNS_TAG);

			res.insert(0, header);
			xml::PrimitiveParser::wrapContentWithTag(res, XML_ROOT_TAG);
			return res;
		}

        bool ColumnsDouble::parseXMLString(std::string xmlString,  ColumnsDouble& entity)
		{
			std::string content;
			xml::PrimitiveParser::TagStatus tagStatus;
			bool complete = true;

			tagStatus = xml::PrimitiveParser::extractTagContent(xmlString, content, XML_ROOT_TAG);
			xmlString.clear();
			if (tagStatus == xml::PrimitiveParser::TagStatus::not_exist)
			{
                throw std::invalid_argument(CLASS_NAME + "::parseXMLString: string is not ColumnsDouble xml string.");
			}
			if (tagStatus == xml::PrimitiveParser::TagStatus::bad)
			{
				complete = false;
			}

			std::string description;
			tagStatus = xml::PrimitiveParser::extractTagContent(content, description, XML_DESCRIPTION_TAG);
			if (tagStatus != xml::PrimitiveParser::TagStatus::good)
			{
				complete = false;
			}
            description = basement::extension::string_::removeNewlinesAndTabs(std::move(description));

			std::string captionsString;
			tagStatus = xml::PrimitiveParser::extractTagContent(content, captionsString, XML_CAPTIONS_TAG);
			if (tagStatus != xml::PrimitiveParser::TagStatus::good)
			{
				complete = false;
			}
			std::deque<std::string> captions;
			while (captionsString.length() != 0)
			{
                captions.push_back(basement::extension::string_::removeNewlinesAndTabs(extension::string_::extractToken(captionsString, "\t")));
			}
			size_t cols = captions.size();

			std::string rowsNumberString;
			tagStatus = xml::PrimitiveParser::extractTagContent(content, rowsNumberString, XML_ROWSNUM_TAG);
			if (tagStatus != xml::PrimitiveParser::TagStatus::good)
			{
				complete = false;
			}
			size_t rows = extension::string_::toSize_t(rowsNumberString);

			std::string columnsString;
			tagStatus = xml::PrimitiveParser::extractTagContent(content, columnsString, XML_COLUMNS_TAG);
			if (tagStatus != xml::PrimitiveParser::TagStatus::good)
			{
				complete = false;
			}

			std::deque<std::vector<double>> columns(cols);
			std::string line;
			std::string num;
			size_t row = 0;
			while (columnsString.size() != 0)
			{
				line = extension::string_::extractToken(columnsString, "\n");
				for (size_t col = 0; col < cols; col++)
				{
					num = extension::string_::extractToken(line, "\t");
					if (num.length() != 0)
					{
						if (columns[col].size() != row)
                            throw std::invalid_argument(CLASS_NAME + "::parseXMLString: undefined values in datacolumn.");
						columns[col].push_back(std::stod(num));
					}
				}
				if (line.length() != 0)
				{
                    throw std::invalid_argument(CLASS_NAME + "::parseXMLString: more data columns than captions.");
				}
				row++;
			}
			if (row != rows)
			{
				complete = false;
			}			

            if (columns.size() != captions.size())
                throw std::invalid_argument(CLASS_NAME + "::parseXMLString: data columns number != captions number.");

			entity = ColumnsDouble(description);
			while(captions.size() != 0)
			{
                entity.addColumn( std::move(captions.front()), std::move(columns.front()) );
				captions.pop_front();
				columns.pop_front();
			}

			return complete;
        }

        ColumnsDouble ColumnsDouble::parseXMLString(std::string s)
        {
            ColumnsDouble entity;
            parseXMLString(std::move(s), entity);
            return entity;
        }

        std::string ColumnsDouble::toRawTextColumns(const bool description, const bool captions, const std::string newLineMark, const std::string delimiter) const
        {
            std::string res;

            if (description)
                res += this->description;

            if (captions)
            {
                for (size_t indexCol = 0; indexCol < columns.size() - 1; indexCol++)
                {
                    res += columns[indexCol].first + delimiter;
                }
                res += columns.back().first + newLineMark;
            }

            for (size_t indexRow = 0; indexRow < rows - 1; indexRow++)
            {
                for (size_t indexCol = 0; indexCol < columns.size() - 1; indexCol++)
                {
                    if (indexRow < columns[indexCol].second.size())
                        res += basement::extension::double_::toString(columns[indexCol].second[indexRow]) + delimiter;
                    else
                        res += delimiter;
                }

                if ( indexRow < columns.back().second.size() )
                    res += basement::extension::double_::toString(columns.back().second[indexRow]) + newLineMark;
                else
                    res += newLineMark;
            }

            const size_t indexRow = rows - 1;
            for (size_t indexCol = 0; indexCol < columns.size() - 1; indexCol++)
            {
                if (indexRow < columns[indexCol].second.size())
                    res += basement::extension::double_::toString(columns[indexCol].second[indexRow]) + delimiter;
                else
                    res += delimiter;
            }

            if ( indexRow < columns.back().second.size() )
                res += basement::extension::double_::toString(columns.back().second[indexRow]);

            return res;
        }

        ColumnsDouble ColumnsDouble::parseRawTextColumns(std::string text, const bool hasDescription, const bool hasCaptions, const std::string newLineMark, const std::string delimiter)
        {
            ColumnsDouble table;

            std::string description;
            std::vector< std::pair<std::string, std::vector<double>> > columns;

            if (hasDescription)
                description = basement::extension::string_::extractToken(text, newLineMark);

            if (hasCaptions)
            {
                std::string captionsLine = basement::extension::string_::extractToken(text, newLineMark);
                while ( !captionsLine.empty() )
                    columns.push_back(std::pair(basement::extension::string_::extractToken(captionsLine, delimiter), std::vector<double>()));
            }

            std::string line;
            std::string valString;
            size_t indexRow = 0;

            while (text.size() != 0)
            {
                line = basement::extension::string_::extractToken(text, newLineMark);
                while ( !line.empty() )
                {
                    size_t indexColumn = 0;
                    valString = extension::string_::extractToken(line, delimiter);

                    while ( !valString.empty() )
                    {
                        if (indexColumn == columns.size())
                        {
                            if (indexRow == 0)
                            {
                                // hasCaptions = false -> new data column discovered.
                                // hasCaptions = true ->  new data column discovered, but number of data columns > number of  captions.
                                // Let it parse even if number of captions less than number of data columns.
                                columns.push_back(std::pair(std::string(), std::vector<double>()));
                            }
                            else
                            {
                                //The column begins not from first row;
                                throw std::invalid_argument(CLASS_NAME + "::parseRawTextColumns: value in column is missing.");
                            }
                        }

                        if (valString.length() != 0)
                        {
                            double val;
                            try
                            {
                                val = std::stod(valString);
                            }
                            catch (...)
                            {
                                throw std::invalid_argument(CLASS_NAME + "::parseRawTextColumns: can't parse value.");
                            }

                            if (indexRow == columns[indexColumn].second.size())
                                columns[indexColumn].second.push_back(val);
                            else
                                throw std::invalid_argument(CLASS_NAME + "::parseRawTextColumns: value in column is missing.");
                        }
                        indexColumn++;
                    }
                    indexRow++;
                }
            }

            table.description = std::move(description);
            table.columns = std::move(columns);
            table.rows = indexRow;

            return table;
        }

        const std::string ColumnsDouble::CLASS_NAME = "basement::container::ColumnsDouble";

        const std::string ColumnsDouble::XML_ROOT_TAG = CLASS_NAME + " v2.0";
        const std::string ColumnsDouble::XML_DESCRIPTION_TAG = "description";
        const std::string ColumnsDouble::XML_CAPTIONS_TAG = "captions";
        const std::string ColumnsDouble::XML_ROWSNUM_TAG = "number of rows";
        const std::string ColumnsDouble::XML_COLUMNS_TAG = "columns";
    }
}

