#ifndef BASEMENT_CONTAINER_HISTOGRAM_H
#define BASEMENT_CONTAINER_HISTOGRAM_H

#include "basement/Basement_global.h"

#include <utility>
#include <vector>
#include <algorithm>
#include "basement/container/ColumnsDouble.h"

namespace basement
{
    namespace container
    {
        class Histogram
        {
        private:
            std::vector<std::pair<double, double>> binMargins;
            std::vector<size_t> counts;

            void orientMargins(std::pair<double, double>& binMargins) const
            {
               if (binMargins.first < binMargins.second)
                    return;

               if (binMargins.first == binMargins.second)
                   throw std::invalid_argument("basement::container::Histogram: bin left margin and right one are equal.");

                const double tempFirst = binMargins.first;
                binMargins.first = binMargins.second;
                binMargins.second = tempFirst;
            }

            class Comparator
            {
            public:
                bool operator()(const std::pair<double, double>& a, const std::pair<double, double>& b) const
                {
                    return a.first < b.first;
                }
            };
            static const Comparator comparator;

            void sortBinsUsingLeftMargin(std::vector<std::pair<double, double>>& binMargins) const
            {

                std::sort(binMargins.begin(), binMargins.end(), comparator);
            }

            bool isBinMarginsValid(const std::vector<std::pair<double, double>>& binMargins) const
            {
                for (size_t i = 1; i < binMargins.size(); i++)
                {
                    if (binMargins[i].first < binMargins[i-1].second)
                        return false;

                    if (binMargins[i].first == binMargins[i-1].first)
                        return false;
                }

                return true;
            }

        public:
            Histogram(std::vector<std::pair<double, double>> binMargins)
            {
                if (binMargins.size() < 1)
                     throw std::invalid_argument("basement::container::Histogram: invalid bin margins - must be at least one interval.");

                binMargins.shrink_to_fit();

                for (auto& margins : binMargins)
                    orientMargins(margins);

                sortBinsUsingLeftMargin(binMargins);

                if ( ! isBinMarginsValid(binMargins) )
                    throw std::invalid_argument("basement::container::Histogram: invalid bin margins - bins must be without crossections, right bin margin is treated as closed, right one is treated as opened.");

                this->binMargins = std::move(binMargins);
                this->counts = std::vector(this->binMargins.size(), size_t(0));
            }

            Histogram(std::vector<double> binMarginsSequental)
            {
                if (binMarginsSequental.size() < 2)
                     throw std::invalid_argument("basement::container::Histogram: invalid bin margins - must be at least two points.");

                binMarginsSequental.shrink_to_fit();

                std::vector<std::pair<double, double>> binMargins(binMarginsSequental.size() - 1);
                for (size_t i = 0; i < binMargins.size(); i++)
                {
                    binMargins[i].first = binMarginsSequental[i];
                    binMargins[i].second = binMarginsSequental[i+1];
                }

                 sortBinsUsingLeftMargin(binMargins);

                 if ( ! isBinMarginsValid(binMargins) )
                     throw std::invalid_argument("basement::container::Histogram: invalid bin margins - bins must be without crossections, right bin margin is treated as closed, right one is treated as opened.");

                 this->binMargins = std::move(binMargins);
                 this->counts = std::vector(this->binMargins.size(), size_t(0));
            }

            bool receiveValue(const double& val)
            {
                if (val < binMargins.front().first)
                    return false;

                std::pair<double, double> val_(val, 0.0);

                const auto biggerOrEqualThanIt = std::lower_bound(binMargins.begin(), binMargins.end(), val_, comparator);
                const size_t index = std::distance(binMargins.begin(), biggerOrEqualThanIt);

                if (index >= binMargins.size())
                    return false;

                counts[index]++;
                return true;
            }

            void receiveValues(const std::vector<double>& vals)
            {
                for (const auto& val : vals)
                    receiveValue(val);
            }

            basement::container::ColumnsDouble toColumnsDouble() const
            {
                basement::container::ColumnsDouble table("Histogram");

                std::vector<double> leftBinMargins, rightBinMargins, binMidVals, doubleCounts;
                leftBinMargins.reserve(binMargins.size());
                rightBinMargins.reserve(binMargins.size());
                binMidVals.reserve(binMargins.size());
                doubleCounts.reserve(binMargins.size());

                for (const auto& margins : binMargins)
                {
                    leftBinMargins.push_back(margins.first);
                    rightBinMargins.push_back(margins.second);
                    binMidVals.push_back( (margins.second + margins.first) / 2.0);
                }

                for (const auto& count : counts)
                    doubleCounts.push_back(count);

                table.addColumn("Left bin margin", leftBinMargins);
                table.addColumn("Right bin margin", rightBinMargins);
                table.addColumn("Midpoint of bin", binMidVals);
                table.addColumn("Counts", doubleCounts);

                return table;
            }
        };
    }
}

#endif // BASEMENT_CONTAINER_HISTOGRAM_H
