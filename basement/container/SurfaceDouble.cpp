#include "basement/container/SurfaceDouble.h"

#include "basement/extension/double.h"
#include "basement/extension/string.h"
#include "basement/xml/PrimitiveParser.h"
#include <stdexcept>

namespace basement
{
	namespace container
	{
        SurfaceDouble::SurfaceDouble(grid::D1 _xGrid, grid::D1 _yGrid)
        : xGrid(_xGrid), yGrid(_yGrid), matrix(0,0,0)
		{
            cols = xGrid.getNumberOfIntervals();
            rows = yGrid.getNumberOfIntervals();
			matrix = MatrixStorage<double>( rows, cols, std::numeric_limits<double>::quiet_NaN() );
		}

		SurfaceDouble::SurfaceDouble()
            : SurfaceDouble(grid::D1(), grid::D1())
		{

		}

        void SurfaceDouble::setDescription(std::string _description)
		{
            if (basement::extension::string_::containsNewlineOrTab(_description))
                throw std::invalid_argument(CLASS_NAME + ": description contains newline or tab.");

            description = std::move(_description);
		}

		std::string SurfaceDouble::getDescription() const
		{
			return description;
		}

		const double& SurfaceDouble::at(const size_t row, const size_t col) const
		{
			return matrix.at(row, col);
		}

		double& SurfaceDouble::at(const size_t row, const size_t col)
		{
			return matrix.at(row, col);
		}

        const grid::D1& SurfaceDouble::getXGrid() const
		{
			return xGrid;
        }

        const grid::D1& SurfaceDouble::getYGrid() const
		{
			return yGrid;
        }

        void SurfaceDouble::setXGrid(grid::D1 newXGrid)
        {
            if (newXGrid.getNumberOfIntervals() != this->xGrid.getNumberOfIntervals())
                throw std::invalid_argument(CLASS_NAME + "::setXGrid: matrix has different dimensions.");

            this->xGrid = std::move(newXGrid);
        }

        void SurfaceDouble::setYGrid(grid::D1 newYGrid)
        {
            if (newYGrid.getNumberOfIntervals() != this->yGrid.getNumberOfIntervals())
                throw std::invalid_argument(CLASS_NAME + "::setYGrid: matrix has different dimensions.");

            this->yGrid = std::move(newYGrid);
        }
		
		const MatrixStorage<double>& SurfaceDouble::getMatrix() const
		{
			return matrix;
        }

        MatrixStorage<double>& SurfaceDouble::getMatrix()
        {
            return matrix;
        }

        void SurfaceDouble::setMatrix(MatrixStorage<double> newMatrix)
        {
            if (newMatrix.getRowsNumber() != matrix.getRowsNumber() || newMatrix.getColsNumber() != matrix.getColsNumber())
                throw std::invalid_argument(CLASS_NAME + "::setMatrix: matrix has different dimensions.");

            matrix = std::move(newMatrix);
        }

        std::pair<double, std::pair<double, double>> SurfaceDouble::findMaxAndItsCoordinates() const
		{
            const auto maxAndIndices = matrix.findMaxAndItsPositionIgnoreNaN();
            const auto& max = maxAndIndices.first;
            const auto& indices = maxAndIndices.second;

            if (std::isnan(max))
            {
                const double& nan = std::numeric_limits<double>::quiet_NaN();
                return std::pair(nan, std::pair(nan, nan));
            }

            const double x = xGrid.getIntervalCenter(indices.second);
            const double y = yGrid.getIntervalCenter(indices.first);

            return std::pair(max, std::pair(x, y));
        }

		std::string SurfaceDouble::toXMLString(const int precision, const bool scientific) const
		{
            std::string header = xml::PrimitiveParser::wrapContentWithTag(description, XML_DESCRIPTION_TAG) + "\n";

            header += xml::PrimitiveParser::wrapContentWithTag(xGrid.getDescription(), XML_X_CAPTION_TAG) + "\n";
            header += xml::PrimitiveParser::wrapContentWithTag(yGrid.getDescription(), XML_Y_CAPTION_TAG) + "\n";

			header += xml::PrimitiveParser::wrapContentWithTag(extension::double_::toString(rows,0), XML_ROWSNUM_TAG) + "\n";
			header += xml::PrimitiveParser::wrapContentWithTag(extension::double_::toString(cols,0), XML_COLSNUM_TAG) + "\n";

            std::string res = "grids(endpoints)\t|";
			std::string xDelimeter = "_\t|";
            for (size_t col = 0; col < cols + 1; col++)
			{
                res += "\t" + extension::double_::toStringScientific(xGrid.getEndPoints()[col], precision);
                xDelimeter += "\t_";
            }
            res += "\n" + std::move(xDelimeter) + "\n";

			for (size_t row = 0; row < rows; row++)
			{
                res += extension::double_::toStringScientific(yGrid.getEndPoints()[row], precision) + "\t|";

				for (size_t col = 0; col < cols; col++)
				{
                    if (scientific)
                        res += "\t" + extension::double_::toStringScientific(matrix.at(row, col), precision);
                    else
                        res += "\t" + extension::double_::toString(matrix.at(row, col), precision);
				}
                if (row != rows-1)
                    res += "\n";
			}
            res += "\n" + extension::double_::toStringScientific(yGrid.getEndPoints()[rows], precision) + "\t|";

			xml::PrimitiveParser::wrapContentWithTag(res, XML_MATRIX_TAG);
			res.insert(0, header);

			xml::PrimitiveParser::wrapContentWithTag(res, XML_ROOT_TAG);
			return res;
		}

		bool SurfaceDouble::parseXMLString(std::string& xmlString, SurfaceDouble& entity)
		{
            const std::string FUNC_NAME = "::parseXMLString: ";

			std::string content;
			xml::PrimitiveParser::TagStatus tagStatus;
			bool complete = true;

			tagStatus = xml::PrimitiveParser::extractTagContent(xmlString, content, XML_ROOT_TAG);
			xmlString.clear();
			if (tagStatus == xml::PrimitiveParser::TagStatus::not_exist)
			{
                throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "string is not SurfaceDouble xml string.");
			}
			if (tagStatus == xml::PrimitiveParser::TagStatus::bad)
			{
				complete = false;
			}

			std::string description;
			tagStatus = xml::PrimitiveParser::extractTagContent(content, description, XML_DESCRIPTION_TAG);
			if (tagStatus != xml::PrimitiveParser::TagStatus::good)
			{
				complete = false;
			}
            description = basement::extension::string_::removeNewlinesAndTabs(std::move(description));

			std::string xCaption;
			tagStatus = xml::PrimitiveParser::extractTagContent(content, xCaption, XML_X_CAPTION_TAG);
			if (tagStatus != xml::PrimitiveParser::TagStatus::good)
			{
				complete = false;
			}
            xCaption = basement::extension::string_::removeNewlinesAndTabs(std::move(xCaption));

			std::string yCaption;
			tagStatus = xml::PrimitiveParser::extractTagContent(content, yCaption, XML_Y_CAPTION_TAG);
			if (tagStatus != xml::PrimitiveParser::TagStatus::good)
			{
				complete = false;
			}
            yCaption = basement::extension::string_::removeNewlinesAndTabs(std::move(yCaption));

			std::string rowsNumString;
			tagStatus = xml::PrimitiveParser::extractTagContent(content, rowsNumString, XML_ROWSNUM_TAG);
			if (tagStatus != xml::PrimitiveParser::TagStatus::good)
			{
                throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "\"" + XML_ROWSNUM_TAG + "\" tag is not good.");
			}
			size_t rows = extension::string_::toSize_t(rowsNumString);

			std::string colsNumString;
			tagStatus = xml::PrimitiveParser::extractTagContent(content, colsNumString, XML_COLSNUM_TAG);
			if (tagStatus != xml::PrimitiveParser::TagStatus::good)
			{
                throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "\"" + XML_COLSNUM_TAG + "\" tag is not good.");
			}
			size_t cols = extension::string_::toSize_t(colsNumString);

            basement::container::grid::D1 xGrid = basement::container::grid::D1(cols);
            xGrid.setDescription(xCaption);

            basement::container::grid::D1 yGrid = basement::container::grid::D1(rows);
            yGrid.setDescription(yCaption);

            entity = SurfaceDouble(std::move(xGrid), std::move(yGrid));
			entity.setDescription(description);

			std::string matrixString;
			tagStatus = xml::PrimitiveParser::extractTagContent(content, matrixString, XML_MATRIX_TAG);
			if (tagStatus != xml::PrimitiveParser::TagStatus::good)
			{
                throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "matrix tag is not good.");
			}

			std::string xGridString = extension::string_::extractToken(matrixString, "\n");
			std::string gridsWord = extension::string_::extractToken(xGridString, "\t");
			std::string yDelimeter = extension::string_::extractToken(xGridString, "\t");
			if (yDelimeter != "|")
                throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "invalid x grid line.");

            std::vector<double> xGridTemp(cols + 1, 0);

			std::string xVal;
            for (size_t col = 0; col < xGridTemp.size(); col++)
			{
				xVal = extension::string_::extractToken(xGridString, "\t");
				if (xVal.length() == 0)
                    throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "undefined x grid node value.");
                xGridTemp[col] = std::stod(xVal);
			}
			if (xGridString.length() != 0)
                throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "more x nodes than it should be.");

			std::string xDelimiter = extension::string_::extractToken(matrixString, "\n");
			if ( (xDelimiter.find("_\t|\t_")) == std::string::npos )
                throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "invalid horizontal delimiter.");

            entity.xGrid = basement::container::grid::D1(std::move(xGridTemp));


            std::vector<double> yGridTemp(rows + 1, 0);
			std::string line;
			std::string item;
			for (size_t row = 0; row < rows; row++)
			{
				line = extension::string_::extractToken(matrixString, "\n");

				item = extension::string_::extractToken(line, "\t");
				if (item.length() == 0)
                    throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "undefined y grid node value.");
                yGridTemp[row] = std::stod(item);

				item = extension::string_::extractToken(line, "\t");
				if (item != "|")
                    throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "invalid vertical delimiter.");

				for (size_t col = 0; col < cols; col++)
				{
					item = extension::string_::extractToken(line, "\t");
					if (item.length() == 0)
                        throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "undefined matrix value.");
					entity.at(row,col) = std::stod(item);
				}

				if (line.length() != 0)
                    throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "more matrix values in a row than it should be.");
			}
            line = extension::string_::extractToken(matrixString, "\n");

            item = extension::string_::extractToken(line, "\t");
            if (item.length() == 0)
                throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "undefined y grid node value.");
            yGridTemp[rows] = std::stod(item);

            item = extension::string_::extractToken(line, "\t");
            if (item != "|")
                throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "invalid vertical delimiter.");

			if (matrixString.length() != 0)
                throw std::invalid_argument(CLASS_NAME + FUNC_NAME + "more rows than it should be.");

            entity.yGrid = basement::container::grid::D1(std::move(yGridTemp));

			return complete;
		}

        const std::string SurfaceDouble::CLASS_NAME = "basement::container::SurfaceDouble";
        const std::string SurfaceDouble::XML_ROOT_TAG = CLASS_NAME + " v.2.0";
        const std::string SurfaceDouble::XML_DESCRIPTION_TAG = "description";
        const std::string SurfaceDouble::XML_X_CAPTION_TAG = "x caption";
        const std::string SurfaceDouble::XML_Y_CAPTION_TAG = "y caption";
        const std::string SurfaceDouble::XML_ROWSNUM_TAG = "number of rows";
        const std::string SurfaceDouble::XML_COLSNUM_TAG = "number of columns";
        const std::string SurfaceDouble::XML_MATRIX_TAG = "matrix";
	}
}
