#ifndef BASEMENT_XML_PRIMITIVEPARSER_H
#define BASEMENT_XML_PRIMITIVEPARSER_H

#include "basement/Basement_global.h"

#include <string>

namespace basement
{
	namespace xml
	{
		class PrimitiveParser
		{
            //FIXME Does not extract nested tags
            //FIXME Incorrectly extracts tag, if they are nonunique
            //FIXME Ignores nesting
        public:
            enum class TagStatus {good, bad, not_exist};

        private:
            PrimitiveParser();

        public:
            static std::string& wrapContentWithTag(std::string& content, const std::string& tag);
            static std::string wrapContentWithTag(const std::string& content, const std::string& tag);

            static TagStatus extractTagContent(std::string& xmlString, std::string& content, const std::string& tag);
            static TagStatus extractTagContent(const std::string& xmlString, std::string& content, const std::string& tag);
		};
	}
}

#endif // BASEMENT_XML_PRIMITIVEPARSER_H
