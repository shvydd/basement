#include "basement/xml/PrimitiveParser.h"

#include "basement/extension/string.h"

namespace basement
{
	namespace xml
	{
		PrimitiveParser::PrimitiveParser()
		{

		}

		std::string& PrimitiveParser::wrapContentWithTag(std::string& content, const std::string &tag)
		{
			content = "<" + tag + ">\n" + extension::string_::addIndent(content) + "\n</" + tag + ">";
			return content;
		}

		std::string PrimitiveParser::wrapContentWithTag(const std::string& content, const std::string& tag)
		{
			std::string contentCopy = content;
			return wrapContentWithTag(contentCopy, tag);
		}

		PrimitiveParser::TagStatus PrimitiveParser::extractTagContent(std::string& xmlString, std::string& content, const std::string& tag)
		{
			std::string startTag = "<" + tag + ">\n";
			std::string endTag = "\n</" + tag + ">";

			auto startTagPos = xmlString.find(startTag);
			if (startTagPos == std::string::npos)
			{
				return TagStatus::not_exist;
			}

			auto endTagPos = xmlString.rfind(endTag);
			auto contentLength = endTagPos - (startTagPos + startTag.length());
			auto eraseLength = endTagPos + endTag.length() - startTagPos;
			TagStatus tagStatus = TagStatus::good;
			if (endTagPos == std::string::npos)
			{
				tagStatus = TagStatus::bad;
				contentLength = xmlString.length() - (startTagPos + startTag.length());
				eraseLength = xmlString.length() - startTagPos;
			}

			//TODO Avoid copying.
			content = xmlString.substr(startTagPos + startTag.length(), contentLength);
			extension::string_::removeIndent(content);
			xmlString.erase(startTagPos, eraseLength);

			return tagStatus;
		}

		PrimitiveParser::TagStatus PrimitiveParser::extractTagContent(const std::string &xmlString, std::string &content, const std::string &tag)
		{
			std::string xmlStringCopy = xmlString;
			return extractTagContent(xmlStringCopy, content, tag);
		}
	}
}
