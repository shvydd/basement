#ifndef BASEMENT_CONSTANTS
#define BASEMENT_CONSTANTS

#include "basement/Basement_global.h"

#include <string>
#include <complex>

namespace basement
{
	namespace constants
	{
		extern const double pi;

		/*
		imaginary unit
		*/
		extern const std::complex<double> i;

		/*
		light speed in μm/fs
		*/
		extern const double c;

		extern const std::string degree;
		extern const std::string micrometer;
	}
}

#endif /* BASEMENT_CONSTANTS */
