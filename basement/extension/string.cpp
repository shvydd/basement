#include "basement/extension/string.h"
#include <stdexcept>
#include <sstream>
#include <algorithm>
#include <cctype>

namespace basement
{
	namespace extension
	{
		namespace string_
		{
			//Adds indent (in number of tabs) to the whole text
			std::string& addIndent(std::string& str, const size_t tabNumber)
			{
				if (tabNumber == 0)
				{
					return str;
				}
				const std::string tabString(tabNumber, '\t');

				str.insert(0,tabString);
				size_t pos = str.find('\n', tabNumber);
				while (pos != std::string::npos)
				{
					str.insert(++pos, tabString);
					pos += tabNumber;
					pos = str.find('\n', pos);
				}
				return str;
			}

			//Adds indent (in number of tabs) to the whole text
			std::string addIndent(const std::string& str, const size_t tabNumber)
			{
				std::string strCopy = str;
				return addIndent(strCopy, tabNumber);
			}

			std::string& removeIndent(std::string& str, const size_t tabNumber)
			{
				static const std::string NO_INDENT_IN_LINE = "basement::extension::string::removeIndent: some line does not contain indent of specified length.";
				const std::string tabString(tabNumber, '\t');

				size_t pos = 0;
				//Check and remove the indent in the first line.
				if (str.substr(pos, tabNumber) != tabString)
				{
					throw std::invalid_argument(NO_INDENT_IN_LINE);
				}
				str.erase(pos, tabNumber);
				//Check and remove indents further in the text.
				while (true)
				{
					pos = str.find("\n", pos);
					if (pos == std::string::npos)
					{
						break;
					}
					pos++;
					if (pos >= str.length())
					{
						throw std::invalid_argument(NO_INDENT_IN_LINE);
					}
					if (str.substr(pos, tabNumber) != tabString)
					{
						throw std::invalid_argument(NO_INDENT_IN_LINE);
					}
					str.erase(pos, tabNumber);
				}
				return str;
			}

			//Finds first substring (token) in the string (determined as substring,
			//sandwiched between beginning of the line and a delimeter), cuts off
			//the token and the bracing delimeter from string and returns token.
			std::string extractToken(std::string& str, const std::string& delimiter)
			{
				size_t pos;
				std::string token;
				if ((pos = str.find(delimiter)) != std::string::npos)
				{
					token = str.substr(0, pos);
					str.erase(0, pos + delimiter.length());
				}
				else
				{
                    token = std::move(str);
					str.clear();
				}
				return token;
			}

			//Finds first substring (token) in the string (determined as substring,
			//sandwiched between beginning of the line and a delimeter), cuts off
			//the token and the bracing delimeter from string and returns token.
			std::string extractToken(std::string& str, const std::vector<std::string>& delimiters)
			{
				size_t pos = std::string::npos;
				size_t actualDelimiterIndex = 0;
				std::string token;
				for (size_t i = 0; i < delimiters.size(); i++)
				{
					auto delimiterPos = str.find(delimiters[i]);
					if (delimiterPos != std::string::npos && pos != std::string::npos)
					{
						if (delimiterPos < pos)
						{
							pos = delimiterPos;
							actualDelimiterIndex = i;
						}
					}
				}
				if ( pos != std::string::npos)
				{
					token = str.substr(0, pos);
					str.erase(0, pos + delimiters[actualDelimiterIndex].length());
				}
				else
				{
                    token = std::move(str);
					str.clear();
				}
				return token;
			}

            bool containsNewlineOrTab(const std::string& str)
            {
                if (str.find('\n') != std::string::npos)
                    return true;

                if (str.find('\t') != std::string::npos)
                    return true;

                return false;
            }

			std::string removeNewlinesAndTabs(const std::string& str)
			{
				std::string res = str;

				res.erase( std::remove(res.begin(), res.end(), '\n'), res.end() );
				res.erase( std::remove(res.begin(), res.end(), '\t'), res.end() );
				res.erase( std::remove(res.begin(), res.end(), '\r'), res.end() );
				return res;
			}

            std::string removeNewlinesAndTabs(std::string&& res)
			{
                res.erase( std::remove(res.begin(), res.end(), '\n'), res.end() );
				res.erase( std::remove(res.begin(), res.end(), '\t'), res.end() );
				res.erase( std::remove(res.begin(), res.end(), '\r'), res.end() );

                return std::move(res);
			}

			std::string removeCharacters(const std::string& str, const std::initializer_list<char>& chars)
			{
				std::string res = str;
				for( auto& character : chars )
				{
					res.erase( std::remove(res.begin(), res.end(), character), res.end() );
				}
				return res;
			}

			size_t toSize_t(const std::string& str)
			{
				std::stringstream sstream(str);
				size_t result;
				sstream >> result;
				return result;
			}

			std::string toLowerCase(const std::string& str)
			{
				std::string res = str;

				std::transform(res.begin(), res.end(), res.begin(), [](unsigned char c){ return std::tolower(c); } );

				return res;
			}

			std::string toLowerCase(std::string&& str)
			{
				std::string res = std::move(str);

				std::transform(res.begin(), res.end(), res.begin(), [](unsigned char c){ return std::tolower(c); } );

				return res;
			}

			std::string toUpperCase(const std::string& str)
			{
				std::string res = str;

				std::transform(res.begin(), res.end(), res.begin(), [](unsigned char c){ return std::toupper(c); } );

				return res;
			}

			std::string toUpperCase(std::string&& str)
			{
				std::string res = std::move(str);

				std::transform(res.begin(), res.end(), res.begin(), [](unsigned char c){ return std::toupper(c); } );

				return res;
			}

			std::string removeExtension(const std::string& filename)
			{
				size_t lastdot = filename.find_last_of(".");
				if (lastdot == std::string::npos) return filename;
				return filename.substr(0, lastdot);
			}
		}
	}
}
