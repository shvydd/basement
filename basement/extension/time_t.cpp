#include "basement/extension/time_t.h"

#include <sstream>
#include <iomanip>
#include <chrono>

namespace basement
{
	namespace extension
	{
		namespace time_t_
		{
			extern const std::string format_ISO8601_DTZ_Interchange = "%FT%T%z";
			extern const std::string format_ISO8601_DTZ = "%F %T %z";
			extern const std::string format_ISO8601_D = "%F";
			extern const std::string format_ISO8601_T = "%T";
			extern const std::string format_ISO8601_T_NoSec = "%R";

			std::string toGmtTimeString(const std::time_t& time, const std::string& format)
			{
				//The GMT is a synonym for UTC
				std::ostringstream oss;

				oss << std::put_time( std::gmtime(&time), format.c_str() );
				return oss.str();
			}

			std::string toLocalTimeString(const std::time_t& time, const std::string& format)
			{
				std::ostringstream oss;

				oss << std::put_time( std::localtime(&time), format.c_str() );
				return oss.str();
			}

			std::time_t timeNow()
			{
				return std::chrono::system_clock::to_time_t( std::chrono::system_clock::now() );
			}

			std::string toTimeString(const int time)
			{
				const int seconds = time % 60;
				const int minutes = (time / 60) % 60;
				const int hours = ((time / 60) / 60) % 24;
				const int days = ((time / 60) / 60) / 24;

				std::string text;

				if (days)
					text += std::to_string(days) + "d ";
				if (hours || !text.empty())
					text += std::to_string(hours) + "h ";
				if (minutes || !text.empty())
					text += std::to_string(minutes) + "m ";

				text += std::to_string(seconds) + "s";

				return text;
			}
		}
	}
}
