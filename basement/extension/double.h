#ifndef BASEMENT_EXTENSION_DOUBLE_H
#define BASEMENT_EXTENSION_DOUBLE_H

#include "basement/Basement_global.h"

#include <limits>

namespace basement
{
	namespace extension
	{
		namespace double_
		{
            std::string toString(const double value, const int precision = std::numeric_limits<double>::max_digits10);

            /**
             * @brief Coverts double to string. If string is "0.(0)", returns "0".
             */
			std::string toString(const double value, const int precision, const bool discardZeros);

            /**
             * @brief Coverts double to string. If string is "0.(0)", returns "0".
             */
            std::string toStringScientific
			(
            const double value,
			const int precision = std::numeric_limits<double>::max_digits10,
			const bool discardZeros = false
			);

            bool isNull(const double& value, double epsilon = std::numeric_limits<double>::epsilon());

            bool approximatelyEqual(double a, double b, double epsilon = std::numeric_limits<double>::epsilon());

            bool essentiallyEqual(double a, double b, double epsilon = std::numeric_limits<double>::epsilon());

            bool definitelyGreaterThan(double a, double b, double epsilon = std::numeric_limits<double>::epsilon());

            bool definitelyLessThan(double a, double b, double epsilon = std::numeric_limits<double>::epsilon());
		}
	}
}

#endif /* BASEMENT_EXTENSION_DOUBLE_H */
