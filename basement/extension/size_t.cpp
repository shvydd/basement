#include "basement/extension/size_t.h"

#include <string>
#include <sstream>

namespace basement
{
	namespace extension
	{
		namespace size_t_
		{
			bool isPowerOfTwo(const size_t value)
			{
				if(value != 0 && (value & (value-1)) == 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}

			std::string toString(const size_t value)
			{
				std::ostringstream out;
				out << value;
				return out.str();
			}
		}
	}
}
