#ifndef BASEMENT_EXTENSION_SIZE_T_H
#define BASEMENT_EXTENSION_SIZE_T_H

#include "basement/Basement_global.h"

#include <cstddef>

namespace basement
{
	namespace extension
	{
		namespace size_t_
		{
			bool isPowerOfTwo(const size_t value);

			std::string toString(const size_t value);
		}
	}
}

#endif // BASEMENT_EXTENSION_SIZE_T_H
