#include "basement/algorithm/findRoot/Bisection.h"

#include <cmath>
#include "basement/math/Sign.h"

namespace basement
{
	namespace algorithm
	{
		namespace findRoot
		{
			double bisection
			(
			const std::function<double(const double&)>& f,
			const double y,
			const double absoluteYtolerance,
			double xMin,
			double xMax,
			size_t maxIterations
			)
			{
				double yMinDiff = f(xMin) - y;
				double yMaxDiff = f(xMax) - y;

				if ( math::sign(yMinDiff) == math::sign(yMaxDiff) )
				{
					return std::numeric_limits<double>::quiet_NaN();
				}

				size_t i = 0;
				double xMid;

				double yMidDiff;

				while (i<maxIterations)
				{
					i++;
					xMid = (xMin + xMax)/2.0;					
					yMidDiff = f(xMid) - y;

					if ( std::abs(yMidDiff) <= absoluteYtolerance )
					{
						return xMid;
					}

					yMinDiff = f(xMin) - y;
					yMaxDiff = f(xMax) - y;

					if ( math::sign(yMidDiff) == math::sign(yMinDiff) )
					{
						xMin = xMid;
					}
					else if (math::sign(yMidDiff) == math::sign(yMaxDiff))
					{
						xMax = xMid;
					}
					else
					{
						//The function f is not monotonic on the chosen interval.
						break;
					}
				}

				return std::numeric_limits<double>::quiet_NaN();
			}
		}
	}
}
