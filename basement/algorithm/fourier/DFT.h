#ifndef BASEMENT_ALGORITHM_FOURIER_DFT_H
#define BASEMENT_ALGORITHM_FOURIER_DFT_H

#include "basement/Basement_global.h"

#include <complex>
#include <vector>

namespace basement
{
    namespace algorithm
    {
        namespace fourier
        {
            /**
             * @brief Discrete Fourier transform. Spectrum defined in angular frequency.
             * Stright transform is defined with exp(-); inverse transform is defined with exp(+).
             * f(x) = Summ(n): A[n] * exp(i * x * frq[n])
             * Domains' sequences must be strictly increasing and with constanst step.
             */
            class DFT
            {
            public:
                typedef std::complex<double> complex;

                static void moveNegativeSpectrumPartToFront(std::vector<double>& vector);
                static void moveNegativeSpectrumPartToFront(std::vector<complex>& vector);

                static void moveNegativeSpectrumPartToBack(std::vector<double>& vector);
                static void moveNegativeSpectrumPartToBack(std::vector<complex>& vector);

                /**
                 * @brief Preserves norm: integral[ |f(t)|^2 dt ] == integral[ |f(omega)|^2 d omega].
                 *
                 */
                static std::vector<complex>  tranformDirect(const std::vector<complex>& fInRegularDomain);

                /**
                 * @brief Preserves norm: integral[ |f(t)|^2 dt ] == integral[ |f(omega)|^2 d omega].
                 *
                 */
                static std::vector<complex>  transformInverse(std::vector<complex> fInSpectralDomain);

                /**
                 * @brief Preserves norm: integral[ |f(t)|^2 dt ] == integral[ |f(omega)|^2 d omega].
                 *
                 */
                static std::vector<complex>  tranformDirect(const std::vector<double>& fInRegularDomain);

                static std::vector<double> generateFrequencyGrid(const std::vector<double>& timeGrid);
                static std::vector<double> generateTimeGrid(const std::vector<double>& frequencyGrid);

             private:
                static const std::string CLASS_NAME;

                DFT() = delete;
            };
        }
    }
}

#endif // BASEMENT_ALGORITHM_FOURIER_DFT_H
