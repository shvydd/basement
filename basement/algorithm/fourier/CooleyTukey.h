#ifndef BASEMENT_ALGORITHM_FOURIER_COOLEYTUKEY_H
#define BASEMENT_ALGORITHM_FOURIER_COOLEYTUKEY_H


#include "basement/Basement_global.h"

#include <complex>
#include <vector>
#include <utility>

namespace basement
{
	namespace algorithm
	{
		namespace fourier
		{
            /**
             * @brief Fast Fourier transform. Spectrum defined in angular frequency.
             * Stright transform is defined with exp(-); inverse transform is defined with exp(+).
             * f(x) = Summ(n): A[n] * exp(i * x * frq[n])
             * Domains' sequences must be strictly increasing, with constanst step and of size 2^n. These conditions are not checked.
             */
			class CooleyTukey
			{
            public:
                /**
                 * @brief Does not preserve norm: integral[ |f(t)|^2 dt ] != integral[ |f(omega)|^2 d omega].
                 */
                static std::vector<std::complex<double>>  fft(const std::vector<std::complex<double>>& fInRegularDomain);

                /**
                 * @brief Does not preserve norm: integral[ |f(t)|^2 dt ] != integral[ |f(omega)|^2 d omega].
                 */
                static std::vector<std::complex<double>> ifft(const std::vector<std::complex<double>>& fInSpectralDomain);

                /**
                 * @brief Preserves norm: integral[ |f(t)|^2 dt ] == integral[ |f(omega)|^2 d omega].
                 */
                static std::vector<std::complex<double>>  fft(const double& regularDomainStep, const std::vector<std::complex<double>>& fInRegularDomain);

                /**
                 * @brief Preserves norm: integral[ |f(t)|^2 dt ] == integral[ |f(omega)|^2 d omega].
                 */
                static std::vector<std::complex<double>> ifft(const double& spectralDomainStep, const std::vector<std::complex<double>>& fInSpectralDomain);

                /**
                 * @brief Preserves norm: integral[ |f(t)|^2 dt ] == integral[ |f(omega)|^2 d omega].
                 *
                 */
                static std::pair< std::vector<double>, std::vector<std::complex<double>> >  fft(const std::vector<double>& regularDomain, const std::vector<std::complex<double>>& fInRegularDomain, const double& spectralShift = 0);

                /**
                 * @brief Preserves norm: integral[ |f(t)|^2 dt ] == integral[ |f(omega)|^2 d omega].
                 */
                static std::pair< std::vector<double>, std::vector<std::complex<double>> > ifft(const std::vector<double>& spectralDomain, const std::vector<std::complex<double>>& fInSpectralDomain, const double& regularShift = 0);

                static std::vector<double> generateDomain(const double& step, const double& shift, const size_t size);

                static std::vector<double> spectralDomain(const std::vector<double>& regularDomain, const double& spectralShift = 0);
                static std::vector<double> regularDomain(const std::vector<double>& spectralDomain, const double& regularShift = 0);

                static std::pair<std::vector<double>, std::vector<double>> generateOptimalDomains
                (
                const double& reg_step,
                const double& reg_span,
                const double& reg_shift,
                const double& spec_step,
                const double& spec_span,
                const double& spec_shift
                );

            private:
                CooleyTukey() = delete;

                static void straight(std::vector<std::complex<double>>& fInRegularDomain);
                static void inverse (std::vector<std::complex<double>>& fInSpectralDomain);

                static void splitAndSwap(std::vector<std::complex<double>>& vector);

                static double findStep(const std::vector<double>&);
			};
		}
	}
}

#endif // BASEMENT_ALGORITHM_FOURIER_COOLEYTUKEY_H
