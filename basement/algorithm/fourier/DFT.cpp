#include "basement/algorithm/fourier/DFT.h"

#include "basement/Constants.h"
#include <iostream>
#include "basement/math/Sequence.h"

namespace basement
{
    namespace algorithm
    {
        namespace fourier
        {
			const std::string DFT::CLASS_NAME = "basement::algorithm::fourier::DFT";

			void DFT::moveNegativeSpectrumPartToFront(std::vector<double>& vector)
			{
				if (vector.size() == 0)
					throw std::invalid_argument(CLASS_NAME + "::moveNegativeSpectrumPartToFront: vector is empty.");

				std::vector<double> buff(vector.begin() + vector.size()/2 + 1, vector.end());
				vector.erase(vector.begin() + vector.size()/2 + 1, vector.end());
				vector.insert(vector.begin(), buff.begin(), buff.end());
			}

			void DFT::moveNegativeSpectrumPartToFront(std::vector<complex>& vector)
			{
				if (vector.size() == 0)
					throw std::invalid_argument(CLASS_NAME + "::moveNegativeSpectrumPartToFront: vector is empty.");

				std::vector<complex> buff(vector.begin() + vector.size()/2 + 1, vector.end());
				vector.erase(vector.begin() + vector.size()/2 + 1, vector.end());
				vector.insert(vector.begin(), buff.begin(), buff.end());
			}

			void DFT::moveNegativeSpectrumPartToBack(std::vector<double>& vector)
			{
				if (vector.size() < 2)
					throw std::invalid_argument(CLASS_NAME + "::moveNegativeSpectrumPartToBack: vector size < 2.");

				std::vector<double> buff(vector.begin(), vector.begin() + vector.size()/2 - 1);
				vector.erase            (vector.begin(), vector.begin() + vector.size()/2 - 1);
				vector.insert(vector.end(), buff.begin(), buff.end());
			}

			void DFT::moveNegativeSpectrumPartToBack(std::vector<complex>& vector)
			{
				if (vector.size() < 2)
					throw std::invalid_argument(CLASS_NAME + "::moveNegativeSpectrumPartToBack: vector size < 2.");

				std::vector<complex> buff(vector.begin(), vector.begin() + vector.size()/2 - 1);
				vector.erase             (vector.begin(), vector.begin() + vector.size()/2 - 1);
				vector.insert(vector.end(), buff.begin(), buff.end());
			}

			std::vector<DFT::complex>  DFT::tranformDirect(const std::vector<complex>& f_time)
            {
				std::vector<complex> f_frq(f_time.size());

                if (f_time.size() == 0)
                    return f_frq;

                for (size_t index_frq = 0; index_frq < f_frq.size(); index_frq++)
                {
                    f_frq[index_frq] = 0.0;

                    for (size_t index_time = 0; index_time < f_time.size(); index_time++)
                    {
                        const double angle = -2.0 * basement::constants::pi * double(index_frq) * double(index_time) / double(f_time.size());
                        f_frq[index_frq] += f_time[index_time] * std::polar(1.0, angle);
                    }

                    f_frq[index_frq] = f_frq[index_frq] / std::sqrt(f_time.size());
                }

				//double summInTime = 0;
				//for (const auto& val : f_time)
				//summInTime += std::real(val * std::conj(val));

				//double summInSpec = 0;
				//for (const auto& val : f_frq)
				//summInSpec += std::real(val * std::conj(val));

				//std::cerr << "DFT. Summ of of f_time: " << summInTime << std::endl;
				//std::cerr << "DFT. Summ of of f_frq: " << summInSpec << std::endl;

				moveNegativeSpectrumPartToFront(f_frq);

                return f_frq;
            }

			std::vector<DFT::complex>  DFT::tranformDirect(const std::vector<double>& f_time)
			{
				std::vector<complex> f_time_complex;
				f_time_complex.reserve(f_time.size());
				for (const auto val : f_time)
					f_time_complex.push_back(val);

				return tranformDirect(f_time_complex);
			}

			std::vector<DFT::complex> DFT::transformInverse(std::vector<complex> f_frq)
            {
				moveNegativeSpectrumPartToBack(f_frq);
				std::vector<complex> f_time(f_frq.size());

                if (f_frq.size() == 0)
                    return f_time;

                for (size_t index_time = 0; index_time < f_time.size(); index_time++)
                {
                    f_time[index_time] = 0.0;

                    for (size_t index_frq = 0; index_frq < f_frq.size(); index_frq++)
                    {
                        const double angle =  2.0 * basement::constants::pi * double(index_time) * double(index_frq) / double(f_frq.size());
                        f_time[index_time] += f_frq[index_frq] * std::polar(1.0, angle);
                    }

                    f_time[index_time] = f_time[index_time] / std::sqrt(f_frq.size());
                }

                return f_time;
			}

			std::vector<double> DFT::generateFrequencyGrid(const std::vector<double>& timeGrid)
			{
				const size_t N = timeGrid.size();

                //TODO Adjust tolerance
                const auto isUniform = basement::math::sequence::isUniform(timeGrid);
                if ( !isUniform.first )
                    throw std::invalid_argument(CLASS_NAME + ": time grid is not uniform.");

                const double& oppositeDomainStep = isUniform.second;

				if (oppositeDomainStep <= 0.0)
                    throw std::invalid_argument(CLASS_NAME + ": time grid step <= 0");

				const double step = basement::constants::pi * 2.0 / (oppositeDomainStep * double(N));
				std::vector<double> grid;
				grid.reserve(N);
				for (size_t i = 0; i <= N/2; i++)
				{
					grid.push_back(step * double(i));
				}

				for (size_t i = N/2 + 1; i < N; i++)
				{
					grid.push_back(-grid[N-i]);
				}

				moveNegativeSpectrumPartToFront(grid);

				return grid;
			}

			std::vector<double> DFT::generateTimeGrid(const std::vector<double>& frequencyGrid)
			{
				const size_t N = frequencyGrid.size();

				//TODO Adjust tolerance
                const auto isUniform = basement::math::sequence::isUniform(frequencyGrid);
                if ( !isUniform.first )
                    throw std::invalid_argument(CLASS_NAME + ": frequency grid is not uniform.");

                const double& oppositeDomainStep = isUniform.second;

				if (oppositeDomainStep <= 0.0)
                    throw std::invalid_argument(CLASS_NAME + ": frequency grid step <= 0");

				const double step = basement::constants::pi * 2.0 / (oppositeDomainStep * double(N));
				std::vector<double> grid;
				grid.reserve(N);
				for (size_t i = 0; i < N; i++)
				{
					grid.push_back(step * double(i));
				}

				return grid;
			}
        }
    }
}
