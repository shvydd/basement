#include "basement/algorithm/fourier/Brute.h"

#include "basement/Constants.h"

namespace basement
{
	namespace algorithm
	{
		namespace fourier
		{
			std::vector<Brute::complex> Brute::ifft(const std::vector<double>& spectralDomain, const std::vector<complex>& fInSpectralDomain, const std::vector<double>& regularDomain)
			{
				if (spectralDomain.size() != fInSpectralDomain.size())
					throw std::invalid_argument("basement::algorithm::fourier::Brute: spectrum size and size of amplitudes on spectral domain are not equal");

				std::vector<complex> fInRegularDomain(regularDomain.size());

				for (size_t indReg = 0; indReg < regularDomain.size(); indReg++)
				{
					const double& t = regularDomain[indReg];
					complex& f_t = fInRegularDomain[indReg];

					f_t = 0.0;
					for (size_t indSpec = 0; indSpec < spectralDomain.size(); indSpec++)
					{
						const double& frq = spectralDomain[indSpec];
						const complex& f_frq = fInSpectralDomain[indSpec];

						f_t += f_frq * std::exp(basement::constants::i * t * frq);
					}
				}

				return fInRegularDomain;
			}
		}
	}
}
