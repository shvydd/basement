#ifndef BASEMENT_ALGORITHM_INTEGRAL_RIEMANNSUM_H
#define BASEMENT_ALGORITHM_INTEGRAL_RIEMANNSUM_H

#include "basement/Basement_global.h"

#include <vector>
#include <complex>
#include <functional>

namespace basement
{
	namespace algorithm
	{
		namespace integral
		{
			class RiemannSum
			{
				private:
					template<class Tx, class Tf>
					std::complex<double> integrate(const std::vector<Tx>& x, const std::vector<Tf>& f) const;

				public:
					double operator() (const std::vector<double>& x, const std::vector<double>& f) const;
                    double operator() (const std::vector<double>& x, const std::function<double(const double& x)>& F) const;
					std::complex<double> operator() (const std::vector<double>& x, const std::vector<std::complex<double>>& f) const;
					std::complex<double> operator() (const std::vector<std::complex<double>>& x, const std::vector<double>& f) const;
					std::complex<double> operator() (const std::vector<std::complex<double>>& x, const std::vector<std::complex<double>>& f) const;

				private:
					template<class Tx, class Ty>
					static std::complex<double> integrate(const std::vector<Tx>& x, const std::vector<Tx>& y, const std::function<std::complex<double>(const Tx& x, const Ty& y)>& F);

				public:
					std::complex<double> operator() (const std::vector<double>& x, const std::vector<double>& y, const std::function<std::complex<double>(const double& x, const double& y)>& F) const;
					double operator() (const std::vector<double>& x, const std::vector<double>& y, const std::function<double(const double& x, const double& y)>& F) const;
			};
		}
	}
}

#endif // BASEMENT_ALGORITHM_INTEGRAL_RIEMANNSUM_H
