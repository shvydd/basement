#ifndef BASEMENT_AlGORITHM_EXPANSION_TAYLOR_H
#define BASEMENT_AlGORITHM_EXPANSION_TAYLOR_H


#include "basement/Basement_global.h"

#include <vector>
#include <functional>

namespace basement
{
	namespace algorithm
	{
		namespace expansion
		{
			class Taylor
			{
				public:
					static double f(const std::vector<double>& coeffs, const double& xCenter, const double& x);

					static std::vector<double> findCoefficients
					(
					const std::vector<double>& x,
					const std::vector<double>& F,
					const size_t centralPointIndex,
					const size_t dif_order,
					const size_t err_order,
					const bool checkXGridUniformity = true
					);

					static std::vector<double> findCoefficients
					(
					const std::function<double(const double)>& F,
					const double x,
					const double d,
					const size_t dif_order,
					const size_t err_order
					);
			};
		}
	}
}

#endif // BASEMENT_AlGORITHM_EXPANSION_TAYLOR_H
