#include "basement/algorithm/expansion/Taylor.h"

#include <stdexcept>
#include "basement/math/Factorial.h"
#include <cmath>
#include "basement/algorithm/derivative/Taylor.h"

namespace basement
{
	namespace algorithm
	{
		namespace expansion
		{
			double Taylor::f(const std::vector<double> &coeffs, const double &xCenter, const double &x)
			{
				if (coeffs.size() == 0.0)
					throw std::invalid_argument("basement::algorithm::expansion::Taylor: coeffs.size = 0");

				double res = 0.0;
				for (size_t i = 0; i < coeffs.size(); i++)
				{
					res += coeffs[i] / basement::math::factorial(double(i)) * std::pow(x - xCenter, i);
				}
				return res;
			}

			std::vector<double> Taylor::findCoefficients
			(
			const std::vector<double>& x,
			const std::vector<double>& F,
			const size_t centralPointIndex,
			const size_t dif_order,
			const size_t err_order,
			const bool checkXGridUniformity
			)
			{
				basement::algorithm::derivative::Taylor derivative;
				return derivative
				(
				x,
				F,
				centralPointIndex,
				dif_order,
				err_order,
				checkXGridUniformity
				);
			}

			std::vector<double> Taylor::findCoefficients
			(
			const std::function<double(const double)>& F,
			const double x,
			const double d,
			const size_t dif_order,
			const size_t err_order
			)
			{
				std::vector<double> res(dif_order+1);
				basement::algorithm::derivative::Taylor derivative;

				for (size_t n = 0; n <= dif_order; n++)
				{
					res[n] = derivative
					(
					F,
					x,
					d,
					n,
					err_order
					);
				}

				return res;
			}
		}
	}
}
