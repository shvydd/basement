#include "basement/algorithm/width/LastRidge.h"
#include <stdexcept>
#include <algorithm>

namespace basement
{
	namespace algorithm
	{
		namespace width
		{
			LastRidge::LastRidge()
			{

			}

			double LastRidge::find(const std::vector<double>& xVals, const std::vector<double>& yVals, const double level)
			{
				if (xVals.size() != yVals.size())
				{
					throw std::invalid_argument("basement::algorithm::width::LastRidge::find: lengths of xVals and yVals are not equal.");
				}

				if (xVals.size() == 0)
				{
					throw std::invalid_argument("basement::algorithm::width::LastRidge::find: vals.size = 0.");
				}

				double maxYVal = *std::max_element(yVals.begin(), yVals.end());
				double yValAtLevel = maxYVal * level;

				double xLeft = 0.0;
				double xRight = 0.0;

				for (size_t i=0; i<xVals.size()-1; i++)
				{
					if (yVals[i+1]>=yValAtLevel && yVals[i]<yValAtLevel)
					{
						xLeft = (yValAtLevel - yVals[i]) / (yVals[i+1] - yVals[i]) * (xVals[i+1] - xVals[i]) + xVals[i];
						break;
					}
				}

				for (size_t i=xVals.size()-1; i>0; i--)
				{
					if(yVals[i]<=yValAtLevel && yVals[i-1]>yValAtLevel)
					{
						xRight = (yValAtLevel - yVals[i-1]) / (yVals[i] - yVals[i-1]) * (xVals[i] - xVals[i-1]) + xVals[i-1];
						break;
					}
				}

				double widthAtLevel =  xRight - xLeft;


				if (widthAtLevel < 0.0)
				{
					widthAtLevel = xVals.back()- xVals.front();
				}

				return widthAtLevel;
			}
		}
	}
}
