#ifndef BASEMENT_AlGORITHM_INTERPOLATION_LINEAR_H
#define BASEMENT_AlGORITHM_INTERPOLATION_LINEAR_H


#include "basement/Basement_global.h"

#include <vector>
#include "basement/math/Sequence.h"

#include <complex>

namespace basement
{
	namespace algorithm
	{
		namespace interpolation
		{
			class Linear
			{
				private:
					std::vector<double> x;
                    basement::math::sequence::Monotonicity xBehavior;
					std::vector<double> y;

				public:
					Linear(const std::vector<double>& x, const std::vector<double>& y);

					const std::vector<double>& getX() const;
					const std::vector<double>& getY() const;

					double operator() (const double& x) const;
			};

            class LinearComplex
            {
               private:
                    typedef std::complex<double> complex;

                    std::vector<double> x;
                    basement::math::sequence::Monotonicity xBehavior;
                    std::vector<complex> y;

                public:
                    LinearComplex(const std::vector<double>& x, const std::vector<complex>& y);

                    const std::vector<double>& getX() const;
                    const std::vector<complex>& getY() const;

                    complex operator() (const double& x) const;
            };
		}
	}
}

#endif // BASEMENT_AlGORITHM_INTERPOLATION_LINEAR_H
