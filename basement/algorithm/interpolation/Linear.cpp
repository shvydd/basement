#include "basement/algorithm/interpolation/Linear.h"

#include <basement/Constants.h>

namespace basement
{
	namespace algorithm
	{
		namespace interpolation
		{
			Linear::Linear(const std::vector<double>& _x, const std::vector<double>& _y)
				: x(_x), y(_y)
			{
				if ( x.size() != y.size() )
					throw std::invalid_argument("basement::algorithm::interpolation::Linear: x vector size does not match y vector size");

				if ( x.size() <= 1 )
					throw std::invalid_argument("basement::algorithm::interpolation::Linear: size of x and y <=1");

                xBehavior = basement::math::sequence::monotonicity::find(x);
                if (xBehavior != basement::math::sequence::Monotonicity::strictly_increasing)
				{
                    if (xBehavior == basement::math::sequence::Monotonicity::strictly_decreasing)
					{
						std::reverse(x.begin(), x.end());
						std::reverse(y.begin(), y.end());
					}
					else
						throw std::invalid_argument("basement::algorithm::interpolation::Linear: x vector values must be strictly monotonic");
				}
			}

			const std::vector<double>& Linear::getX() const
			{
				return x;
			}

			const std::vector<double>& Linear::getY() const
			{
				return y;
			}

			double Linear::operator() (const double& point) const
			{
				auto const it = std::lower_bound(x.begin(), x.end(), point);
				const size_t index = std::distance(x.begin(), it);

                if (index == x.size())
					throw std::invalid_argument("basement::algorithm::interpolation::Linear: x point is out of domain");

                if (index == 0)
                {
                    if (point == x.front())
                        return y.front();
                    else
                        throw std::invalid_argument("basement::algorithm::interpolation::Linear: x point is out of domain");
                }


				return y[index - 1] + (y[index] - y[index - 1]) / (x[index] - x[index - 1]) * (point - x[index - 1]);
			}




            LinearComplex::LinearComplex(const std::vector<double>& _x, const std::vector<complex>& _y)
                : x(_x), y(_y)
            {
                if ( x.size() != y.size() )
                    throw std::invalid_argument("basement::algorithm::interpolation::LinearComplex: x vector size does not match y vector size");

                if ( x.size() <= 1 )
                    throw std::invalid_argument("basement::algorithm::interpolation::LinearComplex: size of x and y <=1");

                xBehavior = basement::math::sequence::monotonicity::find(x);
                if (xBehavior != basement::math::sequence::Monotonicity::strictly_increasing)
                {
                    if (xBehavior == basement::math::sequence::Monotonicity::strictly_decreasing)
                    {
                        std::reverse(x.begin(), x.end());
                        std::reverse(y.begin(), y.end());
                    }
                    else
                        throw std::invalid_argument("basement::algorithm::interpolation::LinearComplex: x vector values must be strictly monotonic");
                }
            }

            const std::vector<double>& LinearComplex::getX() const
            {
                return x;
            }

            const std::vector<LinearComplex::complex>& LinearComplex::getY() const
            {
                return y;
            }

            LinearComplex::complex LinearComplex::operator() (const double& point) const
            {
                auto const it = std::lower_bound(x.begin(), x.end(), point);
                const size_t index = std::distance(x.begin(), it);

                if (index == x.size())
                    throw std::invalid_argument("basement::algorithm::interpolation::LinearComplex: x point is out of domain");

                if (index == 0)
                {
                    if (point == x.front())
                        return y.front();
                    else
                        throw std::invalid_argument("basement::algorithm::interpolation::LinearComplex: x point is out of domain");
                }

                const double amp = std::abs(y[index - 1]) + std::abs((y[index] - y[index - 1])) / (x[index] - x[index - 1]) * (point - x[index - 1]);
                const double arg = std::arg(y[index - 1]) + std::arg((y[index] - y[index - 1])) / (x[index] - x[index - 1]) * (point - x[index - 1]);

                return std::exp(basement::constants::i * arg) * amp;
            }
		}
	}
}
