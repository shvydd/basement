#include "basement/geometry/General.h"

#include "basement/Constants.h"

namespace basement
{
	namespace geometry
	{
		extern const std::string EXCEPTION_VECTOR_NORM_NULL = "basement::geometry::general: vector norm = 0";

		double findAngleUsingSinAndCos(const double& cosA, const double& sinA)
		{
			if (cosA*cosA + sinA*sinA > 1.0)
				throw std::invalid_argument("basement::geometry::findAngleUsingSinAndCos: cos^2 + sin^2 > 1.0");

			if (sinA >= 0)
			{
				return acos(cosA);
			}
			else
			{
				return 2.0*basement::constants::pi - acos(cosA);
			}
		}

		double findAngleFromMinusPiToPiUsingSinAndCos(const double& cosA, const double& sinA)
		{
			if (cosA*cosA + sinA*sinA > 1.0)
				throw std::invalid_argument("basement::geometry::findAngleFromMinusPiToPiUsingSinAndCos: cos^2 + sin^2 > 1.0");

			if (sinA >= 0)
			{
				return acos(cosA);
			}
			else
			{
				return -acos(cosA);
			}
		}

		Vector findOrthogonalUnitaryVector(const Vector& vector)
		{
			if(vector.getX() != 0.0)
			{
				Vector orthogonal =
				Vector(-(vector.getY() + vector.getZ()) / vector.getX(), 1.0, 1.0 );
				return orthogonal.scalingToThis( 1.0/orthogonal.norm() );
			}
			if(vector.getY() != 0.0)
			{
				Vector orthogonal =
				Vector(1.0, -(vector.getX() + vector.getZ()) / vector.getY(), 1.0 );
				return orthogonal.scalingToThis( 1.0/orthogonal.norm() );
			}
			if(vector.getZ() != 0.0)
			{
				Vector orthogonal =
				Vector(1.0, 1.0, -(vector.getX() + vector.getY()) / vector.getZ() );
				return orthogonal.scalingToThis( 1.0/orthogonal.norm() );
			}
			else
			{
				throw std::invalid_argument(EXCEPTION_VECTOR_NORM_NULL);
			}
		}

		Vector findBetweenVectorsAngleAndAxis(Vector vector1, Vector vector2)
		{
			double vector1Norm = vector1.norm();
			double vector2Norm = vector2.norm();

			if(vector1Norm == 0.0 || vector2Norm == 0.0)
				throw std::invalid_argument(EXCEPTION_VECTOR_NORM_NULL);

			vector1.scalingToThis(1.0/vector1Norm);
			vector2.scalingToThis(1.0/vector2Norm);

			Vector rotationAxis = vector1.crossProduct(vector2);

			//NOTE sin always >= 0
			double sinRotationAngle = rotationAxis.norm();
			double cosRotationAngle = vector1.dotProduct(vector2);

			//NOTE RotationAngle always [0; pi].
			double rotationAngle = findAngleUsingSinAndCos(cosRotationAngle, sinRotationAngle);

			if (sinRotationAngle != 0.0)
			{
				rotationAxis = rotationAxis.scalingToThis(1.0/sinRotationAngle);
			}
			else
			{
				//vectors are collinear
				rotationAxis = findOrthogonalUnitaryVector(vector1);
			}

			return rotationAxis.scalingToThis(rotationAngle);
		}

		double findBetweenVectorsAngle(Vector vector1, Vector vector2)
		{
			double vector1Norm = vector1.norm();
			double vector2Norm = vector2.norm();

			if(vector1Norm == 0.0 || vector2Norm == 0.0)
				throw std::invalid_argument(EXCEPTION_VECTOR_NORM_NULL);

			vector1.scalingToThis(1.0/vector1Norm);
			vector2.scalingToThis(1.0/vector2Norm);

			Vector rotationAxis = vector1.crossProduct(vector2);

			//NOTE sin always >= 0
			double sinRotationAngle = rotationAxis.norm();
			double cosRotationAngle = vector1.dotProduct(vector2);

			//NOTE Anle between vectors always [0; pi].
			return findAngleUsingSinAndCos(cosRotationAngle, sinRotationAngle);
		}

		Matrix generateRotationMatrix(const Vector& axis, const double& angle)
		{
			Matrix matrix;

			matrix(0,0) = cos(angle) + (1-cos(angle)) * axis.getX()*axis.getX();
			matrix(0,1) = (1-cos(angle)) * axis.getX()*axis.getY() - sin(angle)*axis.getZ();
			matrix(0,2) = (1-cos(angle)) * axis.getX()*axis.getZ() + sin(angle)*axis.getY();

			matrix(1,0) = (1-cos(angle)) * axis.getX()*axis.getY() + sin(angle)*axis.getZ();
			matrix(1,1) = cos(angle) + (1-cos(angle)) * axis.getY()*axis.getY();
			matrix(1,2) = (1-cos(angle)) * axis.getY()*axis.getZ() - sin(angle)*axis.getX();

			matrix(2,0) = (1-cos(angle)) * axis.getX()*axis.getZ() - sin(angle)*axis.getY();
			matrix(2,1) = (1-cos(angle)) * axis.getY()*axis.getZ() + sin(angle)*axis.getX();
			matrix(2,2) = cos(angle) + (1-cos(angle)) * axis.getZ()*axis.getZ();

			return matrix;
		}

		Matrix generateRotationMatrix(const Vector& angleAxis)
		{
			double angle = angleAxis.norm();
			if (angle == 0.0)
			{
				return generateRotationMatrix(angleAxis, angle);
			}
			return generateRotationMatrix(angleAxis.scaling(1.0/angle), angle);
		}

		Matrix_4_4 generateTransformationMatrix(const boost::numeric::ublas::matrix<double>& rotation, const Vector& shift)
		{
			Matrix_4_4 transformation = boost::numeric::ublas::matrix<double>(4,4);
			for (int i=0; i<3; i++)
			{
				for (int j=0; j<3; j++)
				{
					transformation(i,j) = rotation(i,j);
				}

				switch (i)
				{
					case 0:
						transformation(i,3) = shift.getX();
						break;
					case 1:
						transformation(i,3) = shift.getY();
						break;
					case 2:
						transformation(i,3) = shift.getZ();
						break;
				}
			}
			transformation(3,0) = 0.0;
			transformation(3,1) = 0.0;
			transformation(3,2) = 0.0;
			transformation(3,3) = 1.0;

			return transformation;
		}

		Matrix_4_4 generateTransformationMatrix(const Vector& shift, const Matrix& rotation)
		{
			Matrix_4_4 rotation4x4 = boost::numeric::ublas::matrix<double>(4,4,0.0);
			for (int i=0; i<3; i++)
			{
				for (int j=0; j<3; j++)
				{
					rotation4x4(i,j) = rotation(i,j);
				}
			}
			rotation4x4(3,3) = 1.0;

			Matrix_4_4 shift4x4 = boost::numeric::ublas::matrix<double>(4,4,0.0);
			shift4x4(0,3) = shift.getX();
			shift4x4(1,3) = shift.getY();
			shift4x4(2,3) = shift.getZ();
			shift4x4(0,0) = 1.0;
			shift4x4(1,1) = 1.0;
			shift4x4(2,2) = 1.0;
			shift4x4(3,3) = 1.0;

			return boost::numeric::ublas::prec_prod(rotation4x4, shift4x4);
		}

		Matrix_4_4 transformExtendedQuadraticFormMatrix(const Matrix_4_4& formMatrixInOldBasis, const Matrix_4_4& vectorTransformMatrix)
		{
			using namespace boost::numeric::ublas;

			//TODO Why I can't multiply in this way: prod(trans(T), prod(C,T))?
			matrix<double> temp = prec_prod(formMatrixInOldBasis, vectorTransformMatrix);
			matrix<double> formMatrixInNewBasis = prec_prod(trans(vectorTransformMatrix), temp);
			//make matrix symmetric
			formMatrixInNewBasis = (formMatrixInNewBasis + trans(formMatrixInNewBasis))/2.0;
			return formMatrixInNewBasis;
			//http://mathhelpplanet.com/static.php?p=kanonicheskie-uravneniya-poverhnosti-vtorogo-poryadka
			//https://ru.wikipedia.org/wiki/%D0%9F%D0%BE%D0%B2%D0%B5%D1%80%D1%85%D0%BD%D0%BE%D1%81%D1%82%D1%8C_%D0%B2%D1%82%D0%BE%D1%80%D0%BE%D0%B3%D0%BE_%D0%BF%D0%BE%D1%80%D1%8F%D0%B4%D0%BA%D0%B0
			//https://ru.wikipedia.org/wiki/%D0%9C%D0%B0%D1%82%D1%80%D0%B8%D1%86%D0%B0_%D0%BF%D0%B5%D1%80%D0%B5%D1%85%D0%BE%D0%B4%D0%B0
		}

		Vector projectVectorOnPlane(const Vector& vector, Vector planeNormal)
		{
			double planeNormalNorm = planeNormal.norm();
			if (planeNormalNorm == 0.0)
				throw std::invalid_argument("basement::geometry::general::projectVectorOnPlane: plane normal norm = 0");

			planeNormal.scalingToThis(1.0 / planeNormalNorm);

			Vector vectorPrOnNormal = planeNormal.scaling( planeNormal.dotProduct(vector) );

			return vector.difference(vectorPrOnNormal);
		}
	}
}
