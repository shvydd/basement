#include "basement/Constants.h"

namespace basement
{
	namespace constants
	{
		extern const double pi = 3.141592653589;
		extern const std::complex<double> i = std::complex<double>(0.0, 1.0);
		extern const double c = 299792458.0 / 1000000000.0;

		extern const std::string degree = "°";
		extern const std::string micrometer = "μm";
	}
}

