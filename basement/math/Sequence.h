#ifndef BASEMENT_MATH_SEQUENCE_H
#define BASEMENT_MATH_SEQUENCE_H

#include "basement/Basement_global.h"

#include <string>
#include <vector>

namespace basement
{
    namespace math
    {
        namespace sequence
        {
            enum class Monotonicity {strictly_increasing, increasing, strictly_decreasing, decreasing, non_monotonic, constant};
            namespace monotonicity
            {
                extern const std::string STRICTLY_INCREASING_STRING;
                extern const std::string INCREASING_STRING;
                extern const std::string STRICTLY_DECREASING_STRING;
                extern const std::string DECREASING_STRING;
                extern const std::string NON_MONOTONIC_STRING;
                extern const std::string CONSTANT_STRING;
                extern const std::string CLASS_NAME;
                std::string toString(const Monotonicity&);

                /**
                 * @brief Find monotonicity of sequence.
                 * @param tolerance Ignored difference between values in sequence.
                 */
                Monotonicity find(const std::vector<double>& sequence, const double& tolerance = std::numeric_limits<double>::epsilon());
            }

            /**
             * @brief Check if the step between sequence points is constant.
             * @return Returns true if grid is uniform and its mean step.
             */
            std::pair<bool, double> isUniform(const std::vector<double>& sequence, const double tolerance = std::numeric_limits<double>::epsilon());
        } // namespace sequence
    } // namespace math
} // namespace basement

#endif // BASEMENT_MATH_SEQUENCE_H
