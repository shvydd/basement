#ifndef BASEMENT_MATH_MODULO_H
#define BASEMENT_MATH_MODULO_H


#include "basement/Basement_global.h"

namespace basement
{
	namespace math
	{
		double modulo(const double& val, const double& quotient);
	}
}

#endif // BASEMENT_MATH_MODULO_H
