#include "basement/math/InverseFunction.h"

#include <stdexcept>
#include <algorithm>

namespace basement
{
	namespace math
	{
		const std::string InverseFunction::CLASS_NAME = "basement::math::InverseFunction";

		std::pair<std::vector<double>, std::vector<double>> InverseFunction::find
		(
		const std::vector<double>& x,
		const std::vector<double>& y
		)
		{
			if (x.size() != y.size())
				throw std::invalid_argument(CLASS_NAME + ": x.size does not match y.size.");

			std::vector<std::pair<double, double>> xy;
			xy.reserve(x.size());

			for (size_t i = 0; i < x.size(); i++)
				xy.push_back(std::pair(x[i], y[i]));

			const auto sorter = [] (const std::pair<double, double>& a, const std::pair<double, double>& b)
			{
				if (a.second < b.second)
					return true;
				else
					return false;
			};

			std::sort(xy.begin(), xy.end(), sorter);

			for (auto it = xy.begin() + 1; it != xy.end(); )
			{
				if (it->second == (it-1)->second)
				{
					if (it->first == (it-1)->first)
					{
						xy.erase(it);
					}
					else
					{
						throw std::invalid_argument(CLASS_NAME + ": x(y) is ambiguous");
					}
				}
				else
				{
					it++;
				}
			}

			std::vector<double> x_i, y_i;
			x_i.reserve(xy.size());
			y_i.reserve(xy.size());
			for (const auto& pair : xy)
			{
				x_i.push_back(pair.second);
				y_i.push_back(pair.first);
			}

			return std::pair(x_i, y_i);
		}
	}
}
