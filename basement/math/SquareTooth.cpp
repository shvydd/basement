#include "basement/math/SquareTooth.h"

namespace basement
{
	namespace math
	{
		double squareTooth(const double& min, const double& max, const double& x)
		{
			if (min > max)
				throw std::invalid_argument("basement::math::squareTooth: min margin > max margin");

			if (x >= min && x <= max)
			{
				return 1.0;
			}
			return 0.0;
		}
	}
}
