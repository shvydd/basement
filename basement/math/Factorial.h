#ifndef BASEMENT_MATH_FACTORIAL_H
#define BASEMENT_MATH_FACTORIAL_H


#include "basement/Basement_global.h"

#include <cstddef>

namespace basement
{
	namespace math
	{
		size_t factorial(const size_t n);
		double factorial(const double n);
	}
}

#endif // BASEMENT_MATH_FACTORIAL_H
