#ifndef BASEMENT_MATH_EXTREMA_H
#define BASEMENT_MATH_EXTREMA_H

#include "basement/Basement_global.h"

#include <vector>
#include <string>

namespace basement
{
	namespace math
	{
        class Extrema
		{
        public:
            enum class ExtremumType{max, min};

            /**
             * @brief Finds all extrema. Returns pairs, composed of type and index. If function has shelf with ,
             */
            static std::vector<std::pair<size_t, ExtremumType>> find(const std::vector<double>& y);

        private:
            static const std::string CLASS_NAME;

            Extrema() = delete;
		};
	}
}

#endif // BASEMENT_MATH_EXTREMA_H
