#include "basement/math/Sign.h"

namespace basement
{
	namespace math
	{
		double sign(const double x)
		{
			if (x < 0)
			{
				return -1;
			}
			return 1.0;
		}
	}
}
