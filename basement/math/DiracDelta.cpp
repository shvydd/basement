#include "basement/math/DiracDelta.h"

namespace basement
{
	namespace math
	{
		double diracDelta(const double& x)
		{
			return (x==0.0 ? 1.0 : 0.0);
		}
	}
}
