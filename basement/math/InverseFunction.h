#ifndef BASEMENT_MATH_INVERSEFUNCTION_H
#define BASEMENT_MATH_INVERSEFUNCTION_H


#include "basement/Basement_global.h"

#include <string>
#include <vector>
#include <utility>

namespace basement
{
	namespace math
	{
		class InverseFunction
		{
			private:
				static const std::string CLASS_NAME;

				InverseFunction() = delete;

			public:
				static std::pair<std::vector<double>, std::vector<double>> find
				(
				const std::vector<double>& x,
				const std::vector<double>& y
				);

		};
	}
}

#endif // BASEMENT_MATH_INVERSEFUNCTION_H
