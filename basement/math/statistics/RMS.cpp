#include "basement/math/statistics/RMS.h"
#include <cmath>
#include "basement/math/statistics/ArithmeticMean.h"

namespace basement
{
	namespace math
	{
		namespace statistics
		{
			double RMS(const std::vector<double>& vals)
			{
				double res = 0;
				for (auto& val : vals)
				{
					res += val*val;
				}
				return std::sqrt(res/vals.size());
			}

            double RMS_ShiftSampleToZeroMean(std::vector<double> vals)
            {
                const double mean = arithmeticMean(vals);

                for (auto& val : vals)
                    val -= mean;

                return RMS(vals);
            }
		}
	}
}
