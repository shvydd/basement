#include "basement/math/statistics/ArithmeticMean.h"

namespace basement
{
	namespace math
	{
		namespace statistics
		{
			double arithmeticMean(const std::vector<double>& v)
			{
				double sum = std::accumulate(v.begin(), v.end(), 0.0);
				return sum / v.size();
			}
		}
	}
}
