#ifndef BASEMENT_MATH_STATISTICS_ARITHMETICMEAN_H
#define BASEMENT_MATH_STATISTICS_ARITHMETICMEAN_H


#include "basement/Basement_global.h"

#include <vector>
#include <numeric>

namespace basement
{
	namespace math
	{
		namespace statistics
		{
			double arithmeticMean(const std::vector<double>& vals);
		}
	}
}

#endif // BASEMENT_MATH_STATISTICS_ARITHMETICMEAN_H
