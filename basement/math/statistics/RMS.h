#ifndef BASEMENT_MATH_STATISTICS_RMS_H
#define BASEMENT_MATH_STATISTICS_RMS_H


#include "basement/Basement_global.h"

#include <vector>

namespace basement
{
	namespace math
	{
		namespace statistics
		{
			double RMS(const std::vector<double>& vals);
            double RMS_ShiftSampleToZeroMean(std::vector<double> vals);
		}
	}
}

#endif // BASEMENT_MATH_STATISTICS_RMS_H
