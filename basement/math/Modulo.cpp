#include "basement/math/Modulo.h"

#include <cmath>

namespace basement
{
	namespace math
	{
		double modulo(const double& val, const double& quotient)
		{
			double fmod = std::fmod(val, quotient);
			if (fmod < 0)
				fmod += quotient;

			return fmod;
		}
	}
}
