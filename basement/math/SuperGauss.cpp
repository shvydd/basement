#include "basement/math/SuperGauss.h"

#include <cmath>
#include <limits>
#include <stdexcept>

namespace basement
{
	namespace math
	{
		double superGauss(const double& center, const double& widthFWHM, const double& order, const double& x)
		{
			if (order < 0.0)
				throw std::invalid_argument("basement::math::superGauss: order < 0");

			if (widthFWHM < 0.0)
				throw std::invalid_argument("basement::math::superGauss: widthFWHM < 0");

			if (widthFWHM == 0.0)
			{
				if ( x-center == 0.0) return 1.0;
				else return 0.0;
			}

			double expArgument = (x-center)/widthFWHM;
			expArgument = std::pow(4.0 * expArgument*expArgument, order);
			return std::exp(std::log(0.5) * expArgument);
		}

		std::pair<double,double> superGaussMarginsAtLevel(const double& center, const double& widthFWHM, const double& order, const double& level)
		{
			if (level > 1.0 || level <= 0.0)
				throw std::invalid_argument("basement::math::superGaussMarginsAtLevel: level does not belong to (0; 1]");

			if (order < 0.0)
				throw std::invalid_argument("basement::math::superGaussMarginsAtLevel: order < 0");

			if (widthFWHM < 0.0)
				throw std::invalid_argument("basement::math::superGaussMarginsAtLevel: widthFWHM < 0");

			if (widthFWHM == 0.0) return std::pair<double,double>(center, center);

			if (order == 0.0) return std::pair<double,double>(center-widthFWHM/2.0, center+widthFWHM/2.0);

			const double wing = widthFWHM/2.0 * std::pow( std::log(level)/std::log(0.5), 1.0/(2.0*order) );
			return std::pair<double,double>(center-wing, center+wing);
		}

		double superGaussWidthFWHMtoWidthAtLevel(const double& widthFWHM, const double& order, const double& level)
		{
			if (level > 1.0 || level <= 0.0)
				throw std::invalid_argument("basement::math::superGaussWidthFWHMtoWidthAtLevel: level does not belong to (0; 1]");

			if (order < 0.0)
				throw std::invalid_argument("basement::math::superGaussWidthFWHMtoWidthAtLevel: order < 0");

			if (widthFWHM < 0.0)
				throw std::invalid_argument("basement::math::superGaussWidthFWHMtoWidthAtLevel: widthFWHM < 0");

			if (widthFWHM == 0.0) return 0.0;

			if (order == 0.0) return widthFWHM;

			return widthFWHM * std::pow( std::log(level)/std::log(0.5), 1.0/(2.0*order) );
		}

		double superGaussWidthAtLeveltoWidthFWHM(const double& fullWidthAtLevel, const double& order, const double& level)
		{
			if (level > 1.0 || level <= 0.0)
				throw std::invalid_argument("basement::math::superGaussWidthAtLeveltoWidthFWHM: level does not belong to (0; 1]");

			if (order < 0.0)
				throw std::invalid_argument("basement::math::superGaussWidthAtLeveltoWidthFWHM: order < 0");

			if (fullWidthAtLevel < 0.0)
				throw std::invalid_argument("basement::math::superGaussWidthAtLeveltoWidthFWHM: fullWidthAtLevel < 0");

			if (fullWidthAtLevel == 0.0) return 0.0;

			if (order == 0.0) return fullWidthAtLevel;

			return fullWidthAtLevel * std::pow( std::log(0.5)/std::log(level),  1.0/(2.0*order) );
		}
	}
}
