#ifndef BASEMENT_MATH_SUPERGAUSS_H
#define BASEMENT_MATH_SUPERGAUSS_H


#include "basement/Basement_global.h"

#include <utility>

namespace basement
{
	namespace math
	{
		double superGauss(const double& center, const double& widthFWHM, const double& order, const double& x);

		std::pair<double,double> superGaussMarginsAtLevel(const double& center, const double& widthFWHM, const double& order, const double& level);

		double superGaussWidthFWHMtoWidthAtLevel(const double& widthFWHM, const double& order, const double& level);

		double superGaussWidthAtLeveltoWidthFWHM(const double& fullWidthAtLevel, const double& order, const double& level);
	}
}

#endif // BASEMENT_MATH_SUPERGAUSS_H
