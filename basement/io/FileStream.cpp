#include "basement/io/FileStream.h"

namespace basement
{
	namespace io
	{
        FileStreamException::FileStreamException(const char* description) noexcept
		: description(description)
		{

		}

        FileStreamException::FileStreamException(const std::string& description) noexcept
		: description(description)
		{

		}

		FileStreamException::~FileStreamException() noexcept
		{

		}

		const char* FileStreamException::what() const noexcept
		{
			return description.c_str();
		}


        FileStream::FileStream
		(
        const std::filesystem::path& _path,
		std::ios_base::openmode _mode,
        bool _createFolderIfNotExist
		)
        : path(_path)
		{
			auto folderPath = path.parent_path();

			if ( (_mode & std::ios::out) == std::ios::out)
			{
                if ( !_createFolderIfNotExist )
				{
                    if ( !std::filesystem::exists(folderPath) )
					{
                        throw FileStreamException(EXCEPTION_OPENING + ". Folder does not exist: \"" + folderPath.string() + "\"");
					}
				}

                std::error_code errCode;
                std::filesystem::create_directories(folderPath, errCode);
                if ( errCode )
				{
					throw FileStreamException(EXCEPTION_OPENING + ". " + errCode.message() + ": \"" + path.string() +"\"");
				}
			}

			this->open(path.string(), _mode);
			if ( !this->is_open() )
			{
				throw FileStreamException(EXCEPTION_OPENING + ": \"" + path.string() + "\"");
			}

			this->exceptions(std::istream::failbit | std::fstream::badbit);
		}

		FileStream::~FileStream()
		{
            std::fstream::close();
		}

        const std::filesystem::path& FileStream::getPath() const
		{
			return path;
		}

        const std::string FileStream::EXCEPTION_OPENING = "Can't open/create file";
        const std::string FileStream::EXCEPTION_CLOSING = "Something went wrong during closing of file";
        const std::string FileStream::WARNING_FAIL = "Error(s) occured.";
	}
}
