#ifndef BASEMENT_IO_TEXTINPUT_H
#define BASEMENT_IO_TEXTINPUT_H

#include "basement/Basement_global.h"

#include "basement/io/FileStream.h"
#include <mutex>

namespace basement
{
	namespace io
	{
		class TextInput
		{
        public:
            TextInput(const std::filesystem::path& path);

            /**
             * @brief Read a line from file. Preserves "\\n" at the line end.
             * @return true, if eof has been reached.
             */
            std::pair<std::string, bool> readLine();
            std::string readToEnd();

        private:
            FileStream file;
            mutable std::mutex mutex;

            TextInput(const TextInput&) = delete;
            TextInput& operator=(const TextInput&) = delete;

            std::pair<std::string, bool> readLinePrivate();
		};
	}
}

#endif // BASEMENT_IO_TEXTINPUT_H
