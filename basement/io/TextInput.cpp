#include "basement/io/TextInput.h"

namespace basement
{
	namespace io
	{
        TextInput::TextInput(const std::filesystem::path& path)
        : file(path, std::ios::in)
		{

		}

		std::pair<std::string, bool>  TextInput::readLine()
		{
			std::lock_guard<std::mutex> lock(mutex);

            return readLinePrivate();
		}

		std::string TextInput::readToEnd()
		{
			//"file.eof()" works fine on Windows,
			//but sets failbit on Linux,
			//as last line on Win formed as: content-EOF;
			//and last line on Lin formed as: content-EOL-EOF.

			std::lock_guard<std::mutex> lock(mutex);

            std::string res;
            bool eof = false;
            while ( !eof )
            {
                const auto readLineRes = readLinePrivate();
                res += readLineRes.first;
                eof = readLineRes.second;
            }

			return res;
		}

        std::pair<std::string, bool> TextInput::readLinePrivate()
        {
            const auto& eof = std::char_traits<char>::eof();
            std::pair<std::string, bool> res(std::string(), true);
            if ( !file.eof() )
            {
                if ( file.peek() != eof )
                {
                    std::getline(file, res.first);

                    if ( !file.eof() )
                    {
                        //If next symbol is not EOF - add "\n".
                        if ( file.peek() != eof )
                        {
                            res.first += "\n";
                            res.second = false;
                        }
                        else
                        {
                            //If the last string contains "\n", "\n" will be lost.
                            //Equivalently: if the next line is empty, it will be lost.
                            //This fixes it.
                            char lastChar;
                            file.unget();
                            file.get(lastChar);
                            if (lastChar == '\n')
                            {
                                res.first += "\n";
                            }
                            res.second = true;

                            file.setstate(std::ios::eofbit);
                            return res;
                        }
                    }
                    else
                    {
                        res.second = true;
                        return res;
                    }
                }
            }
            return res;
        }
	}
}


