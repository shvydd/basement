#ifndef BASEMENT_IO_TEXTOUTPUT_H
#define BASEMENT_IO_TEXTOUTPUT_H

#include "basement/Basement_global.h"

#include "basement/io/FileStream.h"
#include <list>

namespace basement
{
	namespace io
	{
		class TextOutput
		{
        public:
            /**
             * @brief Enables output only to console.
             * @param indent Number of tabs are added before each line.
             */
            TextOutput(const int indent = 0);

            /**
             * @brief Enables output to file and (optional) to console.
             * @param indent Number of tabs are added before each line.
             */
            TextOutput
            (
            const std::filesystem::path& filePath,
            const bool consoleOutput = false,
            const bool truncFiles = true,
            const bool createFolderIfNotExist = true,
            const int  indent = 0
            );

            /**
             * @brief Enables output to files and (optional) to console.
             * @param indent Number of tabs are added before each line.
             */
            TextOutput
            (
            const std::list<std::filesystem::path>& filePaths,
            const bool consoleOutput = false,
            const bool truncFiles = true,
            const bool createFoldersIfNotExist = true,
            const int  indent = 0
            );

            ~TextOutput();

            void indentAdd() const;
            void indentReduce() const;
            int  indentGet() const;
            void indentSet(const int) const;

            void print(const std::string& text) const;
            void printLine(const std::string& text) const;

        private:
            std::list<FileStream*> files;
            const bool consoleOutput;

            mutable int indent;

            static const std::string EXCEPTION_INDENT_NEGATIVE;

            TextOutput(const TextOutput&) = delete;
            TextOutput& operator=(const TextOutput&) = delete;
		};
	}
}

#endif /* BASEMENT_IO_TEXTOUTPUT_H */
