****

## Basement ##

A c++ library with the following content:

1. Collection of basic algorithms for interpolation, integration, derivation, Fourier transform, Taylor expansion and equation solving.
2. 3D vector operations and some operations for extended quadratic forms.
3. Containers for storage of surface and volume distributions, container for data that is convenient to store in the form of columns, as well as grid and histogram classes.
4. Functions and function wraps for some fundamental and standard library data types.
5. Math functions: finding extrema, inverse function, type of sequence monotonicity etc.
6. Wraps for std::fstream for convenient reading and writing text files.

---

**Dependencies**

1. c++17
2. Boost/serialization (tested with v.1.75);
3. Qt libraries: core.

---
